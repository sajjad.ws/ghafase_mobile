import 'dart:async';

import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:http/http.dart';
import 'package:arzaan_sara/models/search_model.dart';

import 'api_provider.dart';

class Repository<T> {
  final apiProvider = ApiProvider<T>();

  Future<int> logIn(String username, String password) =>
      apiProvider.logIn(username, password);

  Future<List<SearchModel>> fetchSearchList(String name, LatLng loc) =>
      apiProvider.fetchSearchList(name, loc);

  Future<T> fetchTransactions(String apiAddress, Map<String, String> body,
          T Function(dynamic) returnType, Function(Response) fullResponse) =>
      apiProvider.fetchDataPOSTAuth(apiAddress, body, returnType,fullResponse);

  Future<T> fetchRequestHistory(String apiAddress, Map<String, String> body,
          T Function(dynamic) returnType, Function(Response) fullResponse) =>
      apiProvider.fetchDataPOSTAuth(apiAddress, body, returnType,fullResponse);

  Future<T> fetchMessages(String apiAddress, Map<String, String> body,
      T Function(dynamic) returnType, Function(Response) fullResponse) =>
      apiProvider.fetchDataPOSTAuth(apiAddress, body, returnType,fullResponse);

  Future<T> fetchUserData(String apiAddress, Map<String, String> body,
      T Function(dynamic) returnType, Function(Response) fullResponse) =>
      apiProvider.fetchDataPOSTAuth(apiAddress, body, returnType,fullResponse);


  Future<T> fetchActiveRequest(String apiAddress, Map<String, String> body,
          T Function(dynamic) returnType, Function(Response) fullResponse) =>
      apiProvider.fetchDataPOSTAuth(apiAddress, body, returnType,fullResponse);

  Future<T> fetchPendingRequest(String apiAddress, Map<String, String> body,
          T Function(dynamic) returnType, Function(Response) fullResponse) =>
      apiProvider.fetchDataPOST(apiAddress, body, returnType,fullResponse);

  Future<T> fetchAddRequest(String apiAddress, Map<String, String> body,
          T Function(dynamic) returnType, Function(Response) fullResponse) =>
      apiProvider.fetchDataPOSTAuth(apiAddress, body, returnType,fullResponse);

  Future<T> fetchAcceptRequestByDriver(String apiAddress, Map<String, String> body,
          T Function(dynamic) returnType, Function(Response) fullResponse) =>
      apiProvider.fetchDataPOSTAuth(apiAddress, body, returnType,fullResponse);

  Future<T> fetchCancelOwner(String apiAddress, Map<String, String> body,
          T Function(dynamic) returnType, Function(Response) fullResponse) =>
      apiProvider.fetchDataPOSTAuth(apiAddress, body, returnType,fullResponse);

  Future<T> fetchStart(String apiAddress, Map<String, String> body,
          T Function(dynamic) returnType, Function(Response) fullResponse) =>
      apiProvider.fetchDataPOSTAuth(apiAddress, body, returnType,fullResponse);

  Future<T> fetchEnd(String apiAddress, Map<String, String> body,
          T Function(dynamic) returnType, Function(Response) fullResponse) =>
      apiProvider.fetchDataPOSTAuth(apiAddress, body, returnType,fullResponse);


  Future<T> fetchEditProfile(String apiAddress, Map<String, String> body,
      T Function(dynamic) returnType, Function(Response) fullResponse) =>
      apiProvider.fetchDataPOSTAuth(apiAddress, body, returnType,fullResponse);


  Future<T> fetchAddComment(String apiAddress, Map<String, String> body,
      T Function(dynamic) returnType, Function(Response) fullResponse) =>
      apiProvider.fetchDataPOSTAuth(apiAddress, body, returnType,fullResponse);

  Future<T> fetchUpdateProfilePhoto(String apiAddress, Map<String, dynamic> body,
      T Function(dynamic) returnType, Function(MultipartRequest) fullResponse) =>
      apiProvider.fetchDataPOSTFileAuth(apiAddress, body, returnType,fullResponse);

  Future<T> logInVerifyPhoneNumber(String apiAddress, Map<String, String> body,
      T Function(dynamic) returnType, Function(Response) fullResponse) =>
      apiProvider.fetchDataPOST(apiAddress, body, returnType,fullResponse);

  Future<T> fetchReceivePhoneNumber(String apiAddress, Map<String, String> body,
          T Function(dynamic) returnType, Function(Response) fullResponse) =>
      apiProvider.fetchDataPOST(apiAddress, body, returnType,fullResponse);

  Future<T> fetchInitialData(String apiAddress, T Function(dynamic) returnType, Function(Response) fullResponse,) =>
      apiProvider.fetchDataGET(apiAddress, returnType,fullResponse);



  ///////////


  Future<T> fetchProducts(String apiAddress, Map<String, String> body,
      T Function(dynamic) returnType, Function(Response) fullResponse) =>
      apiProvider.fetchDataPOST(apiAddress, body, returnType,fullResponse);

  Future<T> fetchStores(String apiAddress, Map<String, String> body,
      T Function(dynamic) returnType, Function(Response) fullResponse) =>
      apiProvider.fetchDataPOST(apiAddress, body, returnType,fullResponse);



  Future<T> fetchAddToCard(String apiAddress, Map<String, String> body,
      T Function(dynamic) returnType, Function(Response) fullResponse) =>
      apiProvider.fetchDataPOSTAuth(apiAddress, body, returnType,fullResponse);

}
