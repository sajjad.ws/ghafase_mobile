import 'package:arzaan_sara/constants/enums.dart';
import 'package:arzaan_sara/models/const_model/banner_model.dart';
import 'package:arzaan_sara/models/const_model/cargo_type_model.dart';
import 'package:arzaan_sara/models/const_model/category_model.dart';
import 'package:arzaan_sara/models/const_model/city_model.dart';
import 'package:arzaan_sara/models/const_model/setting_model.dart';
import 'package:arzaan_sara/models/const_model/vehicle_group_model.dart';
import 'package:arzaan_sara/models/const_model/vehicle_model.dart';
import 'package:arzaan_sara/models/user_model.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:async';

import 'dart:convert';

///shared preferences based class and use for storage data
class Save {
  SharedPreferences sharedPreferences;

  setLogin(bool value) async {
    sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setBool("login", value);

  }

  Future<bool> isLogin() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();

    return sharedPreferences.getBool("login") == null
        ? false
        : sharedPreferences.getBool("login");
  }


  setPhoneInserted(bool value) async {
    sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setBool("phoneInserted", value);

  }

  Future<bool> isPhoneInserted() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();

    return sharedPreferences.getBool("phoneInserted") == null
        ? false
        : sharedPreferences.getBool("phoneInserted");
  }

  setFormFilled(bool value) async {
    sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setBool("formFilled", value);

  }

  Future<bool> isFormFilled() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();

    return sharedPreferences.getBool("formFilled") == null
        ? false
        : sharedPreferences.getBool("formFilled");
  }

  //////////////// first run //////////////////////
  setFirstTime(bool value) async {
    sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setBool("firstTime", value);
  }

  Future<bool> isFirstTime() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();

    return sharedPreferences.getBool("firstTime") == null
        ? true
        : sharedPreferences.getBool("firstTime");
  }

  ///////////////////////userData ///////////////

  setUserData(String value) async {
    sharedPreferences = await SharedPreferences.getInstance();

    sharedPreferences.setString("userData", value);
  }

  Future<UserModel> getUserData() async {
    SharedPreferences shared_User = await SharedPreferences.getInstance();


    Map userMap = jsonDecode(shared_User.getString('userData'));

    return UserModel.fromJson(userMap);
  }

  setCommented(bool value) async {
    sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setBool("commented", value);
  }

  Future<bool> isCommented() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();

    return sharedPreferences.getBool("commented") == null
        ? false
        : sharedPreferences.getBool("commented");
  }

  setToken(String value) async {
    sharedPreferences = await SharedPreferences.getInstance();

    sharedPreferences.setString("token", value);
  }

  Future<String> getToken() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getString("token");
  }



  setCarType(String value) async {
    sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setString("carType", value);
  }

  Future<String> getCarType() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getString("carType");
  }




  ///////////////////////storage cities ///////////////

  setCityData(String value) async {
    sharedPreferences = await SharedPreferences.getInstance();

    sharedPreferences.setString("cityData", value);
  }

  Future<CityModel> getCityData() async {
    SharedPreferences shared_User = await SharedPreferences.getInstance();

    List cityList = jsonDecode(shared_User.getString('cityData'));


    return CityModel.fromJson(cityList);
  }


  ///////////////////////storage cities ///////////////

  setBannerData(String value) async {
    sharedPreferences = await SharedPreferences.getInstance();

    sharedPreferences.setString("bannerData", value);
  }

  Future<BannerModel> getBannerData() async {
    SharedPreferences shared_User = await SharedPreferences.getInstance();

    List cityList = jsonDecode(shared_User.getString('bannerData'));


    return BannerModel.fromJson(cityList);
  }


  ///////////////////////storage cities ///////////////

  setCategoryData(String value) async {
    sharedPreferences = await SharedPreferences.getInstance();

    sharedPreferences.setString("categoryData", value);
  }

  Future<CategoryModel> getCategoryData() async {
    SharedPreferences shared_User = await SharedPreferences.getInstance();

    List cityList = jsonDecode(shared_User.getString('categoryData'));


    return CategoryModel.fromJson(cityList);
  }

  ///////////////////////storage vehicles ///////////////

  setVehiclesData(String value) async {
    sharedPreferences = await SharedPreferences.getInstance();

    sharedPreferences.setString("vehiclesData", value);
  }

  Future<VehicleModel> getVehiclesData() async {
    SharedPreferences shared = await SharedPreferences.getInstance();

    List list = jsonDecode(shared.getString('vehiclesData'));


    return VehicleModel.fromJson(list);
  }


  setVehiclesGroupData(String value) async {
    sharedPreferences = await SharedPreferences.getInstance();

    sharedPreferences.setString("vehiclesGroupData", value);
  }

  Future<VehicleGroupModel> getVehiclesGroupData() async {
    SharedPreferences shared = await SharedPreferences.getInstance();

    List list = jsonDecode(shared.getString('vehiclesGroupData'));


    return VehicleGroupModel.fromJson(list);
  }


  ////////////////////// storage cargo type //////////////


  setCargoTypeData(String value) async {
    sharedPreferences = await SharedPreferences.getInstance();

    sharedPreferences.setString("cargoTypeData", value);
  }

  Future<CargoTypeModel> getCargoTypeData() async {
    SharedPreferences shared = await SharedPreferences.getInstance();

    List list = jsonDecode(shared.getString('cargoTypeData'));


    return CargoTypeModel.fromJson(list);
  }

  ///////////////////////storage setting ///////////////

  setSettingData(String value) async {
    sharedPreferences = await SharedPreferences.getInstance();

    sharedPreferences.setString("settingData", value);
  }

  Future<SettingModel> getSettingData() async {
    SharedPreferences shared = await SharedPreferences.getInstance();

    Map map = jsonDecode(shared.getString('settingData'));


    return SettingModel.fromJson(map);
  }



  ///////////////////////storage firebase token ///////////////

  setFirebaseToken(String value) async {
    sharedPreferences = await SharedPreferences.getInstance();

    sharedPreferences.setString("firebseToken", value);
  }

  Future<String> getFirebaseToken() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getString("firebseToken");
  }



  ////////////////////// clears ///////////////////

  clearSavedData()async{
    sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.clear();

  }








}
