import 'dart:convert';

/// this class use for check the response of server.
/// if status 200 and error not null then return data
/// else return String "nok"
class StatusChecker {
  String _date = "";
  String _errors = "";
  int _status = 0;

  StatusChecker(Map<String, dynamic> parsedJson) {
    if (parsedJson['data'] is String) {
      this._date = parsedJson['data'].toString();
    } else {
      this._date = json.encode(parsedJson['data']).toString();
    }
    this._errors = parsedJson['errors'];
    this._status = parsedJson['status'];
  }

  String getData() {
    if (_status == 200 && _errors == null) {
      return _date;
    } else {
      // OverlyAlert.show("لطفا شهر خود را انتخاب نمایید", context, duration:  OverlyAlert.LENGTH_LONG, gravity: OverlyAlert.BOTTOM,backgroundColor: Colors.red,textColor: Colors.white);

      return null;
    }
  }

  bool isOK() {
    if (_status == 200 && _errors == null) {
      return true;
    } else {
      return false;
    }
  }
}
