import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:path/path.dart';

import 'package:async/async.dart';
import 'package:flutter/cupertino.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:http/http.dart' show Client, MultipartRequest, Response;
import 'package:http/http.dart' as http;
import 'package:arzaan_sara/constants/strings.dart';
import 'package:arzaan_sara/models/search_model.dart';
import 'package:arzaan_sara/models/user_model.dart';
import 'package:arzaan_sara/resources/save.dart';
import 'package:arzaan_sara/resources/status_checker.dart';

class ApiProvider<T> {
  Client client = Client();

  Future<int> logIn(String username, String password) async {
    /**
     * Example Real Connection to Web Service
     *
     *
     * final response = await client.post(
        "$_baseUrl/receive-phone-number",
        body: {
        "phone_number": phone,
        },
        headers: {"Content-Type": "application/x-www-form-urlencoded"},
        );
        return response.statusCode;
     *
     */
    if (username == "admin" && password == "1234")
      return 200;
    else
      return 500;
  }

  Future<List<SearchModel>> fetchSearchList(String name, LatLng loc) async {
    var _lat = loc.latitude;
    var _lon = loc.longitude;
    var uri =
        "https://api.cedarmaps.com/v1/geocode/cedarmaps.streets/$name?access_token=${Strings.cedarMapToken}&location=$_lat,$_lon&distance=10.0";

    var encoded = Uri.encodeFull(uri);
    final response = await client.get(encoded);
    if (response.statusCode == 200) {
      return SearchModel.parsJson(json.decode(response.body));
    } else {
      return [];
    }
  }

  Future<T> fetchDataPOST(String apiAddress, Map<String, String> body,
      @required T Function(dynamic) returnType,
      Function(Response) fullResponse) async {
    Response response;
    try {
      response = await client
          .post(Strings.apiUrl + apiAddress, body: body)
          .timeout(const Duration(seconds: 5));
    } on TimeoutException catch (e) {
      print(e.toString());
      return returnType("to");
    } on Exception catch (e) {
      print(e.toString());
      return returnType("nc");
    }
    if(fullResponse != null)
      fullResponse(response);

    if (response.statusCode == 200) {
      String data =
      StatusChecker(json.decode(response.body)).getData().toString();

      if (StatusChecker(json.decode(response.body)).isOK()) {
        return returnType(data);
      } else {
        return null;
      }
    } else {
      return returnType("error");
    }
  }

  Future<T> fetchDataPOSTAuth(String apiAddress, Map<String, String> body,
      @required T Function(dynamic) returnType,
      Function(Response) fullResponse) async {
    UserModel um = await Save().getUserData();
   // body.addAll({"api_token": um.api_token});
    body.addAll({"api_token": "aaa"});

    Response response;
    try {
      response = await client
          .post(Strings.apiUrl + apiAddress, body: body)
          .timeout(const Duration(seconds: 10));
    } on TimeoutException catch (e) {
      print(e.toString());
      return returnType("to");
    } on Exception catch (e) {
      print(e.toString());
      return returnType("nc");
    }

    if(fullResponse != null)
      fullResponse(response);

    if (response.statusCode == 200) {
      String data =
      StatusChecker(json.decode(response.body)).getData().toString();

      if (StatusChecker(json.decode(response.body)).isOK()) {
        return returnType(data);
      } else {
        return null;
      }
    } else {
      return returnType("error");
    }
  }



  Future<T> fetchDataPOSTFileAuth(String apiAddress,
      Map<String, dynamic> body,
      @required T Function(dynamic) returnType,
      Function(MultipartRequest) fullResponse ) async {

    UserModel um = await Save().getUserData();

    MultipartRequest request = new http.MultipartRequest("POST", Uri.parse(Strings.apiUrl + apiAddress));

    request.fields['api_token'] = um.api_token;

    Map<String, String> exceptImage = {};

    String imageFieldName;
    File imageFile;



    body.forEach((k, v){
      if (v is String) {
        //exceptImage.addAll({k:v});
        exceptImage[k] = v;

      }
      else if (v is File) {
        imageFieldName = k.toString();
        imageFile = v;
        /*      print(k);
        print(v);
        image.addAll({k:v});

        imageFieldName = k.toString();*/

        //images.add(image);

      }
    });




    if(exceptImage != null) {
      request.fields.addAll(exceptImage);
    }
    if(imageFile != null){

      var stream = new http.ByteStream(
          DelegatingStream.typed(imageFile.openRead()));
      var length = await imageFile.length();


      http.MultipartFile multipartFile = new http.MultipartFile(
          imageFieldName, stream, length,
          filename: basename(imageFile.path));

      request.files.add(multipartFile);
    }

    if(fullResponse != null)
      fullResponse(request);


    return request.send().then((response) {
      if (response.statusCode == 200) {
        print(response);

        return returnType("true");



      } else {
        return returnType("error");

      }
    });



  }


  Future<T> fetchDataGET(
      String apiAddress,
      @required T Function(dynamic) returnType,
      Function(Response) fullResponse) async {
    Response response;
    try {
      response = await client
          .get(
        Strings.apiUrl + apiAddress,
      )
          .timeout(const Duration(seconds: 10));
    } on TimeoutException catch (e) {
      print(e.toString());
      return returnType("to");
    } on Exception catch (e) {
      print(e.toString());
      return returnType("nc");
    }


    if(fullResponse != null)
      fullResponse(response);

    if (response.statusCode == 200) {
      String data =
      StatusChecker(json.decode(response.body)).getData().toString();

      if (StatusChecker(json.decode(response.body)).isOK()) {
        return returnType(data);
      } else {
        return null;
      }
    } else {
      return returnType("error");
    }
  }
}