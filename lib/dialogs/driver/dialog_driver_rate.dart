import 'package:flutter/material.dart';
import 'package:arzaan_sara/blocs/request_bloc.dart';
import 'package:arzaan_sara/global/alerts/alerts.dart';
import 'package:arzaan_sara/global/common_views/button_text.dart';
import 'package:arzaan_sara/global/common_views/subtitle_text.dart';
import 'package:arzaan_sara/global/common_views/title_text.dart';
import 'package:arzaan_sara/global/views/easy_flat_button.dart';
import 'package:arzaan_sara/global/views/edit_text.dart';
import 'package:arzaan_sara/global/views/flat_button_streamer.dart';
import 'package:arzaan_sara/global/views/primary_button_streamer.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';


class DialogDriverRate extends StatefulWidget {

  String requestId;

  DialogDriverRate({this.requestId});

  @override
  State<StatefulWidget> createState() => DialogDriverRateState();
}

class DialogDriverRateState extends State<DialogDriverRate>
    with SingleTickerProviderStateMixin {
  AnimationController controller;
  Animation<double> scaleAnimation;

  double rate = 0;
  final description = TextEditingController();

  @override
  void initState() {
    super.initState();
    controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 550));
    scaleAnimation =
        CurvedAnimation(parent: controller, curve: Curves.fastOutSlowIn);
    controller.forward();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    controller.dispose();
  }


  @override
  Widget build(BuildContext context) {
    return new Directionality(
      textDirection: TextDirection.rtl,
      child: Center(
        child: Material(
          color: Colors.transparent,
          child: ScaleTransition(
            scale: scaleAnimation,
            child: new Padding(
              padding: EdgeInsets.all(32),
              child: new Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8),
                ),
                child: Padding(
                  padding: EdgeInsets.all(16),
                  child: new Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Align(
                        alignment: Alignment.topRight,
                        child: InkWell(
                          child: Icon(Icons.clear,),
                          onTap: () {
                            Navigator.pop(context);
                          },
                        ),
                      ),

                      dialogContent(),

                      SizedBox(
                        height: 24,
                      ),
                      Row(
                        children: <Widget>[
                          Expanded(child: PrimaryButtonStreamer<String>(
                            stream: requestBlocEnd.actions,
                            text: "ارسال نظر",
                            backgroundColor: Theme.of(context).primaryColor,
                            onPressed: () {
                              primaryButtonStreamerState.setState(() {
                                primaryButtonStreamerState.isLoading = true;
                              });

                              requestBlocAddComment.fetchAddComment(
                                apiAddress: "add-comment", body: {
                                "cargo_request_id" : widget.requestId,
                                "score" : rate.toInt().toString(),
                                "content" : description.text
                              },).then((res){

                                requestBlocEnd.fetchEnd(
                                  apiAddress: "end", body: {
                                  "request_id" : widget.requestId
                                },).then((res){
                                  primaryButtonStreamerState.setState(() {
                                    primaryButtonStreamerState.isLoading = false;
                                  });
                                  Navigator.pop(context);
                                });
                              });



                            },
                            afterFinish: (snapShot) {
                              Alerts.success(context, "نظر شما با موفقیت ثبت شد");
                              requestBlocEnd.dispose();
                            },
                          ),),
                          SizedBox(
                            width: 16,
                          ),
                          Expanded(child: EasyFlatButton(
                              text: "بیخیال",
                              onPressed: (){
                                Navigator.pop(context);
                              }
                          ),),


                        ],
                      )

                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget dialogContent() {
    return Column(
      children: <Widget>[
        Text(
          "درخواست شما با موفقیت پایان یافت"
          , style: Theme
            .of(context)
            .textTheme
            .title
            .copyWith(color: Colors.green),),
        SizedBox(
          height: 16,
        ),
        SubtitleText('جهت بهبود خدمات اوبار لطفا نظر خود را ثبت کنید '),

        SizedBox(
          height: 16,
        ),

        Container(
          padding: EdgeInsets.only(right: 16, left: 16, top: 8, bottom: 8),
          decoration: BoxDecoration(
            color: Theme
                .of(context)
                .primaryColor,
            borderRadius: BorderRadius.all(Radius.elliptical(50, 50)),
          ),
          child: SmoothStarRating(
            allowHalfRating: false,
            onRatingChanged: (v) {
              setState(() {
                rate = v;
              });
            },
            starCount: 5,
            rating: rate,
            size: 35,
            color: Colors.white,
            borderColor: Colors.white,
          ),
        ),
        SizedBox(
          height: 16,
        ),
        EditText.text(
          controller: description,
          border: true,
          maxLines: 3,
          textAlign: TextAlign.right,
          hint: "نظر خود را بنویسید",

        )

      ],
    );
  }
}
