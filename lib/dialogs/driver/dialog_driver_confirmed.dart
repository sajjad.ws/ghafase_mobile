import 'package:flutter/material.dart';
import 'package:arzaan_sara/constants/enums.dart';
import 'package:arzaan_sara/global/common_views/button_text.dart';
import 'package:arzaan_sara/global/common_views/title_text.dart';
import 'package:arzaan_sara/global/common_views/title_text_error.dart';
import 'package:arzaan_sara/global/views/easy_button.dart';
import 'package:arzaan_sara/global/views/easy_flat_button.dart';
import 'package:arzaan_sara/resources/save.dart';
import 'package:arzaan_sara/screens/phone/phone_register.dart';
import 'package:arzaan_sara/screens/profile/profile_form.dart';


class DialogDriverConfirmed extends StatefulWidget {


  DialogDriverConfirmed();

  @override
  State<StatefulWidget> createState() => DialogDriverConfirmedState();
}

class DialogDriverConfirmedState extends State<DialogDriverConfirmed>
    with TickerProviderStateMixin {
  AnimationController controller;
  Animation<double> scaleAnimation;


  @override
  void initState() {
    super.initState();
    controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 550));
    scaleAnimation =
        CurvedAnimation(parent: controller, curve: Curves.fastOutSlowIn);
    controller.forward();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Directionality(
      textDirection: TextDirection.rtl,
      child: Center(
        child: Material(
          color: Colors.transparent,
          child: ScaleTransition(
            scale: scaleAnimation,
            child: new Padding(
              padding: EdgeInsets.all(32),
              child: new Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8),
                ),
                child: Padding(
                  padding: EdgeInsets.all(16),
                  child: new Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Align(
                        alignment: Alignment.topRight,
                        child: InkWell(
                          child: Icon(Icons.clear,),
                          onTap: (){
                            Navigator.pop(context);
                          },
                        ),
                      ),
                      
                      dialogContent(),

                      SizedBox(
                        height: 24,
                      ),
                      EasyButton(
                        isFilled: true,
                        textColor: Colors.white,
                        text: "ثبت نام | ورود",
                        onPressed: ()async{
                          bool isPhoneInserted = await Save().isPhoneInserted();
                          Navigator.push(context,
                              MaterialPageRoute(builder: (context) => PhoneRegister()));
                        },
                      )

                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget dialogContent() {

    return Column(
      children: <Widget>[
        TitleTextError(
            "عملیات ناموفق"
        ),
        SizedBox(
          height: 16,
        ),
        Text(" در صورت عدم ثبت نام لطفا ثبت نام نمایید، جهت ادامه کار لطفا وارد حساب کاربری خود شوید",textAlign: TextAlign.center,)

      ],
    );
  }
}
