import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:arzaan_sara/global/common_views/button_text.dart';
import 'package:arzaan_sara/global/common_views/title_text.dart';
import 'package:arzaan_sara/models/const_model/vehicle_group_model.dart';
import 'package:arzaan_sara/models/const_model/vehicle_model.dart';


import '../resources/save.dart';

class DialogCarType extends StatefulWidget {

  Function(String carId,String carName) onValueSelected;
  DialogCarType({this.onValueSelected});

  @override
  State<StatefulWidget> createState() => DialogCarTypeState();
}

class DialogCarTypeState extends State<DialogCarType>
    with SingleTickerProviderStateMixin {
  AnimationController controller;
  Animation<double> scaleAnimation;

  @override
  void initState() {
    super.initState();
    controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 550));
    scaleAnimation =
        CurvedAnimation(parent: controller, curve: Curves.fastOutSlowIn);
    controller.forward();

    setData();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    controller.dispose();
  }


  VehicleModel _vehicles;
  VehicleGroupModel _vehicleGroups;

  Future<String> setData() async {
    _vehicles = await Save().getVehiclesData();
    _vehicleGroups = await Save().getVehiclesGroupData();

    return "aaa";
  }

  Future<Map<int, List<VehicleModelDetail>>> setV() async {
    Map<int, List<VehicleModelDetail>> myList = {};

    VehicleModel _vehicles = await Save().getVehiclesData();
    VehicleGroupModel _vehicleGroups = await Save().getVehiclesGroupData();

    List<VehicleModelDetail> items = [];
    for (int i = 0; i < _vehicleGroups.vehicleGroups.length; i++) {
      myList.addAll({i: items});
    }

    print(json.encode(myList).toString());

    return myList;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: onBackPress,
      child: Directionality(
        textDirection: TextDirection.rtl,
        child: Center(
          child: Material(
            color: Colors.transparent,
            child: ScaleTransition(
              scale: scaleAnimation,
              child: new Padding(
                padding: EdgeInsets.all(32),
                child: new Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8),
                  ),
                  child: Padding(
                    padding: EdgeInsets.all(16),
                    child: new Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Align(
                          alignment: Alignment.topRight,
                          child: InkWell(
                            child: Icon(
                              Icons.clear,
                            ),
                            onTap: () {
                              Navigator.pop(context,"نوع خودرو شما*");
                            },
                          ),
                        ),
                        dialogContent(),
                        SizedBox(height: 16,),
                        Align(
                          alignment: Alignment.centerRight,
                          child: InkWell(
                            child: ButtonText("بستن"),
                            onTap: () {
                              Navigator.pop(context,"نوع خودرو شما*");
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ) ,
    );
  }

  Widget dialogContent() {
    return Column(
      children: <Widget>[
        TitleText("انتخاب نوع وسیله برای باربری"),
        SizedBox(height: 16,),
        ClipRRect(
          borderRadius: BorderRadius.all(Radius.circular(8)),
          child: Container(
            height: 200,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(8)),
              border: Border.all(color: Colors.black38, width: 1),
            ),
            child: FutureBuilder(
              future: setData(),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return ListView.builder(
                      padding: EdgeInsets.all(0),
                      itemCount: _vehicleGroups.vehicleGroups.length,
                      itemBuilder: (context, index) {
                        return Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: <Widget>[
                            Container(
                              height: 40,
                              alignment: Alignment.center,
                              color: Theme.of(context).primaryColor,
                              child: Text(
                                _vehicleGroups.vehicleGroups[index].name,
                                style: TextStyle(color: Colors.white),
                                textAlign: TextAlign.center,
                              ),
                            ),
                            content(index),
                          ],
                        );


                      });
                }
                return Center(
                  child: CircularProgressIndicator(),
                );
              },
            ),
          ),
        )
      ],
    );
  }

  int vehiclesCount(int index) {
    int count = 0;

    for (int i = 0; i < _vehicles.vehicles.length; i++) {
      if (_vehicles.vehicles[i].vehicle_group_id ==
          _vehicleGroups.vehicleGroups[index].id) {
        count++;
      }
    }

    //print(count);
    return count;
  }

  Widget content(int index) {
    List<Widget> cols = [];
    for (int i = 0; i < _vehicles.vehicles.length; i++) {
      if (_vehicles.vehicles[i].vehicle_group_id ==
          _vehicleGroups.vehicleGroups[index].id) {
        cols.add(InkWell(
          onTap: (){
            Navigator.pop(context,_vehicles.vehicles[i].name);

            if(widget.onValueSelected != null)
              widget.onValueSelected(_vehicles.vehicles[i].id,_vehicles.vehicles[i].name);
          },
          child: Container(
            alignment: Alignment.center,
            height: 35,
            child: Text(_vehicles.vehicles[i].name),
          ),
        ));
      }
    }
    return Column(
      children: cols,
    );
  }

  Future<bool> onBackPress() {
    Navigator.pop(context,"نوع خودرو شما*");
  }
}
