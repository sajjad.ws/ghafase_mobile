import 'package:flutter/material.dart';
import 'package:arzaan_sara/global/common_views/button_text.dart';
import 'package:arzaan_sara/global/common_views/title_text.dart';

import '../resources/save.dart';
import '../screens/splash.dart';

class DialogMessageDetail extends StatefulWidget {
  String title;
  String content;


  DialogMessageDetail({this.title, this.content});

  @override
  State<StatefulWidget> createState() => DialogMessageDetailState();
}

class DialogMessageDetailState extends State<DialogMessageDetail>
    with SingleTickerProviderStateMixin {
  AnimationController controller;
  Animation<double> scaleAnimation;

  @override
  void initState() {
    super.initState();
    controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 550));
    scaleAnimation =
        CurvedAnimation(parent: controller, curve: Curves.fastOutSlowIn);
    controller.forward();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    controller.dispose();
  }


  @override
  Widget build(BuildContext context) {
    return new Directionality(
      textDirection: TextDirection.rtl,
      child: Center(
        child: Material(
          color: Colors.transparent,
          child: ScaleTransition(
            scale: scaleAnimation,
            child: new Padding(
              padding: EdgeInsets.all(32),
              child: new Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8),
                ),
                child: Padding(
                  padding: EdgeInsets.all(16),
                  child: new Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Align(
                        alignment: Alignment.topRight,
                        child: InkWell(
                          child: Icon(Icons.clear,),
                          onTap: (){
                            Navigator.pop(context);
                          },
                        ),
                      ),
                      
                      dialogContent(),
                      Align(
                        alignment: Alignment.centerRight,
                        child: InkWell(child: ButtonText("بستن"),onTap: (){
                          Navigator.pop(context);
                        },),
                      ),

                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget dialogContent() {

    return Column(
      children: <Widget>[
        Align(
          alignment: Alignment.center,
          child: TitleText(
              widget.title
          ),
        ),
        new Padding(
          padding: EdgeInsets.symmetric(vertical: 16),
          child: new Align(
            alignment: Alignment.center,
            child: Text(
              widget.content,
            ),
          ),
        ),
      ],
    );
  }
}
