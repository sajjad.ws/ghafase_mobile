import 'dart:ui';

import 'package:autocomplete_textfield/autocomplete_textfield.dart';
import 'package:flutter/material.dart';
import 'package:arzaan_sara/global/common_views/subtitle_text.dart';
import 'package:arzaan_sara/global/common_views/title_text.dart';
import 'package:arzaan_sara/global/modules/persian_date_pickerx.dart';
import 'package:arzaan_sara/global/modules/range_slider_x.dart';
import 'package:arzaan_sara/global/views/easy_button.dart';
import 'package:arzaan_sara/global/views/flat_button_icon.dart';
import 'package:arzaan_sara/global/views/futured_spinner.dart';
import 'package:arzaan_sara/models/const_model/cargo_type_model.dart';
import 'package:arzaan_sara/models/const_model/city_model.dart';
import 'package:arzaan_sara/models/const_model/vehicle_model.dart';
import 'package:arzaan_sara/resources/save.dart';
import 'package:arzaan_sara/screens/menu/menu_request_history.dart';


import 'dialog_car_type.dart';
import 'dialog_cargo_type.dart';

class DialogFilterHistory extends StatefulWidget {
  ///after click on ok button in dialog
  ///accessible from other class for do something after click on ok in that class
  Function(Map<String, String> filtesList) afterOk;

  DialogFilterHistory({this.afterOk});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return DialogFilterHistoryState();
  }
}

class DialogFilterHistoryState extends State<DialogFilterHistory> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Dialog(
          shape:
          RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
          //this right here
          child: _MyDialogContent(widget.afterOk)),
    );
  }
}

class _MyDialogContent extends StatefulWidget {
  Function(Map<String, String> filtesList) afterOk;

  _MyDialogContent(this.afterOk);

  @override
  _MyDialogContentState createState() => new _MyDialogContentState();
}

class _MyDialogContentState extends State<_MyDialogContent> {
  List<String> _cityItems = List<String>();

  String carTypeName = "نوع خودرو";
  String cargoTypeName = "نوع بار";
  String carTypeId = "0";

  String _carryDateStr = "تاریخ بارگیری";
  String _evacuationDateStr = "تاریخ تخلیه بار";

  double lowerCargoValue = 10.0;
  double upperCargoValue = 100.0;
  double initialLowerCargoValue = 0;
  double initialUpperCargoValue = 0;

  bool showRangeSlider = false;
  List<String> skillItems = List<String>();
  List<String> workscopeItems = List<String>();

  List<Map<String, dynamic>> skillsList = [];
  Map<String, dynamic> workScope = {};

  TextEditingController _originCityController = TextEditingController();
  TextEditingController _destinationCityController = TextEditingController();
  TextEditingController _simpleACWorkscopeController = TextEditingController();

  FuturedSpinnerModel _selectedValueCargoType;
  FuturedSpinnerModel _selectedValueSalary;
  FuturedSpinnerModel _selectedValueWorkTime;
  FuturedSpinnerModel _selectedValueState;
  FuturedSpinnerModel _selectedValueCity;

  Future<List<FuturedSpinnerModel>> cargoTypeData() async {
    CargoTypeModel _model = await Save().getCargoTypeData();
    List<FuturedSpinnerModel> list = List<FuturedSpinnerModel>();
    for (int i = 0; i < _model.cargoeTypes.length; i++) {
      list.add(FuturedSpinnerModel(
          _model.cargoeTypes[i].id, _model.cargoeTypes[i].name));
    }

    return list;
  }

  Future<List<FuturedSpinnerModel>> cities;

  GlobalKey<AutoCompleteTextFieldState<String>> keyOriginCity = GlobalKey();
  GlobalKey<AutoCompleteTextFieldState<String>> keyDestinationCity =
  GlobalKey();
  GlobalKey<AutoCompleteTextFieldState<String>> keyLevels = GlobalKey();
  GlobalKey<AutoCompleteTextFieldState<String>> keyWorkScope = GlobalKey();

  //////

  List<Map<String, dynamic>> images = List();

  /////
  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    setFirstData();

    checkDataInsertedBefore();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 400.0,
      padding: EdgeInsets.all(24),
      child: ListView(
        shrinkWrap: true,
        children: <Widget>[
          Align(
            alignment: Alignment.topRight,
            child: InkWell(
              child: Icon(
                Icons.clear,
              ),
              onTap: () {
                Navigator.pop(context);
              },
            ),
          ),
          TitleText("انتخاب فیلتر"),
          SizedBox(
            height: 8,
          ),
          SubtitleText("فیلتر مورد نظر خود را انتخاب نمایید"),
          SizedBox(
            height: 16,
          ),
          PersianDatePickerX(
            hint: _carryDateStr,
            onChange: (res) {
              MenuRequestHistory.carryDate.title = res;
              MenuRequestHistory.carryDate.hint = "تاریخ شروع بارگیری: " + res;
            },
          ),
          PersianDatePickerX(
            hint: _evacuationDateStr,
            onChange: (res) {
              MenuRequestHistory.evacuationDate.title = res;
              MenuRequestHistory.evacuationDate.hint = "تاریخ تخلیه بار: " + res;
            },
          ),
          SizedBox(
            height: 8,
          ),
          Directionality(
            textDirection: TextDirection.rtl,
            child: SimpleAutoCompleteTextField(
              decoration: InputDecoration(
                contentPadding: EdgeInsets.only(left: 8, right: 8),
                labelText: "شهر مبدا",
                suffixIcon: Icon(Icons.search),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(8)),
                    borderSide: BorderSide(color: Colors.black54, width: 1)),
              ),
              controller: _originCityController,
              suggestions: _cityItems,
              key: keyOriginCity,
              clearOnSubmit: false,
              textSubmitted: (text) async {
                CityModel model = await Save().getCityData();

                for (int i = 0; i < model.cities.length; i++) {
                  if (model.cities[i].name == text) {
                    MenuRequestHistory.originCity.id = model.cities[i].id.toString();
                    MenuRequestHistory.originCity.title = model.cities[i].name;
                    MenuRequestHistory.originCity.hint =
                        "شهر مبدا: " + model.cities[i].name;
                  }
                }
              },
            ),
          ),
          SizedBox(
            height: 8,
          ),
          Directionality(
            textDirection: TextDirection.rtl,
            child: SimpleAutoCompleteTextField(
              decoration: InputDecoration(
                contentPadding: EdgeInsets.only(left: 8, right: 8),
                labelText: "شهر مقصد",
                suffixIcon: Icon(Icons.search),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(8)),
                    borderSide: BorderSide(color: Colors.black54, width: 1)),
              ),
              controller: _destinationCityController,
              suggestions: _cityItems,
              key: keyDestinationCity,
              clearOnSubmit: false,
              textSubmitted: (text) async {
                CityModel model = await Save().getCityData();

                for (int i = 0; i < model.cities.length; i++) {
                  if (model.cities[i].name == text) {
                    MenuRequestHistory.destinationCity.id =
                        model.cities[i].id.toString();
                    MenuRequestHistory.destinationCity.title = model.cities[i].name;
                    MenuRequestHistory.destinationCity.hint =
                        "شهر مقصد: " + model.cities[i].name;
                  }
                }
              },
            ),
          ),
          SizedBox(
            height: 8,
          ),
          FlatButtonIcon(
            text: carTypeName,
            textColor: Colors.black,
            useIcon: true,
            fillColor: Colors.white,
            borderColor: Colors.black38,
            iconColor: Colors.black,
            icon: Icons.directions_car,
            onTap: () {
              showDialog(
                  context: context,
                  builder: (context) => DialogCarType(
                    onValueSelected: (carId, carName) {
                      MenuRequestHistory.carType.id = carId;
                      MenuRequestHistory.carType.title = carName;
                      MenuRequestHistory.carType.hint = "نوع ماشین: " + carName;
                      setState(() {
                        carTypeName = carName;
                        carTypeId = carId;
                      });
                    },
                  ));
            },
          ),
          SizedBox(
            height: 4,
          ),
          FlatButtonIcon(
            text: cargoTypeName,
            textColor: Colors.black,
            useIcon: true,
            fillColor: Colors.white,
            borderColor: Colors.black38,
            iconColor: Colors.black,
            icon: Icons.card_travel,
            onTap: () {
              showDialog(
                  context: context,
                  builder: (context) => DialogCargoType(
                    onValueSelected: (cargoId, cargoName) {
                      MenuRequestHistory.cargoType.id = cargoId;
                      MenuRequestHistory.cargoType.title = cargoName;
                      MenuRequestHistory.cargoType.hint = "نوع بار: " + cargoName;
                      setState(() {
                        cargoTypeName = cargoName;
                      });
                    },
                  ));
            },
          ),
          Row(
            children: <Widget>[
              Checkbox(
                  value: showRangeSlider,
                  onChanged: (value) {
                    if (value) {
                      setState(() {
                        showRangeSlider = true;
                        MenuRequestHistory.cargoMinWeight.title =
                            lowerCargoValue.toInt().toString();
                        MenuRequestHistory.cargoMinWeight.hint =
                            lowerCargoValue.toInt().toString();
                        MenuRequestHistory.cargoMaxWeight.title =
                            upperCargoValue.toInt().toString();
                        MenuRequestHistory.cargoMaxWeight.hint =
                            upperCargoValue.toInt().toString();

                        MenuRequestHistory.cargoMinWeight.hint = "وزن بار بین: " +
                            lowerCargoValue.toInt().toString() +
                            " تا " +
                            upperCargoValue.toInt().toString();
                      });
                    } else {
                      setState(() {
                        showRangeSlider = false;
                        MenuRequestHistory.cargoMinWeight.title = "";
                        MenuRequestHistory.cargoMinWeight.hint = "";
                        MenuRequestHistory.cargoMaxWeight.title = "";
                        MenuRequestHistory.cargoMaxWeight.hint = "";
                      });
                    }
                  }),
              Flexible(
                child: Text("وزن بار | بر حسب کیلوگرم"),
              ),
            ],
          ),
          Visibility(
            visible: showRangeSlider,
            child: RangeSliderX(
              lowerValue: lowerCargoValue,
              upperValue: upperCargoValue,
              initialLowerValue: initialLowerCargoValue,
              initialUpperValue: initialUpperCargoValue,
              divisions: 50,
              onChange: (RangeValues value) {
                MenuRequestHistory.cargoMinWeight.title =
                    value.start.toInt().toString();
                MenuRequestHistory.cargoMaxWeight.title = value.end.toInt().toString();
                MenuRequestHistory.cargoMinWeight.hint = "وزن بار بین: " +
                    value.start.toInt().toString() +
                    " تا " +
                    value.end.toInt().toString();
              },
              unit: "kg",
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 16),
          ),
          Column(
            children: <Widget>[
              EasyButton(
                onPressed: okClick,
                text: "اعمال فیلتر",
                textColor: Colors.white,
                isFilled: true,
              )
            ],
          )
        ],
      ),
    );
  }

  ////////////////////////////////// add autoComplete data //////////////////
  setFirstData() async {
    CityModel cm = await Save().getCityData();

    for (int i = 0; i < cm.cities.length; i++) {
      _cityItems.add(cm.cities[i].name);
    }
  }

  ////////////////////////////////////// widget /////////////////////////////

  //////////////////////////////// click state /////////////////////////////

  okClick() async {
    Navigator.pop(context);

    Map<String, String> filtersList = Map();

    filtersList = {
      "carTypeStr": MenuRequestHistory.carType.hint,
      "originCityStr": MenuRequestHistory.originCity.hint,
      "destinationCityStr": MenuRequestHistory.destinationCity.hint,
      "carryDateStr": MenuRequestHistory.carryDate.hint,
      "evacuationDateStr": MenuRequestHistory.evacuationDate.hint,
      "cargoTypeStr": MenuRequestHistory.cargoType.hint,
      "cargoWeightStr":  MenuRequestHistory.cargoMinWeight.hint,
    };

    widget.afterOk(filtersList);
  }

  void checkDataInsertedBefore() async {
    if (MenuRequestHistory.originCity.id != "") {}
    if (MenuRequestHistory.destinationCity.id != "0") {
      _destinationCityController.text = MenuRequestHistory.destinationCity.title;
    }
    if (MenuRequestHistory.carType.id != "0") {
      VehicleModel model = await Save().getVehiclesData();
      for (int i = 0; i < model.vehicles.length; i++) {
        if (model.vehicles[i].id == MenuRequestHistory.carType.id) {
          setState(() {
            carTypeName = model.vehicles[i].name;
          });
        }
      }
    }
    if (MenuRequestHistory.cargoType.id != "0") {
      CargoTypeModel model = await Save().getCargoTypeData();
      for (int i = 0; i < model.cargoeTypes.length; i++) {
        if (model.cargoeTypes[i].id == MenuRequestHistory.cargoType.id) {
          setState(() {
            cargoTypeName = model.cargoeTypes[i].name;
          });
        }
      }
    }

    initialLowerCargoValue = lowerCargoValue;
    initialUpperCargoValue = upperCargoValue;
    if (MenuRequestHistory.cargoMinWeight.title != "") {
      setState(() {
        showRangeSlider = true;
        initialLowerCargoValue = double.parse(MenuRequestHistory.cargoMinWeight.title);
        initialUpperCargoValue = double.parse(MenuRequestHistory.cargoMaxWeight.title);
      });
    }

    if (MenuRequestHistory.carryDate.title != "") {
      setState(() {
        _carryDateStr = MenuRequestHistory.carryDate.title;
      });
    }

    if (MenuRequestHistory.evacuationDate.title != "") {
      setState(() {
        _evacuationDateStr = MenuRequestHistory.evacuationDate.title;
      });
    }
  }
}
