import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:arzaan_sara/global/common_views/button_text.dart';
import 'package:arzaan_sara/global/common_views/title_text.dart';
import 'package:arzaan_sara/models/const_model/cargo_type_model.dart';
import 'package:arzaan_sara/models/const_model/vehicle_group_model.dart';
import 'package:arzaan_sara/models/const_model/vehicle_model.dart';


import '../resources/save.dart';

class DialogCargoType extends StatefulWidget {
  Function(String carId,String carName) onValueSelected;
  DialogCargoType({this.onValueSelected});

  @override
  State<StatefulWidget> createState() => DialogCargoTypeState();
}

class DialogCargoTypeState extends State<DialogCargoType>
    with SingleTickerProviderStateMixin {
  AnimationController controller;
  Animation<double> scaleAnimation;


  Future<List<CargoTypeDetail>> cargoTypeFutureDate;

  @override
  void initState() {
    super.initState();
    controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 550));
    scaleAnimation =
        CurvedAnimation(parent: controller, curve: Curves.fastOutSlowIn);
    controller.forward();

    cargoTypeFutureDate = getCargoTypes("null");

  }




  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: onBackPress,
      child: Directionality(
        textDirection: TextDirection.rtl,
        child: Center(
          child: Material(
            color: Colors.transparent,
            child: ScaleTransition(
              scale: scaleAnimation,
              child: new Padding(
                padding: EdgeInsets.all(32),
                child: new Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8),
                  ),
                  child: Padding(
                    padding: EdgeInsets.all(16),
                    child: new Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Align(
                          alignment: Alignment.topRight,
                          child: InkWell(
                            child: Icon(
                              Icons.clear,
                            ),
                            onTap: () {
                              Navigator.pop(context,"انتخاب نوع بار*");
                            },
                          ),
                        ),
                        dialogContent(),
                        SizedBox(height: 16,),
                        Align(
                          alignment: Alignment.centerRight,
                          child: InkWell(
                            child: ButtonText("بستن"),
                            onTap: () {
                              Navigator.pop(context,"انتخاب نوع بار*");
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ) ,
    );
  }


  Widget dialogContent() {
    return Column(
      children: <Widget>[
        TitleText("انتخاب نوع بار"),
        SizedBox(height: 16,),
        TextField(
          textAlign: TextAlign.right,
          decoration: new InputDecoration(
            hintText: "جستجو...",
            focusedBorder:
            OutlineInputBorder(borderSide: BorderSide(color: Colors.black38)),
            contentPadding: EdgeInsets.only(
                left: 8, right: 8, top: 12, bottom: 12),
            alignLabelWithHint: false,
            border: OutlineInputBorder(
                borderRadius:
                BorderRadius.all(Radius.circular(8))),
          ),
          onChanged: (value) {

            if(value.length>0){
              setState(() {
                cargoTypeFutureDate = getCargoTypes(value);
              });
            }else{
              setState(() {
                cargoTypeFutureDate = getCargoTypes("null");
              });
            }

          },
        ),
        SizedBox(
          height: 8,
        ),
        Container(
            height: 200,
            decoration: BoxDecoration(
              border: Border.all(color: Colors.black38, width: 1),
            ),
            child: FutureBuilder(
              future: cargoTypeFutureDate,
              builder: (context,
                  AsyncSnapshot<List<CargoTypeDetail>> snapshot) {
                if (snapshot.hasData) {

                  return ListView.builder(
                      itemCount: snapshot.data.length,
                      itemBuilder: (context, index) {
                        return Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: <Widget>[
                            InkWell(child: Container(
                              height: 40,
                              alignment: Alignment.center,

                              child: Text(
                                snapshot.data[index].name,
                                textAlign: TextAlign.center,
                              ),
                            ),
                              onTap: (){
                                if(widget.onValueSelected != null)
                                  widget.onValueSelected(snapshot.data[index].id,snapshot.data[index].name);

                                Navigator.pop(context,snapshot.data[index].name);

                              },),
                          ],
                        );
                      });
                }
                return Center(
                  child: CircularProgressIndicator(),
                );
              },
            ))

      ],
    );
  }


  Future<List<CargoTypeDetail>> getCargoTypes(String searchStr) async {

    CargoTypeModel _model = await Save().getCargoTypeData();

    if(searchStr != "null"){
      List<CargoTypeDetail> newItems = [];
      List<String> allNames = [];

      for(int i = 0; i< _model.cargoeTypes.length; i++){
        allNames.add(_model.cargoeTypes[i].name);
      }

      for(int i = 0; i< allNames.length; i++){
        if(allNames[i].contains(searchStr)){
          newItems.add(_model.cargoeTypes[i]);
        }


      }

      return newItems;
    }else{
      return _model.cargoeTypes;
    }

  }


  Future<bool> onBackPress() {
    Navigator.pop(context,"انتخاب نوع بار*");
  }
}