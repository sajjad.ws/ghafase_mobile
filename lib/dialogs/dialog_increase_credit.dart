import 'package:flutter/material.dart';
import 'package:arzaan_sara/global/common_views/subtitle_text.dart';
import 'package:arzaan_sara/global/common_views/title_text.dart';
import 'package:arzaan_sara/global/views/easy_flat_button.dart';
import 'package:arzaan_sara/global/views/edit_text_form.dart';
import 'package:arzaan_sara/models/user_model.dart';
import 'package:arzaan_sara/resources/save.dart';
import 'package:url_launcher/url_launcher.dart';


class DialogIncreaseCredit extends StatefulWidget {



  DialogIncreaseCredit();

  @override
  State<StatefulWidget> createState() => DialogIncreaseCreditState();
}

class DialogIncreaseCreditState extends State<DialogIncreaseCredit>
    with SingleTickerProviderStateMixin {
  AnimationController controller;
  Animation<double> scaleAnimation;


  @override
  void initState() {
    super.initState();
    controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 550));
    scaleAnimation =
        CurvedAnimation(parent: controller, curve: Curves.fastOutSlowIn);
    controller.forward();

  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    controller.dispose();
  }


  String b1 = "10000 ";
  String b2 = "50000 ";
  String b3 = "100000 ";
  String b4 = "200000 ";

  final _ctrlPrice = TextEditingController();

  String credit = "50000 تومان";
  final _formKey = GlobalKey<FormState>();



  @override
  Widget build(BuildContext context) {
    return new Directionality(
      textDirection: TextDirection.rtl,
      child: Center(
        child: Material(
          color: Colors.transparent,
          child: ScaleTransition(
            scale: scaleAnimation,
            child: new Padding(
              padding: EdgeInsets.all(32),
              child: new Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8),
                ),
                child: Padding(
                  padding: EdgeInsets.all(16),
                  child: new Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Align(
                        alignment: Alignment.topRight,
                        child: InkWell(
                          child: Icon(Icons.clear,),
                          onTap: (){
                            Navigator.pop(context);
                          },
                        ),
                      ),

                      dialogContent(),

                      SizedBox(
                        height: 24,
                      ),

                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget dialogContent() {

    return Column(
      children: <Widget>[
        TitleText(
            "افزایش اعتبار کیف پول"
        ),
        SizedBox(height: 12,),
        FutureBuilder(
          future: getUserCredit(),
          builder: (context,snapshot){
            if(snapshot.hasData)
              return RichText(
                textAlign: TextAlign.center,
                text: TextSpan(
                  text: "",
                  style: TextStyle(color: Colors.black),
                  children: <TextSpan>[
                    TextSpan(
                        text: 'موجودی کیف پول شما: ',
                        style: Theme.of(context).textTheme.title),
                    TextSpan(
                        text:  snapshot.data+" تومان",
                        style: Theme.of(context).textTheme.subtitle),
                  ],
                ),
              );
            return Text("");
          },
        ),
        SizedBox(
          height: 16,
        ),
        Container(
          height: 1,
          color: Colors.black38,
        ),
        SizedBox(
          height: 16,
        ),
        Row(
          children: <Widget>[
            flatButton(
                text: b1+" تومان" ,
                onPressed: () {
                  setState(() {
                    _ctrlPrice.text = b1;
                  });
                }),
            SizedBox(
              width: 16,
            ),
            flatButton(text:  b2+" تومان",
                onPressed: () {
                  setState(() {
                    _ctrlPrice.text = b2;
                  });
                }),
          ],
        ),
        SizedBox(
          height: 16,
        ),
        Row(
          children: <Widget>[
            flatButton(text: b3+ " تومان",
                onPressed: () {
                  setState(() {
                    _ctrlPrice.text = b3;
                  });
                }),
            SizedBox(
              width: 16,
            ),
            flatButton(text:   b4+ " تومان",
                onPressed: () {
                  setState(() {
                    _ctrlPrice.text = b4;
                  });
                }),
          ],
        ),
        SizedBox(
          height: 16,
        ),
        Container(
          height: 1,
          color: Colors.black38,
        ),
        SizedBox(
          height: 16,
        ),
        SubtitleText("مبلغ مورد نظر خود را وارد نمایید"),
        SizedBox(
          height: 16,
        ),
        Form(
          key: _formKey,
          child: Column(
            children: <Widget>[
              EditTextForm(
                controller: _ctrlPrice,
                border: true,
                hint: "مبلغ بر حسب تومان",
                textInput: TextInputType.number,
              ),
              SizedBox(
                height: 16,
              ),
              EasyFlatButton(
                formKey: _formKey,
                text: "افزایش اعتبار کیف پول",
                onPressed: () {
                  launch("https://stackoverflow.com/");
                },
              )
            ],
          ),
        )
      ],
    );
  }



  Widget flatButton({String text, VoidCallback onPressed}) {
    return Expanded(
      child: FlatButton(
        onPressed: onPressed,
        shape: OutlineInputBorder(
            borderSide: BorderSide(
                color: Theme.of(context).primaryColor),
            borderRadius: BorderRadius.circular(8)),
        padding: EdgeInsets.only(right: 8, left: 8, bottom: 12, top: 12),
        child: Text(
          text,
          style: TextStyle(color: Colors.black),
          textAlign: TextAlign.center,
        ),
      ),
    );
  }


  Future<String> getUserCredit()async{
    UserModel _um = await Save().getUserData();


    return _um.credit;
  }


}
