import 'package:flutter/material.dart';
import 'package:arzaan_sara/constants/enums.dart';
import 'package:arzaan_sara/global/common_views/button_text.dart';
import 'package:arzaan_sara/global/common_views/title_text.dart';
import 'package:arzaan_sara/global/modules/persian_date_picker_form.dart';
import 'package:arzaan_sara/global/views/easy_flat_button.dart';
import 'package:arzaan_sara/global/modules/time_picker_form.dart';



class DialogRequestExtend extends StatefulWidget {

  VoidCallback onPress;

  DialogRequestExtend({this.onPress});



  @override
  State<StatefulWidget> createState() => DialogRequestExtendState();
}

class DialogRequestExtendState extends State<DialogRequestExtend>
    with SingleTickerProviderStateMixin {
  AnimationController controller;
  Animation<double> scaleAnimation;


  final _formKey = GlobalKey<FormState>();

  FormDateController _formDateControllerStart = FormDateController();
  FormDateController _formDateControllerEnd = FormDateController();

  FormTimeController _formTimeControllerStartCarry = FormTimeController();
  FormTimeController _formTimeControllerEndCarry = FormTimeController();
  FormTimeController _formTimeControllerStartLoad = FormTimeController();
  FormTimeController _formTimeControllerEndLoad = FormTimeController();

  @override
  void initState() {
    super.initState();
    controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 550));
    scaleAnimation =
        CurvedAnimation(parent: controller, curve: Curves.fastOutSlowIn);
    controller.forward();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    controller.dispose();
  }


  @override
  Widget build(BuildContext context) {
    return new Directionality(
      textDirection: TextDirection.rtl,
      child: Center(
        child: Material(
          color: Colors.transparent,
          child: ScaleTransition(
            scale: scaleAnimation,
            child: new Padding(
              padding: EdgeInsets.all(32),
              child: new Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8),
                ),
                child: Padding(
                  padding: EdgeInsets.all(16),
                  child: new Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Align(
                        alignment: Alignment.topRight,
                        child: InkWell(
                          child: Icon(Icons.clear,),
                          onTap: (){
                            Navigator.pop(context);
                          },
                        ),
                      ),
                      
                      dialogContent(),

                      SizedBox(
                        height: 24,
                      ),
                      Row(
                        children: <Widget>[
                          Expanded(child: EasyFlatButton(
                            formKey: _formKey,
                            text: "ثبت اطلاعات",
                            onPressed: widget.onPress,
                          ),),
                          SizedBox(
                            width: 16,
                          ),
                          Expanded(child: EasyFlatButton(
                            text: "بستن",
                            onPressed: (){
                              Navigator.pop(context);
                            },
                          ),),


                        ],
                      )

                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget dialogContent() {

    return Column(
      children: <Widget>[
        TitleText(
            "تمدید درخواست"
        ),
        SizedBox(
          height: 16,
        ),
        Form(
            key: _formKey,
            child:  Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                //start
                PersianDatePickerForm(
                  initialValue: "تاریخ بارگیری*",
                  controller: _formDateControllerStart,
                  validator: (value) {
                    if (value == "تاریخ بارگیری*") {
                      return "تاریخ بارگیری باید ثبت شود";
                    }

                    return null;
                  },
                ),
                SizedBox(height: 8,),
                Row(
                  children: <Widget>[

                    TimePickerForm(
                      initialValue: "ساعت شروع*",
                      controller: _formTimeControllerStartCarry,
                      validator: (value) {
                        if (value == "ساعت شروع*") {
                          return "ساعت بارگیری باید ثبت شود";
                        }

                        return null;
                      },
                    ),
                    SizedBox(width: 8,),
                    TimePickerForm(
                      initialValue: "ساعت پایان*",
                      controller: _formTimeControllerEndCarry,
                      validator: (value) {
                        if (value == "ساعت پایان*") {
                          return "ساعت بارگیری باید ثبت شود";
                        }

                        return null;
                      },
                    ),

                  ],
                ),
                SizedBox(height: 16,),
                //end
                PersianDatePickerForm(
                  initialValue: "تاریخ تخلیه بار*",
                  controller: _formDateControllerEnd,
                  validator: (value) {
                    if (value == "تاریخ تخلیه بار*") {
                      return "تاریخ تخلیه بار باید ثبت شود";
                    }

                    return null;
                  },
                ),
                SizedBox(height: 8,),
                Row(
                  children: <Widget>[

                    TimePickerForm(
                      initialValue: "ساعت شروع*",
                      controller: _formTimeControllerStartLoad,
                      validator: (value) {
                        if (value == "ساعت شروع*") {
                          return "ساعت بارگذاری باید ثبت شود";
                        }

                        return null;
                      },
                    ),
                    SizedBox(width: 8,),
                    TimePickerForm(
                      initialValue: "ساعت پایان*",
                      controller: _formTimeControllerEndLoad,
                      validator: (value) {
                        if (value == "ساعت پایان*") {
                          return "ساعت بارگذاری باید ثبت شود";
                        }

                        return null;
                      },
                    ),

                  ],
                ),
              ],
            ))

      ],
    );
  }
}
