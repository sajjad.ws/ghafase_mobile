import 'package:flutter/material.dart';
import 'package:arzaan_sara/constants/enums.dart';
import 'package:arzaan_sara/global/common_views/button_text.dart';
import 'package:arzaan_sara/global/common_views/title_text.dart';
import 'package:arzaan_sara/global/views/easy_flat_button.dart';

import '../../resources/save.dart';
import '../../screens/splash.dart';

class DialogPaymentValidation extends StatefulWidget {
  String price;
  PaymentType type;
  VoidCallback onPress;

  DialogPaymentValidation({this.price,this.type, this.onPress});

  @override
  State<StatefulWidget> createState() => DialogPaymentValidationState();
}

class DialogPaymentValidationState extends State<DialogPaymentValidation>
    with SingleTickerProviderStateMixin {
  AnimationController controller;
  Animation<double> scaleAnimation;


  @override
  void initState() {
    super.initState();
    controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 550));
    scaleAnimation =
        CurvedAnimation(parent: controller, curve: Curves.fastOutSlowIn);
    controller.forward();
  }


  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    controller.dispose();
  }


  @override
  Widget build(BuildContext context) {
    return new Directionality(
      textDirection: TextDirection.rtl,
      child: Center(
        child: Material(
          color: Colors.transparent,
          child: ScaleTransition(
            scale: scaleAnimation,
            child: new Padding(
              padding: EdgeInsets.all(32),
              child: new Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8),
                ),
                child: Padding(
                  padding: EdgeInsets.all(16),
                  child: new Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Align(
                        alignment: Alignment.topRight,
                        child: InkWell(
                          child: Icon(Icons.clear,),
                          onTap: (){
                            Navigator.pop(context);
                          },
                        ),
                      ),
                      
                      dialogContent(),

                      SizedBox(
                        height: 24,
                      ),
                      Row(
                        children: <Widget>[
                          Expanded(child: EasyFlatButton(
                            text: "تایید و پرداخت",
                            onPressed: (){
                              Navigator.pop(context);
                              widget.onPress();
                            },
                          ),),
                          SizedBox(
                            width: 16,
                          ),
                          Expanded(child: EasyFlatButton(
                            backgroundColor: Colors.red,
                            text: "لغو",
                            onPressed: (){
                              Navigator.pop(context);
                            },
                          ),),


                        ],
                      )

                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget dialogContent() {

    return Column(
      children: <Widget>[
        TitleText(
            "تایید پرداخت"
        ),
        SizedBox(
          height: 16,
        ),
        Text("درصورت لغو درخواست مبلغ پورسانت بازگرداننده نمی شود",textAlign: TextAlign.center,)

      ],
    );
  }
}
