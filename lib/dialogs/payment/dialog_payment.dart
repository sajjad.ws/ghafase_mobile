import 'package:flutter/material.dart';
import 'package:arzaan_sara/blocs/request_bloc.dart';
import 'package:arzaan_sara/constants/enums.dart';
import 'package:arzaan_sara/dialogs/dialog_increase_credit.dart';
import 'package:arzaan_sara/global/alerts/alerts.dart';
import 'package:arzaan_sara/global/common_views/button_text.dart';
import 'package:arzaan_sara/global/common_views/title_text.dart';
import 'package:arzaan_sara/global/views/easy_flat_button.dart';
import 'package:arzaan_sara/global/views/primary_button_streamer.dart';
import 'package:arzaan_sara/models/user_model.dart';

import '../../resources/save.dart';
import '../../screens/splash.dart';
import 'dialog_payment_validation.dart';

class DialogPayment extends StatefulWidget {
  String price;
  String requestId;

  DialogPayment( {this.requestId,this.price});

  @override
  State<StatefulWidget> createState() => DialogPaymentState();
}

class DialogPaymentState extends State<DialogPayment>
    with SingleTickerProviderStateMixin {
  AnimationController controller;
  Animation<double> scaleAnimation;

  String _credit = "0";

  @override
  void initState() {
    super.initState();
    print(widget.requestId);
    controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 550));
    scaleAnimation =
        CurvedAnimation(parent: controller, curve: Curves.fastOutSlowIn);
    controller.forward();


  }

  Future<String> getCredit() async {
    UserModel um = await Save().getUserData();

    _credit = um.credit;

    return _credit;
  }

  @override
  Widget build(BuildContext context) {
    return new Directionality(
      textDirection: TextDirection.rtl,
      child: Center(
        child: Material(
          color: Colors.transparent,
          child: ScaleTransition(
            scale: scaleAnimation,
            child: new Padding(
              padding: EdgeInsets.all(32),
              child: new Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8),
                ),
                child: Padding(
                  padding: EdgeInsets.all(16),
                  child: new Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Align(
                        alignment: Alignment.topRight,
                        child: InkWell(
                          child: Icon(
                            Icons.clear,
                          ),
                          onTap: () {
                            Navigator.pop(context);
                          },
                        ),
                      ),
                      dialogContent(),
                      SizedBox(
                        height: 24,
                      ),
                      Row(
                        children: <Widget>[
                          Expanded(
                            child: EasyFlatButton(
                              text: "پرداخت آنلاین",
                              onPressed: () {
                                showDialog(
                                    context: context,
                                    builder: (context) =>
                                        DialogPaymentValidation(
                                          type: PaymentType.online,
                                          price: widget.price,
                                        ));
                              },
                            ),
                          ),
                          SizedBox(
                            width: 16,
                          ),
                          Expanded(
                            child: PrimaryButtonStreamer<String>(
                              stream: requestBlocAcceptRequestByDriver.actions,
                              backgroundColor: Theme.of(context).primaryColor,
                              text: "پرداخت از اعتبار",
                              afterFinish: (res){
                                Alerts.success(context, "پرداخت با موفقیت انجام شد");
                                requestBlocAcceptRequestByDriver.dispose();
                                Navigator.pop(context);
                              },
                              onPressed: payByWallet,
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget dialogContent() {
    return Column(
      children: <Widget>[
        TitleText("پرداخت پورسانت"),
        SizedBox(
          height: 12,
        ),
        Text(
          "موجودی کیف پول",
        ),
        SizedBox(
          height: 12,
        ),
        Container(
          padding: EdgeInsets.only(left: 24, right: 24, top: 12, bottom: 12),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(8)),
            border: Border.all(color: Colors.black38, width: 1),
          ),
          child: FutureBuilder(
            future: getCredit(),
            builder: (context,snapshot){
              if(snapshot.hasData){
                return Text("${snapshot.data} تومان");
              }
              return Text("0 تومان");
            },
          ),
        ),
        SizedBox(
          height: 12,
        ),
        InkWell(
          onTap: () {
            showDialog(
                context: context, builder: (context) => DialogIncreaseCredit());
          },
          child: ButtonText("افزایش اعتبار کیف پول"),
        ),
        SizedBox(
          height: 16,
        ),
        Container(
          height: 1,
          color: Colors.black38,
        ),
        SizedBox(
          height: 16,
        ),
        Text("مبلغ پورسانت"),
        SizedBox(
          height: 12,
        ),
        Container(
          padding: EdgeInsets.only(left: 24, right: 24, top: 12, bottom: 12),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(8)),
            border: Border.all(color: Colors.black38, width: 1),
          ),
          child: Text("${widget.price} تومان"),
        ),
      ],
    );
  }

  bool isCreditEnough() {
    int credit = int.parse(_credit);
    int price = int.parse(widget.price);

    if (credit >= price) return true;

    return false;
  }

  payByWallet() {
    if (isCreditEnough()) {


      primaryButtonStreamerState.setState(() {
        primaryButtonStreamerState.isLoading = false;
      });
      showDialog(
          context: context,
          builder: (context) => DialogPaymentValidation(
                type: PaymentType.credit,
                price: widget.price,
                onPress: () {
                  primaryButtonStreamerState.setState(() {
                    primaryButtonStreamerState.isLoading = true;
                  });

                  requestBlocAcceptRequestByDriver.fetchAcceptRequestByDriver(
                    apiAddress: "accept-request-by-driver",

                    body: {
                      "request_id" : widget.requestId
                    }

                  );


                },
              ));
    } else {
      //Alerts.error(context, "موجودی کیف پول شما کافی نیست");
    }
  }
}
