import 'package:flutter/material.dart';
import 'package:arzaan_sara/global/common_views/button_text.dart';
import 'package:arzaan_sara/global/common_views/title_text.dart';


class DialogValidation extends StatefulWidget {
  final String title;
  final String content;
  final GestureTapCallback onPress;

  DialogValidation({this.title, this.content, this.onPress});

  @override
  State<StatefulWidget> createState() => DialogValidationState();
}

class DialogValidationState extends State<DialogValidation>
    with TickerProviderStateMixin {
  AnimationController controller;
  Animation<double> scaleAnimation;


  @override
  void initState() {
    super.initState();
    controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 550));
    scaleAnimation =
        CurvedAnimation(parent: controller, curve: Curves.fastOutSlowIn);
    controller.forward();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    controller.dispose();
  }


  @override
  Widget build(BuildContext context) {
    return new Directionality(
      textDirection: TextDirection.rtl,
      child: Center(
        child: Material(
          color: Colors.transparent,
          child: ScaleTransition(
            scale: scaleAnimation,
            child: new Padding(
              padding: EdgeInsets.all(32),
              child: new Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8),
                ),
                child: Padding(
                  padding: EdgeInsets.all(16),
                  child: new Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Align(
                        alignment: Alignment.topRight,
                        child: InkWell(
                          child: Icon(Icons.clear,),
                          onTap: (){
                            Navigator.pop(context);
                          },
                        ),
                      ),
                      
                      dialogContent(),

                      SizedBox(
                        height: 24,
                      ),
                      Row(
                        children: <Widget>[

                          InkWell(child: ButtonText("بله"),
                          onTap: widget.onPress,),
                          SizedBox(width: 16,),
                          InkWell(child: ButtonText("خیر"),
                            onTap: () {
                              Navigator.pop(context);
                            },),


                        ],
                      )

                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget dialogContent() {

    return Column(
      children: <Widget>[
        TitleText(
            widget.title
        ),
        SizedBox(
          height: 16,
        ),
        Text(widget.content,textAlign: TextAlign.center,)

      ],
    );
  }
}
