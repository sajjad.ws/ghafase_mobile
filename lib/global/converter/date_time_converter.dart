import 'package:jalali_date/jalali_date.dart';
import 'package:shamsi_date/shamsi_date.dart';

class DateTimeConverter {
  static String getToJalTimeDate(String dateTime){
    PersianDate date = PersianDate.fromDateTime(DateTime.parse(dateTime));
    DateTime time = DateTime.parse(dateTime);

    String dateStr = date.year.toString()+"/"+date.month.toString()+"/"+date.day.toString();
    String timeStr = time.hour.toString()+":"+time.second.toString();

    return dateStr+" ساعت: "+timeStr;
  }

  static String getGreDate(String date){

    if(date != "null" && date.isNotEmpty){

      List<String> sp = date.split("/");

      int year = int.parse(sp[0]);
      int month = int.parse(sp[1]);
      int day = int.parse(sp[2]);

      final Jalali j1 = Jalali(year,month,day);

      String yearStr = j1.toGregorian().year.toString();
      String monthStr = j1.toGregorian().month.toString();
      String dayStr = j1.toGregorian().day.toString();

      return yearStr+"-"+monthStr+"-"+dayStr;
    }

    return "";
  }

  static String getTime(String dateTime){
    if(dateTime != "null") {
      DateTime time = DateTime.parse(dateTime);
      String timeStr = time.hour.toString() + ":" + time.second.toString();

      return timeStr;
    }

    return "-";
  }

  static String getToJalDate(String dateTime){
    if(dateTime != "null"){
      PersianDate date = PersianDate.fromDateTime(DateTime.parse(dateTime));
      String dateStr = date.year.toString()+"/"+date.month.toString()+"/"+date.day.toString();

      return dateStr;
    }

    return "-";
  }

  static String getToJalDateTime(String dateTime){
    PersianDate date = PersianDate.fromDateTime(DateTime.parse(dateTime));
    DateTime time = DateTime.parse(dateTime);
    String timeSty = time.hour.toString()+":"+time.minute.toString();
    String dateStr = date.year.toString()+"/"+date.month.toString()+"/"+date.day.toString();

    return dateStr+" | "+timeSty;
  }

  static String getToJalBetweenTimeDate(String firsTime, String secondTime){

    if(firsTime != "null" && secondTime != "null"){

      PersianDate date = PersianDate.fromDateTime(DateTime.parse(firsTime));
      DateTime time1 = DateTime.parse(firsTime);
      DateTime time2 = DateTime.parse(secondTime);

      String dateStr = date.year.toString()+"/"+date.month.toString()+"/"+date.day.toString();
      String time1Str = time1.hour.toString()+":"+time1.minute.toString();
      String time2Str = time2.hour.toString()+":"+time2.minute.toString();

      return dateStr+" "+time1Str +" الی "+ time2Str;
    }

    return "-";

  }

  static int timeDifferenceWithNow(String date, String expTime) {
    final updatedAtDateTime = DateTime.parse(date);
    final nowDateTime = DateTime.now();

    int updatedAt = (updatedAtDateTime.millisecondsSinceEpoch ~/ 1000).toInt();
    int now = (nowDateTime.millisecondsSinceEpoch ~/ 1000).toInt();
    int timeToExp = int.parse(expTime);
    int difference = (updatedAt - now) + timeToExp;
    return difference;
  }

  static String formatTime(int timeSeconds) {

    Duration duration = Duration(seconds: timeSeconds);

    return [duration.inHours, duration.inMinutes, duration.inSeconds].map((seg) => seg.remainder(60).toString().padLeft(2, '0')).join(':');
  }





}
