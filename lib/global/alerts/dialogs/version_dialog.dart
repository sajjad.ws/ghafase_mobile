import 'dart:ui';

import 'package:flutter/material.dart';

///this class use for set a comment for a service
class VersionDialog extends StatefulWidget {
  ///after click on ok button in dialog
  ///accessible from other class for do something after click on ok in that class
  Function() afterOk;
  String currentVersion;
  String newVersion;
  String url;

  VersionDialog({this.afterOk,this.currentVersion,this.newVersion,this.url});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return VersionDialogState();
  }
}

class VersionDialogState extends State<VersionDialog> {
  @override
  Widget build(BuildContext context) {
    return  Dialog(
        shape:
        RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
        //this right here
        child:  Container(
          width: 300.0,
          padding: EdgeInsets.all(24),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              Center(
                child: Directionality(
                    textDirection: TextDirection.rtl,
                    child: Icon(
                      Icons.update,
                      color: Theme.of(context).primaryColor,
                      size: 96,
                    )),
              ),
              Padding(
                padding: EdgeInsets.only(bottom: 16),
              ),
              Center(
                child: Text(
                  "برنامه خود را از نسخه "+widget.currentVersion+" به نسخه جدید "+widget.newVersion+" بروزرسانی کنید",
                  textAlign: TextAlign.center,
                ),
              ),
              Padding(
                padding: EdgeInsets.only(bottom: 16),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  InkWell(
                    onTap: okClick,
                    child: Text("دانلود"),
                  ),
                ],
              )
            ],
          ),
        ));
  }


  //////////////////////////////// click state /////////////////////////////
  okClick() async {
    widget.afterOk();
    Navigator.pop(context);
  }


}
