import 'dart:ui';

import 'package:flutter/material.dart';

///this class use for set a comment for a service
class ErrorDialog extends StatefulWidget {
  ///after click on ok button in dialog
  ///accessible from other class for do something after click on ok in that class
  Function() afterOk;

  ErrorDialog({this.afterOk});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return ErrorDialogState();
  }
}

class ErrorDialogState extends State<ErrorDialog> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Dialog(
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
        //this right here
        child: _MyDialogContent(widget.afterOk));
  }
}

class _MyDialogContent extends StatefulWidget {
  Function() afterOk;

  _MyDialogContent(this.afterOk);

  @override
  _MyDialogContentState createState() => new _MyDialogContentState();
}

class _MyDialogContentState extends State<_MyDialogContent> {

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 300.0,
      padding: EdgeInsets.all(24),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          Center(
            child: Directionality(
                textDirection: TextDirection.rtl,
                child: Icon(
                  Icons.signal_wifi_off,
                  color: Theme.of(context).errorColor,
                  size: 96,
                )),
          ),
          Padding(
            padding: EdgeInsets.only(bottom: 16),
          ),
          Center(
            child: Text(
              "ارتباط قطع است، لطفا اینترنت خود را بررسی نمایید",
              textAlign: TextAlign.center,
            ),
          ),
          Padding(
            padding: EdgeInsets.only(bottom: 16),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              InkWell(
                onTap: okClick,
                child: Text("تلاش مجدد"),
              ),
            ],
          )
        ],
      ),
    );
  }

  //////////////////////////////// widget /////////////////////////////
  //////show progress till comment insert
  Widget ProgressBar({bool visible}) {
    return Visibility(
      visible: visible,
      child: Padding(
        padding: EdgeInsets.only(left: 4, right: 4),
        child: ClipRRect(
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(8),
              bottomRight: Radius.circular(8)),
          child: LinearProgressIndicator(
            valueColor: AlwaysStoppedAnimation<Color>(
              Theme.of(context).primaryColorDark,
            ),
          ),
        ),
      ),
    );
  }

  //////////////////////////////// click state /////////////////////////////
  okClick() {

    widget.afterOk();

  }
}
