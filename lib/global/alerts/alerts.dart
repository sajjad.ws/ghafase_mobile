import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:arzaan_sara/dialogs/dialog_validation.dart';
import 'package:arzaan_sara/global/build_controll/app_builder.dart';
import 'package:arzaan_sara/global/modules/overly_alert.dart';
import 'package:arzaan_sara/screens/intro.dart';
import 'package:arzaan_sara/screens/splash.dart';

import 'package:rflutter_alert/rflutter_alert.dart';

import 'dialogs/error_dialog.dart';

class Alerts {
  Alerts.error(BuildContext context, String message) {
    OverlyAlert.show(message, context,
        duration: OverlyAlert.LENGTH_LONG,
        gravity: OverlyAlert.BOTTOM,
        backgroundColor: Colors.red,
        textColor: Colors.white);
  }

  Alerts.success(BuildContext context, String message) {
    OverlyAlert.show(message, context,
        duration: OverlyAlert.LENGTH_LONG,
        gravity: OverlyAlert.BOTTOM,
        backgroundColor: Colors.green,
        textColor: Colors.white);
  }


  Alerts.info(BuildContext context, String message) {
    OverlyAlert.show(message, context,
        duration: OverlyAlert.LENGTH_LONG,
        gravity: OverlyAlert.BOTTOM,
        backgroundColor: Colors.blue,
        textColor: Colors.white);
  }

  Alerts.netError(BuildContext context, Function afterRetry) {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) => ErrorDialog(
          afterOk: afterRetry,
        ));
  }

  Alerts.confidence(BuildContext context, {Function onOk}) {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) => DialogValidation(title: "خروج از حساب کاربری",content: "آیا مطمئنید که میخواهید از حساب کاربری خود خارج شوید؟",onPress: onOk,));

  }

  Alerts.confidenceCustom(BuildContext context, {String title,String content,Function onOk}) {
    Alert(
      context: context,
      title: title,
      desc: content,
      buttons: [
        DialogButton(
          child: Text(
            "خیر",
            style: TextStyle(color: Colors.white),
          ),
          onPressed: () => Navigator.pop(context),
          width: MediaQuery.of(context).size.width,
        ),
        DialogButton(
          child: Text(
            "بله",
            style: TextStyle(color: Colors.white),
          ),
          onPressed: onOk,
          width: 120,
        ),
      ],
    ).show();
  }

  Alerts.newAppVersion(BuildContext context,
      {Function onOk, String currentVersion, String newVersion, String url}) {
/*    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) => VersionDialog(
              currentVersion: currentVersion,
              newVersion: newVersion,
              url: url,
              afterOk: () {},
            ));*/
  }
}
