import 'package:flutter/material.dart';
import 'package:arzaan_sara/global/formatter/num_fornat.dart';

class RangeSliderX extends StatefulWidget{

  double lowerValue = 10.0;
  double upperValue = 100.0;
  double initialLowerValue = 0;
  double initialUpperValue = 0;
  Function(RangeValues) onChange;
  int divisions;
  String unit;

  RangeSliderX({@required this.lowerValue, @required this.upperValue, this.onChange, this.divisions, this.unit, this.initialLowerValue,this.initialUpperValue});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return RangeSliderXState();
  }


}



class RangeSliderXState extends State<RangeSliderX>{

  double _min;
  double _max;
  double _lowerValue;
  double _upperValue;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    _min = widget.lowerValue;
    _max = widget.upperValue;

    if(widget.initialLowerValue != null && widget.initialUpperValue != null){
      _lowerValue = widget.initialLowerValue;
      _upperValue = widget.initialUpperValue;
    }else{
      _lowerValue = widget.lowerValue;
      _upperValue = widget.upperValue;
    }




  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Column(
      children: <Widget>[
        RangeSlider(
          inactiveColor: Theme.of(context).primaryColor,
          activeColor: Theme.of(context).primaryColor,
          values: RangeValues(_lowerValue, _upperValue),
          min: _min,
          max: _max,
          divisions: 50,
          onChanged: (RangeValues value){
            setState(() {
              _lowerValue = value.start;
              _upperValue = value.end;

            });
            widget.onChange(value);
          },

        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(
              "${NumFormat().getPriceFormat(_lowerValue)} ${widget.unit}",
              textDirection: TextDirection.rtl,
              style: TextStyle(color: Colors.black38),
            ),
            Text(
              "${NumFormat().getPriceFormat(_upperValue)} ${widget.unit}",
              textDirection: TextDirection.rtl,
              style: TextStyle(color: Colors.black38),
            ),
          ],
        ),
      ],
    );
  }

}