import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';


import 'dialog_cropper.dart';
import '../overly_alert.dart';

class ImageSelector extends StatefulWidget {
  Widget child;
  Widget outputWidget;
  Function(File image) onFileAdded;
  ImageSelector({this.onFileAdded, this.child, this.outputWidget});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return ImageSelectorState();
  }
}

class ImageSelectorState extends State<ImageSelector> {

  CropController _cropController = CropController();

  File _image;


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return InkWell(
      child: widget.child,
      onTap: getImage,
    );
  }

  //////////////////////////////////// widgets //////////////////////////


  Future getImage() async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);

    if (image != null) {


      _cropController.inputFile = image;


      showDialog(
          context: context,
          builder: (BuildContext context) => DialogCropper(
              controller: _cropController,
                afterCrop: () {
                  Navigator.pop(context);

                  setState(() {
                    _image = _cropController.outputFile;
                    if((_image.lengthSync()/1024)/1024 < 3) {
                      widget.onFileAdded(_image);

                    }else{
                      OverlyAlert.show("حجم فایل بیشتر از مقدار تعیین شده است", context,
                          duration: OverlyAlert.LENGTH_LONG,
                          gravity: OverlyAlert.BOTTOM,
                          backgroundColor: Colors.red,
                          textColor: Colors.white);
                    }
                  });
                },
              ));
    }
  }



}




