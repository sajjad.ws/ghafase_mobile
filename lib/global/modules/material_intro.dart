import 'dart:math';

import 'package:flutter/material.dart';
import 'package:arzaan_sara/constants/strings.dart';
const SCALE_FRACTION = 0.85;
const FULL_SCALE = 1.0;

class MaterialIntro extends StatefulWidget {
  List<Map<String, String>> dataList;
  GestureTapCallback lastPageTap;

  MaterialIntro({this.dataList,this.lastPageTap});

  @override
  MaterialIntroState createState() => MaterialIntroState();
}

class MaterialIntroState extends State<MaterialIntro> {
  ScrollController scrollController = ScrollController();

  PageController pageControllerImage;
  PageController pageControllerText;
  SyncScrollController _syncScroller;

  double PAGER_HEIGHT;
  double PAGER_WIDTH;
  double iconSelectedHeight = 10.0;
  double iconSelectedWidth = 10.0;
  double iconDeSelectedHeight = 8.0;
  double iconDeSelectedWidth = 8.0;
  double viewPortFraction = 0.8;
  double page = 0.0;

  int currentPage;


  @override
  void initState() {
    currentPage = 0;

    pageControllerImage = PageController(
        initialPage: currentPage, viewportFraction: viewPortFraction);

    pageControllerText = PageController(
        initialPage: currentPage, viewportFraction: viewPortFraction);

    _syncScroller =
    new SyncScrollController([pageControllerImage, pageControllerText]);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    PAGER_HEIGHT = MediaQuery.of(context).size.height * 3 / 5;
    PAGER_WIDTH = MediaQuery.of(context).size.width * 6 / 7;

    return Directionality(
      textDirection: TextDirection.ltr,
      child: Scaffold(
        backgroundColor: Colors.white,
        body: Column(
          children: <Widget>[
            SizedBox(
              height: 20,
            ),
            Container(
              height: PAGER_HEIGHT,
              child: NotificationListener<ScrollNotification>(
                onNotification: (ScrollNotification notification) {
                  _syncScroller.processNotification(
                      notification, pageControllerImage);
                  if (notification is ScrollUpdateNotification) {
                    setState(() {
                      page = pageControllerImage.page;
                    });
                  }

                  return ;
                },
                child: PageView.builder(
                  onPageChanged: (pos) {
                    setState(() {
                      currentPage = pos;
                    });
                  },
                  physics: BouncingScrollPhysics(),
                  controller: pageControllerImage,
                  itemCount: widget.dataList.length,
                  itemBuilder: (context, index) {
                    final scale = max(SCALE_FRACTION,
                        (FULL_SCALE - (index - page).abs()) + viewPortFraction);
                    return imagePages(widget.dataList[index]['image'], scale);
                  },
                ),
              ),
            ),
            Center(
              child: Container(
                height: 20,
                child: ListView.builder(
                  itemBuilder: (BuildContext context, int index) {
                    return Row(
                      children: <Widget>[
                        Container(
                          child: stepsWidget(index),
                        )
                      ],
                    );
                  },
                  itemCount: widget.dataList.length,
                  scrollDirection: Axis.horizontal,
                  controller: scrollController,
                  shrinkWrap: true,
                ),
              ),
            ),
            Expanded(
              child: Container(
                child: NotificationListener<ScrollNotification>(
                  onNotification: (ScrollNotification notification) {
                    _syncScroller.processNotification(
                        notification, pageControllerText);
                    //pageControllerImage.jumpTo(pageControllerText.offset);
                    if (notification is ScrollUpdateNotification) {
                      setState(() {
                        page = pageControllerText.page;
                      });

                    }
                    return;
                  },
                  child: PageView.builder(
                    onPageChanged: (pos) {
                      setState(() {
                        currentPage = pos;
                      });
                    },
                    physics: BouncingScrollPhysics(),
                    controller: pageControllerText,
                    itemCount: widget.dataList.length,
                    itemBuilder: (context, index) {
                      final scale = max(
                          SCALE_FRACTION,
                          (FULL_SCALE - (index - page).abs()) +
                              viewPortFraction);
                      return textPages(widget.dataList[index]['text'],widget.dataList[index]['title'], scale);
                    },
                  ),
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.only(top: 8, bottom: 16, left: 16, right: 16),
              child: pageButtonControl(),
            )
          ],
        ),
      ),
    );
  }

  Widget stepsWidget(int index) {
    return Row(
      children: <Widget>[
        AnimatedContainer(
          duration: Duration(milliseconds: 150),
          curve: Curves.easeInOut,
          margin: EdgeInsets.only(right: 4, left: 4),
          height: index == page ? iconSelectedHeight : iconDeSelectedHeight,
          width: index == page ? iconSelectedWidth : iconDeSelectedWidth,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color:
            index == page ? Theme.of(context).accentColor : Colors.black26,
          ),
          child: new InkWell(
            onTap: () {
              if (pageControllerImage != null) {
                pageControllerImage.animateToPage(index,
                    duration: Duration(milliseconds: 200),
                    curve: Curves.easeInOut);
              }
            },
          ),
        ),
      ],
    );
  }

  Widget pageButtonControl() {
    if (page == widget.dataList.length - 1) {
      return endPaget();
    } else {
      return otherPaget();
    }
  }

  Widget otherPaget() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        InkWell(
          onTap: () => pageControllerImage.animateToPage(
              widget.dataList.length - 1,
              duration: Duration(milliseconds: 200),
              curve: Curves.easeInOut),
          child: Text("پرش", style: Theme.of(context).textTheme.button),
        ),
        InkWell(
          onTap: () => pageControllerImage.nextPage(
              duration: Duration(milliseconds: 200), curve: Curves.easeInOut),
          child: Text("بعدی", style: Theme.of(context).textTheme.button),
        )
      ],
    );
  }

  Widget endPaget() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        InkWell(
          onTap: widget.lastPageTap,
          child: Text("ورود", style: Theme.of(context).textTheme.button),
        )
      ],
    );
  }

  Widget imagePages(String image, double scale) {
    return Align(
      alignment: Alignment.center,
      child: Container(
        margin: EdgeInsets.only(bottom: 10),
        height: PAGER_HEIGHT * scale,
        width: PAGER_WIDTH * scale,
        child: Card(
          elevation: 4,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(16))),
          clipBehavior: Clip.antiAlias,
          child: Image.asset(
            image,
            fit: BoxFit.cover,
          ),
        ),
      ),
    );
  }

  Widget textPages(String text,String title, double scale) {
    return Container(
      margin: EdgeInsets.only(
          top: 16,
          left: MediaQuery.of(context).size.width / 11,
          right: MediaQuery.of(context).size.width / 11),
      alignment: Alignment.topCenter,
      height: 50,
      child: Column(
        children: <Widget>[
          Text(
            title,
            textAlign: TextAlign.center,
            maxLines: 1,
            textDirection: TextDirection.rtl,
            style: TextStyle(color: Theme.of(context).primaryColor,fontSize: 16,),
          ),
          SizedBox(height: 8,),
          Text(
            text,
            textAlign: TextAlign.center,
            maxLines: 4,
            textDirection: TextDirection.rtl,
            style: TextStyle(color: Color(0xFFABACAF)),
          )
        ],
      ),
    );
  }
}



class SyncScrollController {
  List<ScrollController> _registeredScrollControllers =
  new List<ScrollController>();

  ScrollController _scrollingController;
  bool _scrollingActive = false;

  SyncScrollController(List<ScrollController> controllers) {
    controllers.forEach((controller) => registerScrollController(controller));
  }

  void registerScrollController(ScrollController controller) {
    _registeredScrollControllers.add(controller);
  }

  void processNotification(
      ScrollNotification notification, ScrollController sender) {
    if (notification is ScrollStartNotification && !_scrollingActive) {
      _scrollingController = sender;
      _scrollingActive = true;
      return;
    }

    if (identical(sender, _scrollingController) && _scrollingActive) {
      if (notification is ScrollEndNotification) {
        _scrollingController = null;
        _scrollingActive = false;
        return;
      }

      if (notification is ScrollUpdateNotification) {
        _registeredScrollControllers.forEach((controller){
          if (!identical(_scrollingController, controller))
            controller..jumpTo(_scrollingController.offset);
        });
        return;
      }
    }
  }
}
