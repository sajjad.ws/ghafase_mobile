import 'package:flutter/material.dart';

class TestIconButton extends StatelessWidget {
  final GlobalKey<FormState> formKey;
  final IconData icon;
  final VoidCallback onPressed;


  TestIconButton({this.formKey, this.icon, this.onPressed});

  @override
  Widget build(BuildContext context) {


    return IconButton(
      icon: Icon(icon,color: Colors.white,),
      onPressed: () {
        if (formKey == null) {
          onPressed();
        } else {
          if (formKey.currentState.validate()) {
            onPressed();
          }
        }
      },
    );
  }


}