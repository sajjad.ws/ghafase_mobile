import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:jalali_date/jalali_date.dart';
import 'package:persian_datepicker/persian_datepicker.dart';


///this class is a custom persian date picker
class PersianDatePickerX extends StatefulWidget {
  final Function(String value) onChange;
  final String hint;

  PersianDatePickerX({this.onChange, this.hint});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return PersianDatePickerXState();
  }
}

class PersianDatePickerXState extends State<PersianDatePickerX> {


  String _hint = "";

  PersianDatePickerWidget persianDatePicker;

  PersianDatePickerWidget _openDatePicker() {
    FocusScope.of(context)
        .requestFocus(new FocusNode()); // to prevent opening default keyboard
    showModalBottomSheet(
        context: context,
        builder: (BuildContext context) {
          return persianDatePicker;
        });
  }


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _hint = widget.hint;
  }

  @override
  void didUpdateWidget(PersianDatePickerX oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
    _hint = widget.hint;
  }

  @override
  Widget build(BuildContext context) {
    persianDatePicker = PersianDatePicker(
        controller: TextEditingController(),

        datetime:  PersianDate.now().toString(),
        onChange: (String oldText, String newText) {
          Navigator.pop(context);
          setState(() {
            print("sad");
            _hint = newText;
            widget.onChange(newText);
          });
        }).init();

    return  FlatButton(
      onPressed: _openDatePicker,
      shape: OutlineInputBorder(
          borderRadius: BorderRadius.all(
              Radius.circular(8)),
          borderSide: BorderSide(color: Colors.black54)),
      padding: EdgeInsets.all(8),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[

          Icon(
            Icons.date_range,
            size: 24,
          ),
          Expanded(
              child: new Text(
                _hint,
                style: new TextStyle(
                    fontSize: Theme.of(context).textTheme.body1.fontSize),
                textAlign: TextAlign.center,
              )),
          SizedBox(width: 24,)

        ],
      ),
    );
  }
}
