import 'package:flutter/material.dart';
import 'package:arzaan_sara/global/modules/car_plate_form/car_plate_content.dart';

class CarPlateForm extends FormField<bool> {
  bool initialValue = false;
  String selectedValue = "";
  bool needValidate;
  FormCarPlateController carPlateController = FormCarPlateController();

  CarPlateForm({
    FormFieldSetter<bool> onSaved,
    FormFieldValidator<bool> validator,
    this.initialValue,
    this.carPlateController,
    this.needValidate = true,
    bool autovalidate = false,
  }) : super(
            onSaved: onSaved,
            validator: validator,
            initialValue: initialValue,
            autovalidate: autovalidate,
            builder: (FormFieldState<bool> state) {


              return Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Text(needValidate ? "شماره پلاک*" : "شماره پلاک"),
                  Container(
                    padding: EdgeInsets.all(8),
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.black38, width: 1),
                        borderRadius: BorderRadius.all(Radius.circular(8))),
                    child: CarPlateContent(

                      needValidate: needValidate,
                      state: state,
                      carPlateController: carPlateController,
                    ),
                  ),
                  SizedBox(
                    height: 4,
                  ),
                  state.hasError
                      ? Text(
                          state.errorText,
                          textAlign: TextAlign.right,
                          style: TextStyle(color: Colors.red, fontSize: 12),
                        )
                      : Container()
                ],
              );
            });

  static boredrColor(FormFieldState<String> state, String initialValue) {
    if (state.hasError && state.value == initialValue) {
      return Colors.red;
    }
    if (!state.hasError && state.value != initialValue) {
      return Theme.of(state.context).primaryColor;
    } else {
      return Colors.black45;
    }
  }
}

class FormCarPlateController {
  String _plate1 = "";
  String _plate2 = "";
  String _plate3 = "";
  String _plate4 = "";

  String get plate1 => _plate1;

  set plate1(String value) {
    _plate1 = value;
  }

  String get plate2 => _plate2;

  set plate2(String value) {
    _plate2 = value;
  }

  String get plate3 => _plate3;

  set plate3(String value) {
    _plate3 = value;
  }

  String get plate4 => _plate4;

  set plate4(String value) {
    _plate4 = value;
  }
}
