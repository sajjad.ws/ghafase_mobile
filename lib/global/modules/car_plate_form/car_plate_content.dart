import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:arzaan_sara/global/modules/car_plate_form/car_plate_form.dart';

CarPlateContentState carPlateContentState;

class CarPlateContent extends StatefulWidget {
  final carPlateController;
  final state;
  final bool needValidate;

  CarPlateContent(
      {Key key, this.carPlateController, this.state, this.needValidate = true})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return CarPlateContentState();
  }
}

class CarPlateContentState extends State<CarPlateContent> {
  final _ctrlPlate1 = TextEditingController();
  final _ctrlPlate2 = TextEditingController();
  final _ctrlPlate3 = TextEditingController();
  final _ctrlPlate4 = TextEditingController();

  final _fnPlate1 = FocusNode();
  final _fnPlate2 = FocusNode();
  final _fnPlate3 = FocusNode();
  final _fnPlate4 = FocusNode();

  int _maxLengthPlate1 = 2;
  int _maxLengthPlate2 = 3;
  int _maxLengthPlate3 = 3;
  int _maxLengthPlate4 = 2;

  FormFieldState<bool> _state;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _state = widget.state;

    if (!widget.needValidate) _state.didChange(true);
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(right: 24),
          child: Text("ایران"),
        ),
        Row(
          children: <Widget>[
            Expanded(
              child: customTextField(
                  carPlateController: widget.carPlateController,
                  hint: "11",
                  maxLength: _maxLengthPlate1,
                  controller: _ctrlPlate1,
                  preFocus: _fnPlate1,
                  nextFocus: _fnPlate2,
                  keyboardType: TextInputType.number,
                  needValidate: widget.needValidate),
            ),
            SizedBox(
              width: 8,
            ),
            Expanded(
              child: customTextField(
                  carPlateController: widget.carPlateController,
                  hint: "111",
                  maxLength: _maxLengthPlate2,
                  controller: _ctrlPlate2,
                  preFocus: _fnPlate2,
                  nextFocus: _fnPlate3,
                  keyboardType: TextInputType.number,
                  needValidate: widget.needValidate),
            ),
            SizedBox(
              width: 8,
            ),
            Expanded(
              child: customTextField(
                  carPlateController: widget.carPlateController,
                  hint: "الف",
                  maxLength: _maxLengthPlate3,
                  controller: _ctrlPlate3,
                  preFocus: _fnPlate3,
                  nextFocus: _fnPlate4,
                  keyboardType: TextInputType.text,
                  needValidate: widget.needValidate),
            ),
            SizedBox(
              width: 8,
            ),
            Expanded(
              child: customTextField(
                  carPlateController: widget.carPlateController,
                  hint: "11",
                  maxLength: _maxLengthPlate4,
                  controller: _ctrlPlate4,
                  preFocus: _fnPlate4,
                  nextFocus: _fnPlate4,
                  keyboardType: TextInputType.number,
                  needValidate: widget.needValidate),
            ),
          ],
        ),
      ],
    );
  }

  Widget customTextField(
      {FormCarPlateController carPlateController,
      String hint,
      TextEditingController controller,
      int maxLength,
      FocusNode preFocus,
      FocusNode nextFocus,
      TextInputType keyboardType,
      bool needValidate = true}) {
    return TextField(
      textAlign: TextAlign.center,
      maxLength: maxLength,
      focusNode: preFocus,
      keyboardType: keyboardType,
      controller: controller,
      decoration: InputDecoration(
        focusedBorder:
            OutlineInputBorder(borderSide: BorderSide(color: Colors.black38)),
        contentPadding: EdgeInsets.only(left: 8, right: 8, top: 12, bottom: 12),
        hintText: hint,
        border: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(8))),
      ),
      onChanged: (text) {
        bool isAccepted = _ctrlPlate1.text.length == _maxLengthPlate1 &&
            _ctrlPlate2.text.length == _maxLengthPlate2 &&
            (_ctrlPlate3.text.length == _maxLengthPlate3 ||
                _ctrlPlate3.text.length == 1) &&
            _ctrlPlate4.text.length == _maxLengthPlate4;

        print(isAccepted);
        if (isAccepted) {
          _state.didChange(true);
        } else {
          _state.didChange(false);
        }

        if (text.length == maxLength) {
          FocusScope.of(_state.context).requestFocus(nextFocus);
        }

        if (controller == _ctrlPlate1) {
          carPlateController.plate1 = text;
        }
        if (controller == _ctrlPlate2) {
          carPlateController.plate2 = text;
        }
        if (controller == _ctrlPlate3) {
          carPlateController.plate3 = text;
        }
        if (controller == _ctrlPlate4) {
          carPlateController.plate4 = text;
        }
      },
    );
  }
}
