import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:jalali_date/jalali_date.dart';
import 'package:persian_datepicker/persian_datepicker.dart';
import 'package:shamsi_date/shamsi_date.dart';
///this class is a custom persian date picker
class PersianDatePickerForm extends FormField<String> {

  String initialValue = "";
  String selectedValue = "";

  FormDateController controller = FormDateController();
  PersianDatePickerForm(
      {FormFieldSetter<String> onSaved,
      FormFieldValidator<String> validator,
      this.initialValue,
      this.controller,
      bool autovalidate = false,
      })
      : super(
            onSaved: onSaved,
            validator: validator,
            initialValue: initialValue,
            autovalidate: autovalidate,
            builder: (FormFieldState<String> state) {

              PersianDatePickerWidget persianDatePicker;


              PersianDatePickerWidget _openDatePicker() {
                FocusScope.of(state.context)
                    .requestFocus(new FocusNode()); // to prevent opening default keyboard
                showModalBottomSheet(
                    context: state.context,
                    builder: (BuildContext context) {
                      return persianDatePicker;
                    });
              }



              persianDatePicker = PersianDatePicker(
                  controller: TextEditingController(),

                  datetime:  PersianDate.now().toString(),
                  onChange: (String oldText, String newText) {
                    Navigator.pop(state.context);
                    state.didChange(newText);
                    List<String> s = newText.split("/");
                    final Jalali j1 = Jalali(PersianDate.now().year,PersianDate.now().month,PersianDate.now().day);
                    print(s[0]);
                    controller.date = newText;
                  }).init();



              return Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  FlatButton(
                    onPressed: _openDatePicker,
                    shape: OutlineInputBorder(
                        borderRadius: BorderRadius.all(
                            Radius.circular(8)),
                        borderSide: BorderSide(color: boredrColor(state,initialValue))),
                    padding: EdgeInsets.only(left: 8,right: 8,top: 12,bottom: 12),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[

                        Icon(
                          Icons.date_range,
                          size: 24,
                        ),
                        Expanded(
                            child: new Text(
                              state.value == initialValue?initialValue:state.value,
                              style:  TextStyle(
                                  fontSize: Theme.of(state.context).textTheme.body1.fontSize),
                              textAlign: TextAlign.center,
                            )),
                        SizedBox(width: 24,),
                      ],
                    ),
                  ),
                  state.hasError?
                  Text(
                    state.errorText,
                    textAlign: TextAlign.right,
                    style: TextStyle(
                        color: Colors.red,
                      fontSize: 12
                    ),
                  ) :
                  Container()
                ],
              );
            });

  static boredrColor(FormFieldState<String> state, String initialValue) {

    if(state.hasError && state.value == initialValue ){
      return Colors.red;
    }if(!state.hasError && state.value != initialValue){
      return Theme.of(state.context).primaryColor;
    }else{
      return Colors.black45;
    }


  }
}

class FormDateController{
  String _date = "";

  String get date => _date;

  set date(String value) {
    _date = value;
  }


}
