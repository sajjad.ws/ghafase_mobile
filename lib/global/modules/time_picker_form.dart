
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:jalali_date/jalali_date.dart';
import 'package:persian_datepicker/persian_datepicker.dart';
///this class is a custom persian date picker
class TimePickerForm extends FormField<String> {

  String initialValue = "";
  String selectedValue = "";
  
  

  FormTimeController controller = FormTimeController();




  TimePickerForm(
      {FormFieldSetter<String> onSaved,
      FormFieldValidator<String> validator,
      this.initialValue,
      this.controller,
      bool autovalidate = false,
      })
      : super(
            onSaved: onSaved,
            validator: validator,
            initialValue: initialValue,
            autovalidate: autovalidate,
            builder: (FormFieldState<String> state) {



              TimeOfDay _time  = TimeOfDay.now();


              Future<Null> selectTime()async{
                final TimeOfDay picked = await showTimePicker(context: state.context, initialTime: _time );

                if(picked != null){

                  _time = picked;
                  String timeStr = _time.hour.toString()+":"+_time.minute.toString();

                  state.didChange(timeStr);
                  controller.time = timeStr;
                }
              }






              return Expanded(child: Column(
                children: <Widget>[
                  FlatButton(
                    onPressed: (){
                      selectTime();
                    },
                    shape: OutlineInputBorder(
                        borderRadius: BorderRadius.all(
                            Radius.circular(8)),
                        borderSide: BorderSide(color: boredrColor(state,initialValue))),
                    padding: EdgeInsets.only(left: 8,right: 8,top: 12,bottom: 12),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[

                        Icon(
                          Icons.access_time,
                          size: 24,
                        ),
                        Expanded(
                          child: Text(
                            state.value == initialValue?initialValue:state.value,
                            style:  TextStyle(
                                fontSize: Theme.of(state.context).textTheme.body1.fontSize),
                            textAlign: TextAlign.center,
                          ),
                        ),
                        SizedBox(width: 24,),
                      ],
                    ),
                  ),
                  state.hasError?
                  Text(
                    state.errorText,
                    textAlign: TextAlign.right,
                    style: TextStyle(
                        color: Colors.red,
                        fontSize: 12
                    ),
                  ) :
                  Container()
                ],
              ),);
            });

  static boredrColor(FormFieldState<String> state, String initialValue) {

    if(state.hasError && state.value == initialValue ){
      return Colors.red;
    }if(!state.hasError && state.value != initialValue){
      return Theme.of(state.context).primaryColor;
    }else{
      return Colors.black45;
    }


  }
}

class FormTimeController{
  String _time = "";

  String get time => _time;

  set time(String value) {
    _time = value;
  }


}
