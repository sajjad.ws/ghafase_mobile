import 'package:intl/intl.dart';

///this class use for change format of digit in to special type such price
class NumFormat {
  final priceFormat = new NumberFormat("#,###", "en_US");

  getPriceFormat(num) {
    return priceFormat.format(num);
  }
}
