import 'package:flutter/material.dart';
import 'package:arzaan_sara/global/common_views/title_text.dart';
import 'package:arzaan_sara/global/views/easy_button.dart';

class ListEmptyView extends StatelessWidget{

  Function() onRefresh;

  ListEmptyView({this.onRefresh});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        TitleText("اطلاعاتی برای نمایش وجود ندارد"),
        SizedBox(height: 16,),

        EasyButton(
          isFilled: true,
          text: "بروز رسانی",
          onPressed: onRefresh,
        )
      ],
    );
  }
}