import 'package:flutter/material.dart';

class SubtitleText extends StatelessWidget{
  final String text;
  final TextAlign textAlign;

  SubtitleText(this.text,{this.textAlign = TextAlign.center});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Text(text,style: Theme.of(context).textTheme.subtitle,textAlign: textAlign,);
  }


}