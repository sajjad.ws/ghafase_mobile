import 'package:flutter/material.dart';

class TitleTextError extends StatelessWidget {
  final String text;
  final TextAlign textAlign;

  TitleTextError(this.text, {this.textAlign = TextAlign.center});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Text(
      text,
      style: TextStyle(
          fontSize: Theme.of(context).textTheme.title.fontSize,
          color: Colors.red),
      textAlign: textAlign,
    );
  }
}
