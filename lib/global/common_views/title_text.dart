import 'package:flutter/material.dart';

class TitleText extends StatelessWidget{
  final String text;
  final TextAlign textAlign;

  TitleText(this.text,{this.textAlign = TextAlign.center});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Text(text,style: Theme.of(context).textTheme.title,textAlign: textAlign,);
  }

}