import 'package:flutter/material.dart';

class ButtonText extends StatelessWidget{
  final String text;
  final TextAlign textAlign;

  ButtonText(this.text,{this.textAlign = TextAlign.center});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Text(text,style: Theme.of(context).textTheme.button,textAlign: textAlign,);
  }
}