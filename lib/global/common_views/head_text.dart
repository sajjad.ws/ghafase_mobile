import 'package:flutter/material.dart';

class HeadText extends StatelessWidget{
  final String text;
  final TextAlign textAlign;

  HeadText(this.text,{this.textAlign = TextAlign.center});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Text(text,style: Theme.of(context).textTheme.headline,textAlign: textAlign,);
  }

}