import 'package:flutter/material.dart';
import 'package:arzaan_sara/global/common_views/title_text_error.dart';
import 'package:arzaan_sara/global/views/easy_button.dart';
class ListNetError extends StatelessWidget {
  Function() onRefresh;

  ListNetError({this.onRefresh});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Icon(
          Icons.perm_scan_wifi,
          size: 102,
          color: Colors.red,
        ),
        TitleTextError("مشکلی در ارتباط با سرور بوجود آمده است"),
        SizedBox(
          height: 16,
        ),
        EasyButton(
          isFilled: true,
          text: "تلاش مجدد",
          onPressed: onRefresh,
        )
      ],
    );
  }
}
