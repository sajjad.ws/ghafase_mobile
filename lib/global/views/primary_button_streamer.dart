import 'package:flutter/material.dart';
import 'package:arzaan_sara/global/alerts/alerts.dart';

///this class is a customized flat button that use in whole of app
PrimaryButtonStreamerState primaryButtonStreamerState;
class PrimaryButtonStreamer<T> extends StatefulWidget {
  String text = "";
  Color backgroundColor;
  Function onPressed;
  GlobalKey<FormState> formKey;
  Function(AsyncSnapshot snapshot) afterFinish;
  Stream<T> stream;

  PrimaryButtonStreamer(
      {Key key,this.stream,
        this.formKey,
        this.text,
        this.onPressed,
        this.afterFinish,
        this.backgroundColor}):super(key:key);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState

    primaryButtonStreamerState = PrimaryButtonStreamerState<T>();
    return primaryButtonStreamerState;
    //return IconButtonStreamerState<T>();
  }
}

class PrimaryButtonStreamerState<T> extends State<PrimaryButtonStreamer> {
  bool isLoading = false;
  int showAlert = 0;

  PrimaryButtonStreamerState();

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return StreamBuilder<T>(
      stream: widget.stream,
      builder: (context, AsyncSnapshot<T> snapshot) {
        if(snapshot.hasError){
          showAlert++;
          isLoading = false;
          WidgetsBinding.instance.addPostFrameCallback((dur){
            if(showAlert == 1)
              Alerts.error(context, "مشکلی در ورود اطلاعات یا ارتباط با سرور بوجود آمده");

          });



          return FlatButton(
            disabledTextColor: Colors.black54,
            textColor: Colors.white,
            onPressed:
            widget.onPressed == null ? null : isLoading ? null : onPress,
            shape: RoundedRectangleBorder(
                side: BorderSide(
                    color: widget.backgroundColor == null
                        ? Theme.of(context).primaryColor
                        : widget.backgroundColor,
                    width: 1),
                borderRadius: BorderRadius.all(Radius.circular(8))),
            color: widget.backgroundColor == null
                ? Theme.of(context).primaryColor
                : widget.backgroundColor,
            padding: EdgeInsets.fromLTRB(9, 10, 9, 10),
            child: isLoading
                ? SizedBox(
              width: 25,
              height: 25,
              child: CircularProgressIndicator(),
            )
                : Text(
              widget.text,
              
            ),
          );
        }

        if (snapshot.hasData) {
          isLoading = false;
          WidgetsBinding.instance.addPostFrameCallback((res) {
            widget.afterFinish(snapshot);
          });

          return FlatButton(
            disabledTextColor: Colors.black54,
            textColor: Colors.white,
            onPressed:
            widget.onPressed == null ? null : isLoading ? null : onPress,
            shape: RoundedRectangleBorder(
                side:
                BorderSide(color: widget.backgroundColor, width: 1),
                borderRadius: BorderRadius.all(Radius.circular(8))),
            color: widget.backgroundColor,
            padding: EdgeInsets.fromLTRB(9, 10, 9, 10),
            child: isLoading
                ? SizedBox(
              width: 25,
              height: 25,
              child: CircularProgressIndicator(),
            )
                : Text(
              widget.text,
              
            ),
          );
        } else if (snapshot.hasError) {
          isLoading = false;
          return FlatButton(
            disabledTextColor: Colors.black54,
            textColor: Colors.white,
            onPressed:
            widget.onPressed == null ? null : isLoading ? null : onPress,
            shape: RoundedRectangleBorder(
                side: BorderSide(
                    color: widget.backgroundColor == null
                        ? Theme.of(context).primaryColor
                        : widget.backgroundColor,
                    width: 1),
                borderRadius: BorderRadius.all(Radius.circular(8))),
            color: widget.backgroundColor == null
                ? Theme.of(context).primaryColor
                : widget.backgroundColor,
            padding: EdgeInsets.fromLTRB(9, 10, 9, 10),
            child: isLoading
                ? SizedBox(
              width: 25,
              height: 25,
              child: CircularProgressIndicator(),
            )
                : Text(
              widget.text,
              
            ),
          );
        }
        return FlatButton(
          disabledTextColor: Colors.black54,
          textColor: Colors.white,
          onPressed:
          widget.onPressed == null ? null : isLoading ? null : onPress,
          shape: RoundedRectangleBorder(
              side: BorderSide(
                  color: widget.backgroundColor,
                  width: 1),
              borderRadius: BorderRadius.all(Radius.circular(8))),
          color: widget.backgroundColor,
          disabledColor: Colors.black54,
          padding: EdgeInsets.fromLTRB(9, 10, 9, 10),
          child: isLoading
              ? SizedBox(
            width: 25,
            height: 25,
            child: CircularProgressIndicator(
              backgroundColor: Colors.white,
            ),
          )
              : Text(
            widget.text,
            
          ),
        );
      },
    );
  }

  onPress() {
    if (widget.formKey != null) {
      if (widget.formKey.currentState.validate()) {
        // If the form is valid, display a snackbar. In the real world,
        // you'd often call a server or storage the information in a database.
        setState(() {
          isLoading = true;
        });
        showAlert = 0;
        widget.onPressed();
      }
    } else {
      setState(() {
        isLoading = true;
      });

      widget.onPressed();
    }
  }
}
