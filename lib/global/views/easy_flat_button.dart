import 'package:flutter/material.dart';

class EasyFlatButton extends StatelessWidget {
  final GlobalKey<FormState> formKey;
  final Function() onPressed;
  final String text;
  final Color textColor;
  final bool filled;
  final Color backgroundColor;

  EasyFlatButton(
      {this.formKey,
        this.onPressed,
        this.text = "",
        this.backgroundColor,
        this.textColor = Colors.white,
        this.filled = true});

  @override
  Widget build(BuildContext context) {
    return FlatButton(
      color: filled ? backgroundColor == null?Theme.of(context).primaryColor:backgroundColor : Theme.of(context).primaryColor,
      onPressed: () {
        if (formKey == null) {
          onPressed();
        } else {
          if (formKey.currentState.validate()) {
            onPressed();
          }
        }
      },
      shape: OutlineInputBorder(
          borderSide: BorderSide(
              color: backgroundColor == null?Theme.of(context).primaryColor:backgroundColor,
              width: filled?0:1),
          borderRadius: BorderRadius.circular(8)),
      padding: EdgeInsets.only(right: 11, left: 11, bottom: 10, top: 10),
      child: Text(
        text,
        style: TextStyle(color: textColor),
        textAlign: TextAlign.center,
      ),
    );
  }
}