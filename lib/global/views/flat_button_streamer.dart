import 'package:flutter/material.dart';
import 'package:arzaan_sara/global/alerts/alerts.dart';

///this class is a customized flat button that use in whole of app
class FlatButtonStreamer<T> extends StatefulWidget {
  String text = "";
  Function onPressed;
  GlobalKey<FormState> formKey;
  Function(AsyncSnapshot snapshot) afterFinish;
  Stream<T> stream;


  FlatButtonStreamer( {this.stream, this.formKey, this.text, this.onPressed,this.afterFinish});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return FlatButtonStreamerState<T>();
  }




}


class FlatButtonStreamerState<T> extends State<FlatButtonStreamer>{

  bool isLoading = false;

  FlatButtonStreamerState();




  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return StreamBuilder<T>(
      stream: widget.stream,
      builder: (context, AsyncSnapshot<T> snapshot) {

        if(snapshot.hasError && snapshot.error == "nc"){
          WidgetsBinding.instance.addPostFrameCallback((res){
            Alerts.netError(context,(){

            });
          });
          return Container();
        }

        if (snapshot.hasData) {
          isLoading = false;

          WidgetsBinding.instance.addPostFrameCallback((res) {
            widget.afterFinish(snapshot);
          });


          return InkWell(
            onTap: isLoading?null:onPress,
            child: isLoading?SizedBox(width: 25,height: 25,child: CircularProgressIndicator(),): Text(
              widget.text,
              style: TextStyle(
                  fontSize: 16
              ),
            ),
          );
        } else if (snapshot.hasError) {
          isLoading = false;
          return InkWell(
            onTap: isLoading?null:onPress,
            child: isLoading?SizedBox(width: 25,height: 25,child: CircularProgressIndicator(),): Text(
              widget.text,
              style: TextStyle(
                  fontSize: 16
              ),
            ),
          );
        }
        return InkWell(
          onTap: isLoading?null:onPress,
          child: isLoading?SizedBox(width: 25,height: 25,child: CircularProgressIndicator(),): Text(
            widget.text,
            style: TextStyle(
                fontSize: 16
            ),
          ),
        );
      },
    ) ;
  }

  onPress() {

    if(widget.formKey!=null){

      if (widget.formKey.currentState.validate()) {
        // If the form is valid, display a snackbar. In the real world,
        // you'd often call a server or save the information in a database.
        setState(() {
          isLoading = true;
        });

        widget.onPressed();
      }


    }else{

      setState(() {
        isLoading = true;
      });

      widget.onPressed();
    }




  }
}


