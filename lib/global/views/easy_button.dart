import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class EasyButton extends StatelessWidget {
  final GlobalKey<FormState> formKey;
  final String text;
  final VoidCallback onPressed;
  final bool isFilled;
  final Color textColor;


  EasyButton({this.formKey, this.text, this.onPressed, this.isFilled, this.textColor = Colors.white});

  @override
  Widget build(BuildContext context) {

    return FlatButton(onPressed: () {
      if (formKey == null) {
        onPressed();
      } else {
        if (formKey.currentState.validate()) {
          onPressed();
        }
      }
    },
        color: isFilled?Theme.of(context).accentColor:null,
        shape: OutlineInputBorder(
            borderSide: BorderSide(color: Theme
                .of(context)
                .primaryColor),
          borderRadius: BorderRadius.circular(50)
        ),
        padding: EdgeInsets.only(top: 10,bottom: 10,right: 11,left: 11),
        child: Text(text, style: TextStyle(color: textColor,fontWeight: FontWeight.bold,fontSize: 16),textDirection: TextDirection.rtl,
          textAlign: TextAlign.center, ));
  }


}