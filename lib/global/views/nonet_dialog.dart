import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class NonetDialog extends StatelessWidget {
  GestureTapCallback onPress;

  NonetDialog({this.onPress});

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.black38,
      child: Dialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(16)),
        ),
        child: Container(
          width: 300.0,
          padding: EdgeInsets.all(24),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              Center(
                child: Directionality(
                    textDirection: TextDirection.rtl,
                    child: Icon(
                      Icons.signal_wifi_off,
                      color: Theme.of(context).errorColor,
                      size: 96,
                    )),
              ),
              Padding(
                padding: EdgeInsets.only(bottom: 16),
              ),
              Center(
                child: Text(
                  "ارتباط قطع است، لطفا اینترنت خود را بررسی نمایید",
                  textAlign: TextAlign.center,
                ),
              ),
              Padding(
                padding: EdgeInsets.only(bottom: 16),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  InkWell(
                    onTap: onPress,
                    child: Text(
                      "تلاش مجدد",
                      style: Theme.of(context).textTheme.button,
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
