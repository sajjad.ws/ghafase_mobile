import 'package:flutter/material.dart';

///this class is a customized flat button that use in whole of app
class FlatButtonIcon extends StatefulWidget {
  VoidCallback onTap;
  String text = "";
  IconData icon;
  Color textColor = Colors.white;
  Color borderColor = Colors.white;
  Color fillColor = Colors.white;
  Color iconColor = Colors.white;
  bool useIcon;

  FlatButtonIcon(
      {this.onTap,
      this.text,
      this.icon = Icons.add,
      this.textColor,
      this.borderColor,
      this.iconColor =Colors.black,
      this.fillColor = Colors.white,
      this.useIcon = true});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return new FlatButtonIconState();
  }
}

class FlatButtonIconState extends State<FlatButtonIcon> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return FlatButton(
      onPressed: widget.onTap,
      color: widget.fillColor,
      shape: OutlineInputBorder(
          borderSide: BorderSide(
              color: widget.borderColor == null
                  ? Colors.white
                  : widget.borderColor),
          borderRadius: BorderRadius.circular(8)),
      padding: EdgeInsets.only(right: 8,left: 8,bottom: 10,top: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          widget.useIcon
              ? Icon(
                  widget.icon,
                  size: 24,
                  color: widget.iconColor == null
                      ? Colors.white
                      : widget.iconColor,
                )
              : SizedBox(width: 24,),
          Expanded(
            child: new Text(
              "${widget.text}",
              style: TextStyle(
                  color: widget.textColor == null
                      ? Colors.white
                      : widget.textColor),
              textAlign: TextAlign.center,
            ),
          ),
          SizedBox(width: 24,),
        ],
      ),
    );
  }
}
