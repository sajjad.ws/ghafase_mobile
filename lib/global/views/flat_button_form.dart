import 'package:flutter/material.dart';

class FlatButtonForm extends FormField<String> {
  String initialValue = "";
  String selectedValue = "";
  Future<String> Function() onPress;
  IconData icon;

  FormFlatButtonController controller = FormFlatButtonController();

  FlatButtonForm({
    FormFieldSetter<String> onSaved,
    FormFieldValidator<String> validator,
    this.initialValue,
    this.controller,
    this.onPress,
    this.icon,
    bool autovalidate = false,
  }) : super(
            onSaved: onSaved,
            validator: validator,
            initialValue: initialValue,
            autovalidate: autovalidate,
            builder: (FormFieldState<String> state) {
              return Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  FlatButton(
                    onPressed: () async {
                      String s = await onPress();

                      state.didChange(s);

                      controller.date = s;
                    },
                    shape: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(8)),
                        borderSide: BorderSide(
                            color: boredrColor(state, initialValue))),
                    padding: EdgeInsets.only(right: 8,left: 8,bottom: 10,top: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        icon != null
                            ? Icon(
                                icon,
                                size: 24,
                              )
                            : Container(),
                        Expanded(
                            child: Text(
                          state.value == initialValue
                              ? initialValue
                              : state.value,
                          style: TextStyle(
                              fontSize: Theme.of(state.context)
                                  .textTheme
                                  .body1
                                  .fontSize),
                          textAlign: TextAlign.center,
                        )),
                        SizedBox(
                          width: 24,
                        ),
                      ],
                    ),
                  ),
                  state.hasError
                      ? Text(
                          state.errorText,
                          textAlign: TextAlign.right,
                          style: TextStyle(color: Colors.red, fontSize: 12),
                        )
                      : Container()
                ],
              );
            });

  static boredrColor(FormFieldState<String> state, String initialValue) {
    if (state.hasError && state.value == initialValue) {
      return Colors.red;
    }
    if (!state.hasError && state.value != initialValue) {
      return Theme.of(state.context).primaryColor;
    } else {
      return Colors.black45;
    }
  }
}

class FormFlatButtonController {
  String _date = "";

  String get date => _date;

  set date(String value) {
    _date = value;
  }
}
