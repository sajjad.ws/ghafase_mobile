import 'package:flutter/material.dart';

class EasyButtonRounded extends StatelessWidget {
  final GlobalKey<FormState> formKey;
  final String text;
  final VoidCallback onPressed;
  final bool isFilled;
  final Color textColor;


  EasyButtonRounded({this.formKey, this.text, this.onPressed, this.isFilled, this.textColor = Colors.white});

  @override
  Widget build(BuildContext context) {

    return FlatButton(onPressed: () {
      if (formKey == null) {
        if(onPressed != null)
          onPressed();
      } else {
        if (formKey.currentState.validate()) {
          if(onPressed != null)
          onPressed();
        }
      }
    },
        color: isFilled?Theme.of(context).primaryColor:null,
        shape: OutlineInputBorder(
            borderSide: BorderSide(color: Theme
                .of(context)
                .primaryColor),
          borderRadius: BorderRadius.all(Radius.elliptical(50,50))
        ),
        padding: EdgeInsets.only(top: 10,bottom: 10,right: 11,left: 11),
        child: Text(text, style: TextStyle(color: textColor,),textDirection: TextDirection.rtl,
          textAlign: TextAlign.center,));
  }


}