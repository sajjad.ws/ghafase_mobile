import 'package:flutter/material.dart';

class CheckBoxForm extends FormField<bool> {
  bool initialValue = false;
  String selectedValue = "";
  Widget text;
  FormCheckBoxController controller = FormCheckBoxController();

  CheckBoxForm({
    FormFieldSetter<bool> onSaved,
    FormFieldValidator<bool> validator,
    this.initialValue,
    this.controller,
    this.text,
    bool autovalidate = false,
  }) : super(
            onSaved: onSaved,
            validator: validator,
            initialValue: initialValue,
            autovalidate: autovalidate,
            builder: (FormFieldState<bool> state) {
              return Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Checkbox(
                          value: controller.date,
                          onChanged: (value) {
                            controller.date = value;
                            state.didChange(value);
                          }),
                      Flexible(
                        child: text,
                      ),
                    ],
                  ),
                  state.hasError
                      ? Text(
                          state.errorText,
                          textAlign: TextAlign.right,
                          style: TextStyle(color: Colors.red, fontSize: 12),
                        )
                      : Container()
                ],
              );
            });

  static boredrColor(FormFieldState<String> state, String initialValue) {
    if (state.hasError && state.value == initialValue) {
      return Colors.red;
    }
    if (!state.hasError && state.value != initialValue) {
      return Theme.of(state.context).primaryColor;
    } else {
      return Colors.black45;
    }
  }
}

class FormCheckBoxController {
  bool _date = false;

  bool get date => _date;

  set date(bool value) {
    _date = value;
  }
}
