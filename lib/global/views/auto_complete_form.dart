import 'package:autocomplete_textfield/autocomplete_textfield.dart';
import 'package:flutter/material.dart';

class AutoCompleteForm extends FormField<String> {
  String initialValue = "";
  String selectedValue = "";
  String lableText;
  List<String> suggestions;
  Function(String text) textChange;
  Function(String text) textSubmitted;
  FocusNode focusNode = FocusNode();
  FocusNode nextFocusNode = FocusNode();
  FormAutoCompleteController controller = FormAutoCompleteController();
  TextEditingController textEditingController = TextEditingController();
  GlobalKey<AutoCompleteTextFieldState<String>> key = new GlobalKey();

  AutoCompleteForm({
    FormFieldSetter<String> onSaved,
    FormFieldValidator<String> validator,
    this.initialValue,
    this.controller,
    this.suggestions,
    this.lableText,
    this.textChange,
    this.textSubmitted,
    this.textEditingController,
    this.key,
    this.focusNode,
    this.nextFocusNode,
    bool autovalidate = false,
  }) : super(
      onSaved: onSaved,
      validator: validator,
      initialValue: initialValue,
      autovalidate: autovalidate,
      builder: (FormFieldState<String> state) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[

            SimpleAutoCompleteTextField(
              focusNode: focusNode,
              decoration: InputDecoration(
                  labelText: lableText,
                  suffixIcon: Icon(Icons.search),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                      borderSide: BorderSide(color: Colors.black54,width: 1)
                  ),

                  contentPadding: EdgeInsets.only(left: 8,right: 8,bottom: 12,top: 12)
              ),
              controller: textEditingController,
              suggestions: suggestions,
              textChanged: (text) {
                controller.data = text;
                state.didChange(text);
                textChange(text);
              },
              key: key,
              clearOnSubmit: false,
              textSubmitted: (text) async {
                if(nextFocusNode!=null)
                  FocusScope.of(state.context).requestFocus(nextFocusNode);
                controller.data = text;
                state.didChange(text);
                textSubmitted(text);

              },
            ),

            state.hasError
                ? Text(
              state.errorText,
              textAlign: TextAlign.right,
              style: TextStyle(color: Colors.red, fontSize: 12),
            )
                : Container()
          ],
        );
      });

  static boredrColor(FormFieldState<String> state, String initialValue) {
    if (state.hasError && state.value == initialValue) {
      return Colors.red;
    }
    if (!state.hasError && state.value != initialValue) {
      return Theme.of(state.context).primaryColor;
    } else {
      return Colors.black45;
    }
  }
}

class FormAutoCompleteController {
  String _data = "";

  String get data => _data;

  set data(String value) {
    _data = value;
  }
}
