import 'package:flutter/material.dart';
import 'dart:convert';
import 'my_dropdown.dart' as myDropDown;
///this class is a customized drop down button button that use in whole of app
class FuturedSpinner extends StatefulWidget {

  List<FuturedSpinnerModel> listItems = List<FuturedSpinnerModel>();
  String hint;
  bool hasBorder;
  FuturedSpinnerModel selectedValue;
  Function(FuturedSpinnerModel vlaue) onSelectedValue;
  FuturedSpinner(
      {@required this.listItems,
        this.hint,
        this.selectedValue,
        this.onSelectedValue,
        this.hasBorder = false});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState

    return new FuturedSpinnerState();
  }
}

class FuturedSpinnerState extends State<FuturedSpinner> {



  List<myDropDown.DropdownMenuItem<FuturedSpinnerModel>> _dropdownMenuItems = List<myDropDown.DropdownMenuItem<FuturedSpinnerModel>>();
  FuturedSpinnerModel _selectedValue;





  List<myDropDown.DropdownMenuItem<FuturedSpinnerModel>> buildDropdownMenuItems(List<FuturedSpinnerModel> spinnerModelList) {
    List<myDropDown.DropdownMenuItem<FuturedSpinnerModel>> items = List();
    int count = 0;
    for (var company in spinnerModelList) {

      items.add(
        myDropDown.DropdownMenuItem<FuturedSpinnerModel>(
          value: FuturedSpinnerModel(spinnerModelList[count].id,spinnerModelList[count].title),
          child: Center(child: SizedBox(
            width: 200,
            child:  Align(alignment: Alignment.centerRight,child: new Text(
              "${company.title.toString()}",
              textDirection: TextDirection.rtl,
              style: new TextStyle(color: Colors.black,),
              overflow: TextOverflow.clip,
            ),),
          ),),
        ),
      );
      count++;
    }
    return items;
  }




  @override
  void initState() {
    _dropdownMenuItems = buildDropdownMenuItems(widget.listItems);

    super.initState();
    if(widget.selectedValue != null){
      _selectedValue = widget.selectedValue;
    }
  }


  @override
  void didUpdateWidget(FuturedSpinner oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);



    setState(() {
      _dropdownMenuItems.clear();
      _dropdownMenuItems = buildDropdownMenuItems(widget.listItems);
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Directionality(
      textDirection: TextDirection.rtl,
      child: myDropDown.DropdownButtonFormField(
          items:_dropdownMenuItems,
          onChanged: onChangeValue,
          value:_selectedValue,
          decoration: InputDecoration(
              focusedBorder: widget.hasBorder?OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Theme.of(context).primaryColor,
                  ),
                  borderRadius: BorderRadius.all(
                      Radius.circular(8))):null,
              enabledBorder: widget.hasBorder?OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Theme.of(context).primaryColor,
                  ),
                  borderRadius: BorderRadius.all(
                      Radius.circular(8))):null,
              border: widget.hasBorder?OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Theme.of(context).primaryColor,
                  ),
                  borderRadius: BorderRadius.all(
                      Radius.circular(8))):null,
              labelText: widget.hint,)),
    );
  }

  void onChangeValue(FuturedSpinnerModel value) {

    setState(() {

      _selectedValue = value;
      widget.selectedValue = value;

      widget.onSelectedValue(value);
    });


  }




}


class FuturedSpinnerModel{
  String _id;
  String _title;


  FuturedSpinnerModel(this._id, this._title);

  String get title => _title;

  set title(String value) {
    _title = value;
  }

  String get id => _id;

  set id(String value) {
    _id = value;
  }


}

