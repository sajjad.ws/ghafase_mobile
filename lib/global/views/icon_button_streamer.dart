import 'package:flutter/material.dart';
import 'package:arzaan_sara/global/alerts/alerts.dart';

///this class is a customized flat button that use in whole of app
IconButtonStreamerState iconButtonStreamerState;
class IconButtonStreamer<T> extends StatefulWidget {
  Function onPressed;
  GlobalKey<FormState> formKey;
  Function(AsyncSnapshot snapshot) afterFinish;
  Stream<T> stream;
  IconData icon;
  Color iconColor;


  IconButtonStreamer(
      {this.stream,
        this.icon,
        this.iconColor,
        this.formKey,
        this.onPressed,
        this.afterFinish});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState

    iconButtonStreamerState = IconButtonStreamerState<T>();
    return iconButtonStreamerState;
    //return IconButtonStreamerState<T>();
  }
}

class IconButtonStreamerState<T> extends State<IconButtonStreamer> {
  bool isLoading = false;
  int showAlert = 0;

  IconButtonStreamerState();

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return StreamBuilder<T>(
      stream: widget.stream,
      builder: (context, AsyncSnapshot<T> snapshot) {



        if(snapshot.hasError ){
          showAlert++;
          isLoading = false;
          WidgetsBinding.instance.addPostFrameCallback((dur){
            if(showAlert == 1)
              Alerts.error(context, "مشکلی در ورود اطلاعات یا ارتباط با سرور بوجود آمده");

          });
          return IconButton(
            onPressed: isLoading ? null : onPress,
            icon: isLoading
                ? SizedBox(
              width: 25,
              height: 25,
              child: CircularProgressIndicator(
                backgroundColor: widget.iconColor,
              ),
            )
                : Icon(
              widget.icon,
              color: widget.iconColor,
            ),
            padding: EdgeInsets.fromLTRB(9, 12,
                9, 12),
          );
        }

        if (snapshot.hasData) {
          isLoading = false;

          WidgetsBinding.instance.addPostFrameCallback((res){
            widget.afterFinish(snapshot);
          });

          return IconButton(
            onPressed: isLoading ? null : onPress,
            icon: isLoading
                ? SizedBox(
              width: 25,
              height: 25,
              child: CircularProgressIndicator(
                backgroundColor: widget.iconColor,
              ),
            )
                : Icon(
              widget.icon,
              color: widget.iconColor,
            ),
            padding: EdgeInsets.fromLTRB(9, 12,
                9, 12),
          );
        } else if (snapshot.hasError) {

          return IconButton(
            onPressed: isLoading ? null : onPress,
            icon: isLoading
                ? SizedBox(
              width: 25,
              height: 25,
              child: CircularProgressIndicator(
                backgroundColor: widget.iconColor,
              ),
            )
                : Icon(
              widget.icon,
              color: widget.iconColor,
            ),
            padding: EdgeInsets.fromLTRB(9, 12,
                9, 12),
          );
        }
        return IconButton(
          onPressed: isLoading ? null : onPress,
          icon: isLoading
              ? SizedBox(
            width: 25,
            height: 25,
            child: CircularProgressIndicator(
              backgroundColor: widget.iconColor,
            ),
          )
              : Icon(
            widget.icon,
            color: widget.iconColor,
          ),
          padding: EdgeInsets.fromLTRB(9, 12,
              9, 12),
        );
      },
    );
  }

  onPress() {
    if (widget.formKey != null) {
      if (widget.formKey.currentState.validate()) {

        setState(() {
          isLoading = true;
        });

        widget.onPressed();
      }
    } else {

      setState(() {
        isLoading = true;
      });

      widget.onPressed();
    }
  }
}
