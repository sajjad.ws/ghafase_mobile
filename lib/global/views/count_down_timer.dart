import 'dart:async';

import 'package:flutter/cupertino.dart';

class CountDownTimer extends StatefulWidget {
  final String date;
  final String expTime;
  final Function(int timer) onFinish;
  final Widget Function(int timer) child;

  CountDownTimer({this.date, this.expTime, this.onFinish, this.child});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return CountDownTimerState();
  }
}

class CountDownTimerState extends State<CountDownTimer> {
  Timer _timer;
  bool isTimerFinish = false;
  int difference = 0;

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    final updatedAtDateTime = DateTime.parse(widget.date);
    final nowDateTime = DateTime.now();

    int updatedAt = (updatedAtDateTime.millisecondsSinceEpoch ~/ 1000).toInt();
    int now = (nowDateTime.millisecondsSinceEpoch ~/ 1000).toInt();
    int timeToExp = int.parse(widget.expTime);
    difference = (updatedAt - now) + timeToExp;

    _timer = Timer.periodic(
        Duration(seconds: 1),
            (Timer timer) => setState(() {
          difference--;
          print(difference);
          if (difference < 1) {
            timer.cancel();
            widget.onFinish(difference);
          }
        }));
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return widget.child(difference);
  }
}