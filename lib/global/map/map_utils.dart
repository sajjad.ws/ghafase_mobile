
import 'dart:typed_data';
import 'dart:ui' as ui;
import 'package:flutter/services.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class MapUtils{


  static Future<Marker> addMarker(LatLng cameraPosition,Marker m, MarkerId mId, String imageAddress) async {
    final Uint8List icon = await MapUtils()._getBytesFromAsset(imageAddress, 150);

    m = Marker(
      markerId: mId,
      icon: BitmapDescriptor.fromBytes(icon),
      position: LatLng(
        cameraPosition.latitude,
        cameraPosition.longitude,
      ),
    );


      return m;

  }

  static Future<Marker> addMarkerSimply(LatLng cameraPosition, MarkerId mId, String imageAddress) async {
    final Uint8List icon = await MapUtils()._getBytesFromAsset(imageAddress, 150);

    Marker m = Marker(
      markerId: mId,
      icon: BitmapDescriptor.fromBytes(icon),
      position: LatLng(
        cameraPosition.latitude,
        cameraPosition.longitude,
      ),
    );

    return m;


  }

  static removeMarker(Map<MarkerId, Marker> markers, MarkerId mId) {

      if (markers.containsKey(mId)) {
        markers.remove(mId);
      }

  }

  Future<Uint8List> _getBytesFromAsset(String path, int width) async {
    ByteData data = await rootBundle.load(path);
    ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List(),
        targetWidth: width);
    ui.FrameInfo fi = await codec.getNextFrame();
    return (await fi.image.toByteData(format: ui.ImageByteFormat.png))
        .buffer
        .asUint8List();
  }

}