import 'dart:async';

import 'package:arzaan_sara/constants/strings.dart';
import 'package:arzaan_sara/global/common_views/button_text.dart';
import 'package:arzaan_sara/global/common_views/head_text.dart';
import 'package:arzaan_sara/global/common_views/title_text_error.dart';
import 'package:arzaan_sara/global/formatter/num_fornat.dart';
import 'package:arzaan_sara/global/map/map_utils.dart';
import 'package:arzaan_sara/global/views/easy_button_rounded.dart';
import 'package:arzaan_sara/widgets/drawer.dart';
import 'package:arzaan_sara/widgets/home/appbar/home_appbar_main.dart';
import 'package:arzaan_sara/widgets/home/tabs/main_tab.dart';
import 'package:arzaan_sara/widgets/outside_clipper_appbar.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:latlong/latlong.dart' as latlng;
import 'package:liquid_pull_to_refresh/liquid_pull_to_refresh.dart';
import 'package:location/location.dart';
import 'package:arzaan_sara/blocs/logIn_bloc.dart';
import 'package:arzaan_sara/blocs/request_bloc.dart';
import 'package:arzaan_sara/constants/dummy.dart';
import 'package:arzaan_sara/constants/enums.dart';
import 'package:arzaan_sara/dialogs/dialog_filter.dart';
import 'package:arzaan_sara/global/common_views/list_empty_view.dart';
import 'package:arzaan_sara/global/common_views/list_net_error.dart';
import 'package:arzaan_sara/global/common_views/subtitle_text.dart';
import 'package:arzaan_sara/global/common_views/title_text.dart';
import 'package:arzaan_sara/global/converter/date_time_converter.dart';
import 'package:arzaan_sara/global/views/easy_button.dart';
import 'package:arzaan_sara/global/views/easy_flat_button.dart';
import 'package:arzaan_sara/global/views/flat_button_icon.dart';
import 'package:arzaan_sara/models/const_model/filter_model.dart';
import 'package:arzaan_sara/models/request_model.dart';
import 'package:arzaan_sara/resources/save.dart';
import 'package:arzaan_sara/screens/phone/phone_register.dart';
import 'package:arzaan_sara/screens/profile/profile_form.dart';
import 'package:arzaan_sara/widgets/fcm/fcm.dart';
import 'package:arzaan_sara/widgets/home/custom_appbar.dart';
import 'package:arzaan_sara/widgets/home/home_request_items.dart';
import 'dart:math';

import 'package:url_launcher/url_launcher.dart';

class HomeTest extends StatefulWidget {
  DriverVerifyType driverVerifyType;

  static FilterModel carType = FilterModel("", "");
  static FilterModel originCity = FilterModel("", "");
  static FilterModel destinationCity = FilterModel("", "");
  static FilterModel carryDate = FilterModel("", "");
  static FilterModel evacuationDate = FilterModel("", "");
  static FilterModel cargoType = FilterModel("", "");
  static FilterModel cargoMinWeight = FilterModel("", "");
  static FilterModel cargoMaxWeight = FilterModel("", "");

  HomeTest({this.driverVerifyType = DriverVerifyType.notRegister});

  @override
  State<StatefulWidget> createState() => _HomeTestState();
}

class _HomeTestState extends State<HomeTest>
    with SingleTickerProviderStateMixin {
  PageController _myPage = PageController(initialPage: 0);

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  int page = 0;

  final firebaseMessaging = FirebaseMessaging();

  int count = 19506696;
  Timer _timer;

  Marker m1;
  Marker m2;
  Marker m3;
  MarkerId mId1 = MarkerId("1");
  MarkerId mId2 = MarkerId("2");
  MarkerId mId3 = MarkerId("3");

  List<Marker> markers = [];

  GoogleMapController _controller;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    _timer = Timer.periodic(
        Duration(seconds: 1),
        (Timer timer) => setState(() {
              count--;
              if (count < 1) {
                timer.cancel();
              }
            }));

    generateFirebaseToken();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _timer.cancel();
  }

  void generateFirebaseToken() {
    firebaseMessaging.configure(
      onLaunch: (Map<String, dynamic> msg) {
        print('onCall');
        print(msg.toString());
      },
      onResume: (Map<String, dynamic> msg) {
        print('onCall2');
        print(msg.toString());
      },
      onMessage: (Map<String, dynamic> msg) {
        print(msg);
        FCM.manage(msg, context);
      },
    );

    firebaseMessaging.getToken().then((token) {
      print(token);
    });
  }

  List<Widget> items = [
    Image.asset("assets/images/banner.jpeg"),
    Image.asset("assets/images/banner.jpeg"),
    Image.asset("assets/images/banner.jpeg"),
    Image.asset("assets/images/banner.jpeg")
  ];

  @override
  Widget build(BuildContext context) {
    return Directionality(
        textDirection: TextDirection.rtl,
        child: Scaffold(
          key: _scaffoldKey,
          drawer: buildDrawerLayout(context, widget.driverVerifyType),
          /*appBar: PreferredSize(
            preferredSize: Size.fromHeight(MediaQuery.of(context).size.width/3),
            child: CustomAppbar(
              onMenuPress: () {
                _scaffoldKey.currentState.openDrawer();
              },
            ),
          ),*/
          floatingActionButtonLocation:
              FloatingActionButtonLocation.centerDocked,
          body: Stack(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(
                    top: MediaQuery.of(context).size.width * 2 / 7 -
                        MediaQuery.of(context).size.width / 12,
                    bottom: MediaQuery.of(context).size.height / 8 -
                        MediaQuery.of(context).size.width / 18),
                child: PageView(
                  controller: _myPage,
                  onPageChanged: (int) {
                    setState(() {
                      page = int;
                    });
                  },
                  children: <Widget>[
                    MainTab(),
                    storesTab(),
                    shopeTab(),
                    mapTab(),
                    profileTab(),
                  ],
                  physics:
                      NeverScrollableScrollPhysics(), // Comment this if you need to use Swipe.
                ),
              ),
              ClipPath(
                clipper: OutsideClipperAppbar(),
                child: Container(
                  height: MediaQuery.of(context).size.width * 2 / 7,
                  padding: EdgeInsets.only(bottom: 8),
                  decoration: BoxDecoration(
                      gradient: LinearGradient(
                          begin: Alignment.topRight,
                          end: Alignment.bottomLeft,
                          colors: [Strings.greenLight, Strings.greenDark])),
                  child: appBar(),
                ),
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: ClipPath(
                  clipper: _OutsideClipper(),
                  child: BottomAppBar(
                    shape: page == 0 ? null : CircularNotchedRectangle(),
                    child: Container(
                      height: MediaQuery.of(context).size.height / 8,
                      decoration: BoxDecoration(
                          gradient: LinearGradient(
                              begin: Alignment.topRight,
                              end: Alignment.bottomLeft,
                              colors: [Strings.greenLight, Strings.greenDark])),
                      child: Row(
                        mainAxisSize: MainAxisSize.max,
                        children: <Widget>[
                          navigationItems(icon: Icons.home, index: 0),
                          navigationItems(icon: Icons.bookmark, index: 1),
                          navigationItems(icon: Icons.shopping_cart, index: 2),
                          navigationItems(icon: Icons.location_on, index: 3),
                          navigationItems(icon: Icons.person, index: 4),
                        ],
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
        ));
  }

  Widget navigationItems({IconData icon, int index}) {
    return Expanded(
      child: InkWell(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            SizedBox(
              height: MediaQuery.of(context).size.width / 12,
            ),
            Icon(
              icon,
              size: page == index ? 30 : 25,
              color: page == index ? Colors.white : Colors.black38,
            ),
          ],
        ),
        onTap: () {
          setState(() {
            _myPage.animateToPage(index,
                duration: Duration(milliseconds: 200), curve: Curves.easeIn);
          });
        },
      ),
    );
  }

  Widget categoryContent(String title) {
    return Container(
      margin: EdgeInsets.all(8),
      padding: EdgeInsets.fromLTRB(24, 4, 0, 4),
      decoration: BoxDecoration(
          color: Theme.of(context).accentColor,
          border: Border.all(
            width: 0,
          ),
          borderRadius: BorderRadius.all(Radius.elliptical(50, 50))),
      child: Padding(
        padding: EdgeInsets.only(right: 24, top: 4),
        child: Text(
          title,
          style: TextStyle(color: Colors.white),
        ),
      ),
    );
  }




  Widget shopeContent() {
    return Container(
      margin: EdgeInsets.only(right: 16, left: 16, bottom: 16),
      padding: EdgeInsets.only(top: 16, bottom: 16),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(16)),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Row(
            children: <Widget>[
              Expanded(
                child: Image.asset("assets/images/product.png"),
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    HeadText(
                      "عسل آذر کندو",
                      textAlign: TextAlign.right,
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    SubtitleText(
                      "1 سهم در جلوگیری از اسراف",
                      textAlign: TextAlign.right,
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    rows("ویژگی : ", "15 kg"),
                    SizedBox(
                      height: 8,
                    ),
                    rows("فروشنده : ", "فروشگاه یاس"),
                    SizedBox(
                      height: 8,
                    ),
                    rows("تاریخ تولید : ", "1397/5/5"),
                    SizedBox(
                      height: 8,
                    ),
                    rows("تاریخ انقضا : ", "1398/5/5"),
                    Row(
                      children: <Widget>[
                        TitleText("تعداد : "),
                        new DropdownButton<String>(
                          hint: Text(" $hint "),
                          items:
                              <String>['1', '2', '3', '4'].map((String value) {
                            return new DropdownMenuItem<String>(
                              value: value,
                              child: new Text(value),
                            );
                          }).toList(),
                          onChanged: (value) {
                            setState(() {
                              hint = value;
                            });
                          },
                        )
                      ],
                    ),
                    TitleTextError(
                      "حذف",
                      textAlign: TextAlign.right,
                    ),
                  ],
                ),
              ),
            ],
          ),
          Container(
            padding: EdgeInsets.only(top: 8, bottom: 8),
            margin: EdgeInsets.only(right: 8, left: 8),
            decoration: BoxDecoration(
                color: Color(0xffE7EEE6),
                borderRadius: BorderRadius.all(Radius.circular(16))),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(left: 8, right: 8),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      Expanded(
                        child: TitleText(
                          "قیمت اصلی",
                          textAlign: TextAlign.right,
                        ),
                      ),
                      Expanded(
                        child: TitleText(
                          NumFormat().getPriceFormat(10000) + " تومان",
                          textAlign: TextAlign.left,
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  height: 1,
                  color: Colors.black12,
                  margin: EdgeInsets.only(top: 4, bottom: 8),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 8, right: 8),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      Expanded(
                        child: TitleText(
                          "تخفیف",
                          textAlign: TextAlign.right,
                        ),
                      ),
                      Expanded(
                        child: TitleText(
                          "%50",
                          textAlign: TextAlign.left,
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  height: 1,
                  color: Colors.black12,
                  margin: EdgeInsets.only(top: 4, bottom: 8),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 8, right: 8),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      Expanded(
                        child: TitleText(
                          "قیمت نهایی",
                          textAlign: TextAlign.right,
                        ),
                      ),
                      Expanded(
                        child: TitleText(
                          NumFormat().getPriceFormat(5000) + " تومان",
                          textAlign: TextAlign.left,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  String hint = "1";

  Widget rows(String title, String content) {
    return Row(
      children: <Widget>[
        TitleText(title),
        SubtitleText(content),
      ],
    );
  }

  Widget storeContentMarked() {
    return Container(
      margin: EdgeInsets.only(right: 16, bottom: 16),
      width: MediaQuery.of(context).size.width * 3 / 7,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(16)),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Stack(
            children: <Widget>[
              ClipRRect(
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(16),
                    topLeft: Radius.circular(16)),
                child: Padding(
                  padding: EdgeInsets.all(16),
                  child: Image.asset(
                    "assets/images/product.png",
                    width: MediaQuery.of(context).size.width / 3,
                    height: MediaQuery.of(context).size.width / 4,
                  ),
                ),
              ),
              Align(
                alignment: Alignment.topRight,
                child: InkWell(
                  onTap: () {},
                  child: Container(
                    margin: EdgeInsets.only(top: 4, right: 4),
                    padding: EdgeInsets.all(4),
                    decoration: BoxDecoration(
                        shape: BoxShape.circle, color: Strings.green),
                    child: Icon(
                      Icons.location_on,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            ],
          ),
          Padding(
            padding: EdgeInsets.only(right: 4),
            child: TitleText(
              "فروشگاه یاس",
              textAlign: TextAlign.right,
            ),
          ),
          Padding(
            padding: EdgeInsets.only(right: 4),
            child: SubtitleText(
              "15 محصول موجود است",
              textAlign: TextAlign.right,
            ),
          ),
          Container(
            height: 1,
            color: Colors.black12,
          ),
          SizedBox(
            height: 4,
          ),
          Padding(
            padding: EdgeInsets.only(right: 4),
            child: Text(
              "کرمان-خیابان فلسطین",
              textAlign: TextAlign.right,
            ),
          )
        ],
      ),
    );
  }

  Widget appBar() {
    if (page == 0) {
      return HomeAppbarMain();
    } else if (page == 1) {
      return Row(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(right: 8),
            child: IconButton(
              icon: Icon(
                Icons.filter_list,
                color: Colors.white,
                size: 32,
              ),
            ),
          ),
          Expanded(
            child: Padding(
              padding: EdgeInsets.only(left: 16),
              child: TextField(
                style: TextStyle(color: Colors.white),
                textAlign: TextAlign.right,
                keyboardType: TextInputType.text,
                decoration: InputDecoration(
                  suffixIcon: Icon(
                    Icons.search,
                    color: Colors.white,
                    size: 28,
                  ),
                  contentPadding: EdgeInsets.only(
                    right: 8,
                  ),
                  hintText: 'جستجو در فروشگاه ها...',
                  hintStyle: TextStyle(fontSize: 16, color: Colors.white70),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(50),
                    borderSide: BorderSide(
                      width: 0,
                      style: BorderStyle.none,
                    ),
                  ),
                  filled: true,
                  fillColor: Colors.black26,
                ),
              ),
            ),
          ),
        ],
      );
    } else if (page == 2) {
      return Row(
        children: <Widget>[
          Expanded(
            child: Padding(
              padding: EdgeInsets.only(right: 16),
              child: Text(
                "مجموع خرید شما",
                style: TextStyle(color: Colors.white),
              ),
            ),
          ),
          Expanded(
            child: Padding(
              padding: EdgeInsets.only(left: 16),
              child: Text(
                NumFormat().getPriceFormat(15000) + " تومان",
                style: TextStyle(color: Colors.white),
                textAlign: TextAlign.left,
              ),
            ),
          ),
        ],
      );
    } else if (page == 3) {
      return Row(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(right: 8),
            child: IconButton(
              icon: Icon(
                Icons.filter_list,
                color: Colors.white,
                size: 32,
              ),
            ),
          ),
          Expanded(
            child: Padding(
              padding: EdgeInsets.only(left: 16),
              child: TextField(
                style: TextStyle(color: Colors.white),
                textAlign: TextAlign.right,
                keyboardType: TextInputType.text,
                decoration: InputDecoration(
                  suffixIcon: Icon(
                    Icons.search,
                    color: Colors.white,
                    size: 28,
                  ),
                  contentPadding: EdgeInsets.only(
                    right: 8,
                  ),
                  hintText: 'جستجو در فروشگاه ها...',
                  hintStyle: TextStyle(fontSize: 16, color: Colors.white70),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(50),
                    borderSide: BorderSide(
                      width: 0,
                      style: BorderStyle.none,
                    ),
                  ),
                  filled: true,
                  fillColor: Colors.black26,
                ),
              ),
            ),
          ),
        ],
      );
    } else if (page == 4) {
      return Row(
        children: <Widget>[
          Expanded(
            child: Padding(
              padding: EdgeInsets.only(right: 16),
              child: Text(
                "اطلاعات کاربری",
                style: TextStyle(color: Colors.white),
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(right: 8),
            child: IconButton(
              icon: Icon(
                Icons.done,
                color: Colors.white,
              ),
            ),
          ),
        ],
      );
    }
  }



  Widget storesTab() {
    return Container(
      color: Strings.bgColor,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Expanded(
            child: GridView.count(
              crossAxisCount: 2,
              childAspectRatio: 0.80,
              physics: ClampingScrollPhysics(),
              children: List.generate(6, (index) {
                return storeContentMarked();
              }),
            ),
          )
        ],
      ),
    );
  }

  Widget shopeTab() {
    return Container(
      color: Strings.bgColor,
      child: Stack(
        children: <Widget>[
          ListView(
            padding: EdgeInsets.only(bottom: 52,top: 32),
            children: <Widget>[shopeContent(), shopeContent(), shopeContent()],
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child:Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.only(left: 16,right: 16),
              margin: EdgeInsets.only(bottom: 8),
              child: EasyButtonRounded(
                textColor: Colors.white,
                isFilled: true,
                text: "نهایی سازی خرید",
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget profileTab() {
    return Container(
      color: Strings.bgColor,
      child: ListView(
        padding: EdgeInsets.only(right: 32, left: 32),
        children: <Widget>[
          SizedBox(
            height: 16,
          ),
          HeadText("محسن حقیقی"),
          SizedBox(
            height: 16,
          ),
          Image.asset(
            "assets/images/recycle.jpeg",
            height: 50,
            width: 50,
          ),
          Text(
            "تعداد سهم شما در جلوگیری از اسراف",
            style: TextStyle(color: Color(0xff60952D)),
            textAlign: TextAlign.center,
          ),
          Text(
            NumFormat().getPriceFormat(1916116161),
            textAlign: TextAlign.center,
            style:
                TextStyle(color: Theme.of(context).accentColor, fontSize: 26),
          ),
          SizedBox(
            height: 16,
          ),
          Text(
            "شماره تماس ثبت شده  |  09212098662",
            textAlign: TextAlign.center,
            style: TextStyle(color: Colors.black38),
          ),
          SizedBox(
            height: 16,
          ),
          Container(
            padding: EdgeInsets.only(right: 8, left: 8, bottom: 8, top: 8),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(50)),
            ),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: Text("موجودی کیف پول"),
                ),
                Text(NumFormat().getPriceFormat(150000) + " تومان"),
                SizedBox(
                  width: 8,
                ),
                Container(
                  padding: EdgeInsets.all(6),
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Strings.green,
                  ),
                  child: Icon(
                    Icons.add,
                    color: Colors.white,
                  ),
                )
              ],
            ),
          ),
          SizedBox(
            height: 16,
          ),
          Container(
            padding: EdgeInsets.only(right: 8, left: 8),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(50)),
            ),
            child: Row(
              children: <Widget>[
                Text("شهر: "),
                Expanded(
                  child: TextField(
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.all(0),
                      border: InputBorder.none,
                    ),
                  ),
                ),
                SizedBox(
                  width: 8,
                ),
                Icon(
                  Icons.edit,
                )
              ],
            ),
          ),
          SizedBox(
            height: 16,
          ),
          Container(
            padding: EdgeInsets.only(right: 8, left: 8),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(50)),
            ),
            child: Row(
              children: <Widget>[
                Text("آدرس: "),
                Expanded(
                  child: TextField(
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.all(0),
                      border: InputBorder.none,
                    ),
                  ),
                ),
                SizedBox(
                  width: 8,
                ),
                Icon(
                  Icons.edit,
                )
              ],
            ),
          ),
          SizedBox(
            height: 32,
          ),
          EasyButtonRounded(
            textColor: Colors.white,
            isFilled: true,
            text: "مشاهده سوابق خرید",
          ),
          SizedBox(
            height: 8,
          ),
          InkWell(
            child: ButtonText("درباره ما"),
            onTap: () {
              launch("http://ghafase.app-esperlos.ir/about-us/");
            },
          )
        ],
      ),
    );
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;

  mapTab() {
    return Stack(
      children: <Widget>[
        GoogleMap(
          onMapCreated: _onMapCreated,
          initialCameraPosition: CameraPosition(
            target: LatLng(30.2222222, 57.22222),
            zoom: 15.0,
          ),
        ),
        Align(
          alignment: Alignment.bottomCenter,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Container(
                alignment: Alignment.centerRight,
                margin: EdgeInsets.only(right: 24),
                child: FloatingActionButton(
                  onPressed: () async {
                    var location = new Location();
                    LocationData currentLocation = await location.getLocation();

                    if (_controller != null) {
                      _controller.animateCamera(CameraUpdate.newLatLng(LatLng(
                          currentLocation.latitude,
                          currentLocation.longitude)));
                    }
                  },
                  child: Icon(
                    Icons.my_location,
                    color: Colors.black,
                  ),
                  backgroundColor: Colors.white,
                ),
              ),
              SizedBox(
                height: 24,
              ),
            ],
          ),
        ),
      ],
    );
  }

  void _onMapCreated(GoogleMapController controller) async {
    _controller = controller;
  }
}

class _OutsideClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = Path();

    path.lineTo(0, size.height);
    path.lineTo(size.width, size.height);
    path.lineTo(size.width, 0);

    var firstCurvePointEnd = Offset(size.width * .92, size.height * .40);
    var firstCurveController = Offset(size.width + 8, size.height * .40);
    path.quadraticBezierTo(firstCurveController.dx, firstCurveController.dy,
        firstCurvePointEnd.dx, firstCurvePointEnd.dy);

    path.lineTo(size.width * .08, size.height * .40);

    var secondCurvePointEnd = Offset(0, 0);
    var secondCurveController = Offset(-8, size.height * .40);
    path.quadraticBezierTo(secondCurveController.dx, secondCurveController.dy,
        secondCurvePointEnd.dx, secondCurvePointEnd.dy);
/*    var firstCurvePointEnd = Offset(size.width * .4, 0);
    var firstCurveController = Offset(size.width * .3, size.height * .85);
    path.quadraticBezierTo(firstCurveController.dx, firstCurveController.dy,
        firstCurvePointEnd.dx, firstCurvePointEnd.dy);

    path.lineTo(size.width, 0);*/
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => true;
}
