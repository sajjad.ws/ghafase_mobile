import 'dart:async';

import 'package:arzaan_sara/constants/strings.dart';
import 'package:arzaan_sara/global/common_views/button_text.dart';
import 'package:arzaan_sara/global/common_views/head_text.dart';
import 'package:arzaan_sara/global/common_views/title_text_error.dart';
import 'package:arzaan_sara/global/formatter/num_fornat.dart';
import 'package:arzaan_sara/global/map/map_utils.dart';
import 'package:arzaan_sara/global/modules/material_intro.dart';
import 'package:arzaan_sara/global/views/easy_button_rounded.dart';
import 'package:arzaan_sara/screens/home.dart';
import 'package:arzaan_sara/tests/login_form.dart';
import 'package:arzaan_sara/tests/material_intro_test.dart';
import 'package:arzaan_sara/widgets/drawer.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:latlong/latlong.dart' as latlng;
import 'package:liquid_pull_to_refresh/liquid_pull_to_refresh.dart';
import 'package:location/location.dart';
import 'package:arzaan_sara/blocs/logIn_bloc.dart';
import 'package:arzaan_sara/blocs/request_bloc.dart';
import 'package:arzaan_sara/constants/dummy.dart';
import 'package:arzaan_sara/constants/enums.dart';
import 'package:arzaan_sara/dialogs/dialog_filter.dart';
import 'package:arzaan_sara/global/common_views/list_empty_view.dart';
import 'package:arzaan_sara/global/common_views/list_net_error.dart';
import 'package:arzaan_sara/global/common_views/subtitle_text.dart';
import 'package:arzaan_sara/global/common_views/title_text.dart';
import 'package:arzaan_sara/global/converter/date_time_converter.dart';
import 'package:arzaan_sara/global/views/easy_button.dart';
import 'package:arzaan_sara/global/views/easy_flat_button.dart';
import 'package:arzaan_sara/global/views/flat_button_icon.dart';
import 'package:arzaan_sara/models/const_model/filter_model.dart';
import 'package:arzaan_sara/models/request_model.dart';
import 'package:arzaan_sara/resources/save.dart';
import 'package:arzaan_sara/screens/phone/phone_register.dart';
import 'package:arzaan_sara/screens/profile/profile_form.dart';
import 'package:arzaan_sara/widgets/fcm/fcm.dart';
import 'package:arzaan_sara/widgets/home/custom_appbar.dart';
import 'package:arzaan_sara/widgets/home/home_request_items.dart';
import 'dart:math';

class TestWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _TestWidgetState();
}

class _TestWidgetState extends State<TestWidget>
    with SingleTickerProviderStateMixin {
  String image = "assets/images/login_1.jpeg";
  String title = "خوش آمدید";
  String subTitle = "لطفا جهت ورود به نرم افزار شماره همراه خود وارد نمایید.";
  String phone = "";
  String strHint = "شماره همراه";
  String strBtn = "دریافت کد فعال سازی";
  final _ctrlPhone = TextEditingController();
  int step = 0;
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Material(
      color: Strings.bgColor,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          SizedBox(
            height: 32,
          ),
          Image.asset(
            image,
            height: MediaQuery.of(context).size.height * 2 / 5,
          ),
          SizedBox(
            height: 28,
          ),
          HeadText(title),
          SizedBox(
            height: 16,
          ),
          SubtitleText(subTitle),
          SizedBox(
            height: 8,
          ),
          Padding(
            padding: EdgeInsets.only(left: 24, right: 24),
            child: TextField(
              controller: _ctrlPhone,
              style: TextStyle(color: Colors.black),
              textAlign: TextAlign.right,
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                contentPadding: EdgeInsets.only(
                  right: 8,
                ),
                hintText: strHint,
                hintStyle: TextStyle(fontSize: 16, color: Colors.black45),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(50),
                  borderSide: BorderSide(
                    width: 0,
                    style: BorderStyle.none,
                  ),
                ),
                filled: true,
                fillColor: Colors.white,
              ),
            ),
          ),
          SizedBox(
            height: 8,
          ),
          Padding(
            padding: EdgeInsets.only(right: 24, left: 24),
            child: EasyButtonRounded(
              text: strBtn,
              isFilled: true,
              textColor: Colors.white,
              onPressed: onPress,
            ),
          )
        ],
      ),
    );
  }

  onPress() {

    if(step == 0){

      step0();
    }else{
      step1();
    }



  }


  step0(){
    setState(() {
      image = "assets/images/login_2.jpeg";
      title = "دریافت کد فعال سازی";
      subTitle = "کد شما به شماره تلفن $phone ارسال شده است";
      strHint = "کد تایید";
      strBtn = "تایید";
      step = 1;
      _ctrlPhone.text = phone;
    });
  }
  step1(){
    setState(() {
      Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>LoginFrom()));
    });
  }
}
