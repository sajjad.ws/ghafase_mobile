
import 'package:arzaan_sara/tests/intro_test.dart';
import 'package:arzaan_sara/tests/login_phone.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:arzaan_sara/blocs/logIn_bloc.dart';
import 'package:arzaan_sara/constants/enums.dart';
import 'package:arzaan_sara/global/views/nonet_dialog.dart';
import 'package:arzaan_sara/models/const_model/initial_data_model.dart';
import 'package:arzaan_sara/models/user_model.dart';
import 'package:arzaan_sara/resources/save.dart';
import 'package:arzaan_sara/screens/intro.dart';
import 'package:arzaan_sara/screens/phone/phone_register.dart';


class SplashTest extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return SplashTestState();
  }
}

class SplashTestState extends State<SplashTest> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    Future.delayed(Duration(milliseconds: 5000),(){

       Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>IntroTest()));
    });
  }


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Image.asset("assets/images/splash.png",fit: BoxFit.cover,),
      ),
    );
  }
 
}
