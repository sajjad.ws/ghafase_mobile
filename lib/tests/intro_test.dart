import 'dart:async';

import 'package:arzaan_sara/constants/strings.dart';
import 'package:arzaan_sara/global/common_views/button_text.dart';
import 'package:arzaan_sara/global/common_views/head_text.dart';
import 'package:arzaan_sara/global/common_views/title_text_error.dart';
import 'package:arzaan_sara/global/formatter/num_fornat.dart';
import 'package:arzaan_sara/global/map/map_utils.dart';
import 'package:arzaan_sara/global/modules/material_intro.dart';
import 'package:arzaan_sara/global/views/easy_button_rounded.dart';
import 'package:arzaan_sara/screens/home.dart';
import 'package:arzaan_sara/tests/login_phone.dart';
import 'package:arzaan_sara/tests/material_intro_test.dart';
import 'package:arzaan_sara/widgets/drawer.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:latlong/latlong.dart' as latlng;
import 'package:liquid_pull_to_refresh/liquid_pull_to_refresh.dart';
import 'package:location/location.dart';
import 'package:arzaan_sara/blocs/logIn_bloc.dart';
import 'package:arzaan_sara/blocs/request_bloc.dart';
import 'package:arzaan_sara/constants/dummy.dart';
import 'package:arzaan_sara/constants/enums.dart';
import 'package:arzaan_sara/dialogs/dialog_filter.dart';
import 'package:arzaan_sara/global/common_views/list_empty_view.dart';
import 'package:arzaan_sara/global/common_views/list_net_error.dart';
import 'package:arzaan_sara/global/common_views/subtitle_text.dart';
import 'package:arzaan_sara/global/common_views/title_text.dart';
import 'package:arzaan_sara/global/converter/date_time_converter.dart';
import 'package:arzaan_sara/global/views/easy_button.dart';
import 'package:arzaan_sara/global/views/easy_flat_button.dart';
import 'package:arzaan_sara/global/views/flat_button_icon.dart';
import 'package:arzaan_sara/models/const_model/filter_model.dart';
import 'package:arzaan_sara/models/request_model.dart';
import 'package:arzaan_sara/resources/save.dart';
import 'package:arzaan_sara/screens/phone/phone_register.dart';
import 'package:arzaan_sara/screens/profile/profile_form.dart';
import 'package:arzaan_sara/widgets/fcm/fcm.dart';
import 'package:arzaan_sara/widgets/home/custom_appbar.dart';
import 'package:arzaan_sara/widgets/home/home_request_items.dart';
import 'dart:math';

class IntroTest extends StatefulWidget {



  @override
  State<StatefulWidget> createState() => _IntroTestState();
}

class _IntroTestState extends State<IntroTest>
    with SingleTickerProviderStateMixin {

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
        padding: EdgeInsets.only(top: 8,),
        child:MaterialIntroTest(
          dataList: [
            {'image': "assets/images/sp1.png", 'text': "فقسه به شما این امکان را می دهد تا در کمترین زمان ممکن ارزان ترین خرید را داشته باشید",'title': "تا به حال یک خرید با تخفیف %50 رو تجربه کردی؟"},
            {'image': "assets/images/sp2.png", 'text': "با استفاده از قفسه و خرید ارزان مواد غذایی که نزدیک به تاریخ انقضاء هستند، میتونید نقش مهمی در جلوگیری از این اتفاق داشته باشید",'title': "میدونستی هر سال 33% مواد غذایی تولید شده در کره زمین تبدیل به زباله میشه؟"},
            {'image': "assets/images/sp3.png", 'text': "قفسه یک راه حل مناسب برای احیای درصد زیادی از منابع و سرمایه های کشور است",'title': "واقعا برات مهم نیست منابع کشور در حال تموم شدنه؟"},
          ],
          lastPageTap: () {
            Save().setFirstTime(false);
            Navigator.pushReplacement(
                context, MaterialPageRoute(builder: (context) => LoginPhone()));
          },
        )
    );
  }


}
