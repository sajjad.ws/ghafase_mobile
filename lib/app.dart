import 'package:arzaan_sara/screens/phone/phone_register.dart';
import 'package:arzaan_sara/screens/product/product_detail.dart';
import 'package:arzaan_sara/tests/TestWidgets.dart';
import 'package:arzaan_sara/tests/home_test.dart';
import 'package:arzaan_sara/tests/splash_test.dart';
import 'package:flutter/material.dart';
import 'package:arzaan_sara/screens/menu/menu_messages.dart';
import 'package:arzaan_sara/screens/menu/menu_request_history.dart';
import 'package:arzaan_sara/screens/menu/menu_transactions.dart';
import 'package:arzaan_sara/screens/profile/profile_form.dart';
import 'package:arzaan_sara/screens/splash.dart';
import 'package:flutter/services.dart';

import 'constants/enums.dart';
import 'constants/strings.dart';
import 'global/build_controll/app_builder.dart';
import 'screens/home.dart';
import 'screens/profile/profile_show.dart';

class App extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
        statusBarColor: Colors.transparent
    ));
    return AppBuilder(
        builder: (context) => Directionality(
              textDirection: TextDirection.rtl,
              child: MaterialApp(
                title: Strings.AppName,
                theme: ThemeData(
                  fontFamily: 'IranSans',
                  backgroundColor: Color(0xFFF5F6F8),
                  primaryColor: Color(0xFF0D3654),
                  accentColor: Color(0xFF010718),
                  textTheme: TextTheme(
                    button: TextStyle(fontSize: 14, color: Color(0xFF0D3654)),
                    title: TextStyle(fontSize: 14, color: Color(0xff435138),fontWeight: FontWeight.bold),
                    subtitle: TextStyle(fontSize: 13, color: Colors.black45),
                    headline: TextStyle(fontSize: 16, color: Color(0xff435138),fontWeight: FontWeight.bold),
                  ),
                ),
                home: Splash(),
              ),
            ));
  }
}
