import 'dart:convert';

import 'package:arzaan_sara/models/product_model.dart';
import 'package:http/http.dart';
import 'package:arzaan_sara/global/check/bloc_check.dart';
import 'package:arzaan_sara/global/check/checks.dart';
import 'package:arzaan_sara/models/request_model.dart';
import 'package:rxdart/rxdart.dart';

import '../resources/repository.dart';

class ProductBloc<T> {
  Repository _repository = Repository<T>();

  BehaviorSubject _publishSubject = BehaviorSubject<T>();

  ValueObservable<T> get actions => _publishSubject.stream;

  fetchProducts(
      {String apiAddress,
      var dummy,
      Map<String, String> body,
      Function(String) directResult,
      Function(Response) fullResponse}) async {
    _publishSubject.add(null);

    if (dummy != null) {

    } else {
      T model = await _repository.fetchProducts(apiAddress, body, (res) {
        if (BlocCheck.hasError(res) == null) {
          if (directResult != null) directResult(res);
          return ProductModel.fromJson(json.decode(res));
        } else {
          _publishSubject.addError(BlocCheck.hasError(res));
          return null;
        }
      }, fullResponse);

      if (model != null) {
        _publishSubject.add(model);
      }
    }
  }

  fetchAddToCard(
      {String apiAddress,
        var dummy,
        Map<String, String> body,
        Function(String) directResult,
        Function(Response) fullResponse}) async {
    _publishSubject.add(null);

    if (dummy != null) {

    } else {
      T model = await _repository.fetchAddToCard(apiAddress, body, (res) {
        if (BlocCheck.hasError(res) == null) {
          if (directResult != null) directResult(res);
          return ProductModel.fromJson(json.decode(res));
        } else {
          _publishSubject.addError(BlocCheck.hasError(res));
          return null;
        }
      }, fullResponse);

      if (model != null) {
        _publishSubject.add(model);
      }
    }
  }


  dispose() {
    _publishSubject.add(null);
  }
}

final productBlocProducts = ProductBloc<ProductModel>();
final productBlocAddToCard = ProductBloc<ProductModel>();

