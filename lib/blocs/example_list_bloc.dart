import 'package:rxdart/rxdart.dart';

import '../models/example_model.dart';
import '../resources/repository.dart';

class ExampleListBloc {
  final _repository = Repository();

  // ignore: close_sinks
  final _exampleList = PublishSubject<List<ExampleModel>>();

  Observable<List<ExampleModel>> get getExampleList => _exampleList.stream;

  fetchExampleList(list) async {
    /**
     * instead this you must call a real Api Address
     * Using Repository
     * Like This
     *  Response _response  = await _repository.fetchTripsHistory(0);
        _exampleList.sink.add(ExampleModel.parsJson(_body['data']));
     */
    Future.delayed(const Duration(milliseconds: 1000), () {
      List<ExampleModel> dummyData = list;
      _exampleList.sink.add(dummyData);
    });
  }
}

final exampleListBloc = ExampleListBloc();
