import 'dart:convert';

import 'package:http/http.dart';
import 'package:arzaan_sara/global/check/bloc_check.dart';
import 'package:arzaan_sara/global/check/checks.dart';
import 'package:arzaan_sara/models/message_model.dart';
import 'package:arzaan_sara/models/request_model.dart';
import 'package:arzaan_sara/models/trasaction_model.dart';
import 'package:rxdart/rxdart.dart';

import '../models/example_model.dart';
import '../resources/repository.dart';

class MenuBloc<T> {
  Repository _repository = Repository<T>();

  PublishSubject _publishSubject = PublishSubject<T>();

  Observable<T> get actions => _publishSubject.stream;

  fetchTransactions(
      {String apiAddress,
      var dummy,
      Map<String, String> body,
      Function(String) directResult, Function(Response) fullResponse}) async {
    _publishSubject.add(null);

    if (dummy != null) {
      Future.delayed(const Duration(milliseconds: 1000), () {
        _publishSubject.add(TransactionModel.fromJson(dummy));
      });
    } else {
      T model = await _repository.fetchTransactions(apiAddress, body, (res) {
        if (BlocCheck.hasError(res) == null) {
          if (directResult != null) directResult(res);
          return TransactionModel.fromJson(json.decode(res));
        } else {
          _publishSubject.addError(BlocCheck.hasError(res));
          return null;
        }

      },fullResponse);

         if (model != null) {
        _publishSubject.add(model);
      }
    }

    //dispose();
  }

  fetchMessages(
      {String apiAddress,
      var dummy,
      Map<String, String> body,
      Function(String) directResult, Function(Response) fullResponse}) async {
    _publishSubject.add(null);

    if (dummy != null) {
      Future.delayed(const Duration(milliseconds: 1000), () {
        _publishSubject.add(MessageModel.fromJson(dummy));
      });
    } else {
      T model = await _repository.fetchMessages(apiAddress, body, (res) {
        if (BlocCheck.hasError(res) == null) {
          if (directResult != null) directResult(res);
          return MessageModel.fromJson(json.decode(res));
        } else {
          _publishSubject.addError(BlocCheck.hasError(res));
          return null;
        }

      },fullResponse);

         if (model != null) {
        _publishSubject.add(model);
      }
    }
  }

  fetchRequestHistory(
      {String apiAddress,
      var dummy,
      Map<String, String> body,
      Function(String) directResult, Function(Response) fullResponse}) async {
    _publishSubject.add(null);

    if (dummy != null) {
      Future.delayed(const Duration(milliseconds: 1000), () {
        _publishSubject.add(RequestModel.fromJson(dummy));
      });
    } else {
      T model = await _repository.fetchRequestHistory(apiAddress, body, (res) {
        if (BlocCheck.hasError(res) == null) {
          if (directResult != null) directResult(res);
          return RequestModel.fromJson(json.decode(res));
        } else {
          _publishSubject.addError(BlocCheck.hasError(res));
          return null;
        }

      },fullResponse);

         if (model != null) {
        _publishSubject.add(model);
      }
    }
  }

  dispose() {
    _publishSubject.add(null);
  }
}

final menuBlocTransactions = MenuBloc<TransactionModel>();
final menuBlocMessages = MenuBloc<MessageModel>();
final menuBlocRequestHistory = MenuBloc<RequestModel>();
