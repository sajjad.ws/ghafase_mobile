import 'dart:async';
import 'dart:convert';

import 'package:http/http.dart';
import 'package:arzaan_sara/global/check/bloc_check.dart';
import 'package:arzaan_sara/global/check/checks.dart';
import 'package:arzaan_sara/models/const_model/initial_data_model.dart';
import 'package:arzaan_sara/models/user_model.dart';
import 'package:rxdart/rxdart.dart';

import '../resources/save.dart';
import '../resources/repository.dart';

class LogInBloc<T> {
  Repository _repository = Repository<T>();

  PublishSubject _publishSubject = PublishSubject<T>();

  Observable<T> get actions => _publishSubject.stream;

  fetchInitialData(
      {String apiAddress,
      var dummy,
      Function(String) directResult,
      Function(Response) fullResponse}) async {
    if (dummy != null) {
      Future.delayed(const Duration(milliseconds: 1000), () {
        _publishSubject.add(InitialDataModel.fromJson(dummy));
      });
    } else {
      T model = await _repository.fetchInitialData(apiAddress, (res) {
        if (BlocCheck.hasError(res) == null) {
          if (directResult != null) directResult(res);
          return InitialDataModel.fromJson(json.decode(res));
        } else {
          _publishSubject.addError(BlocCheck.hasError(res));
          return null;
        }
      }, fullResponse);

      if (model != null) {
        _publishSubject.add(model);
      }
    }
  }

  fetchUserData(
      {String apiAddress,
      var dummy,
      Map<String, String> body,
      Function(String) directResult,
      Function(Response) fullResponse}) async {
    if (dummy != null) {
      Future.delayed(const Duration(milliseconds: 1000), () {
        //_publishSubject.add(InitialDataModel.fromJson(dummy));
      });
    } else {
      T model = await _repository.fetchUserData(apiAddress, body, (res) {
        if (BlocCheck.hasError(res) == null) {
          if (directResult != null) directResult(res);
          return UserModel.fromJson(json.decode(res));
        } else {
          _publishSubject.addError(BlocCheck.hasError(res));
          return null;
        }
      }, fullResponse);

      if (model != null) {
        _publishSubject.add(model);

        logInBlocInitialData.fetchInitialData(
          apiAddress: "get-initial-data",
        );
      }
    }
  }

  Future<T> fetchReceivePhoneNumber(
      {String apiAddress,
      var dummy,
      Map<String, String> body,
      Function(String) directResult,
      Function(Response) fullResponse}) async {
    _publishSubject.add(null);

    if (dummy != null) {
      Future.delayed(const Duration(milliseconds: 1000), () {
        //_publishSubject.add(RequestModel.fromJson(dummy));
      });
    } else {
      T model =
          await _repository.fetchReceivePhoneNumber(apiAddress, body, (res) {
        if (BlocCheck.hasError(res) == null) {
          if (directResult != null) directResult(res);
          return res.toString();
        } else {
          _publishSubject.addError(BlocCheck.hasError(res));
          return null;
        }
      }, fullResponse);

      if (model != null) {
        _publishSubject.add(model);
        return model;
      }
    }
  }

  Future<T> fetchVerifyPhoneNumber(
      {String apiAddress,
      var dummy,
      Map<String, String> body,
      Function(String) directResult,
      Function(Response) fullResponse}) async {
    _publishSubject.add(null);

    if (dummy != null) {
      Future.delayed(const Duration(milliseconds: 1000), () {
        //_publishSubject.add(RequestModel.fromJson(dummy));
      });
    } else {
      T model =
          await _repository.logInVerifyPhoneNumber(apiAddress, body, (res) {
        if (BlocCheck.hasError(res) == null) {
          if (directResult != null) directResult(res);
          return res.toString();
        } else {
          _publishSubject.addError(BlocCheck.hasError(res));
          return null;
        }
      }, fullResponse);

      if (model != null) {
        _publishSubject.add(model);
        return model;
      }
    }
  }

  fetchEditProfile(
      {String apiAddress,
      var dummy,
      Map<String, String> body,
      Function(String) directResult,
      Function(Response) fullResponse}) async {
    _publishSubject.add(null);

    if (dummy != null) {
      Future.delayed(const Duration(milliseconds: 1000), () {
        //_publishSubject.add(RequestModel.fromJson(dummy));
      });
    } else {
      T model = await _repository.fetchEditProfile(apiAddress, body, (res) {
        print(res);
        if (BlocCheck.hasError(res) == null) {
          if (directResult != null) directResult(res);
          return res.toString();
        } else {
          _publishSubject.addError(BlocCheck.hasError(res));
          return null;
        }
      }, fullResponse);

      if (model != null) {
        _publishSubject.add(model);
      }
    }
  }

  Future<String> fetchUpdateProfilePhoto(
      {String apiAddress,
      var dummy,
      Map<String, dynamic> body,
      Function(String) directResult,
      Function(MultipartRequest) fullResponse}) async {
    _publishSubject.add(null);

    if (dummy != null) {
      Future.delayed(const Duration(milliseconds: 1000), () {
        //_publishSubject.add(RequestModel.fromJson(dummy));
      });
    } else {
      T model =
          await _repository.fetchUpdateProfilePhoto(apiAddress, body, (res) {
        if (BlocCheck.hasError(res) == null) {
          if (directResult != null) directResult(res);
          return res.toString();
        } else {
          _publishSubject.addError(BlocCheck.hasError(res));
          return null;
        }
      }, fullResponse);

      if (model != null) {
        _publishSubject.add(model);
      }
    }
  }

  dispose() {
    _publishSubject.add(null);
  }
}

final logInBlocInitialData = LogInBloc<InitialDataModel>();
final logInBlocUserData = LogInBloc<UserModel>();
final logInBlocEditProfile = LogInBloc<String>();
final logInReceivePhoneNumber = LogInBloc<dynamic>();
final logInVerifyPhoneNumber = LogInBloc<dynamic>();
final logInBlocUpdateProfilePhoto = LogInBloc<String>();
