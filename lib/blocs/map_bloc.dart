import 'dart:async';

import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:arzaan_sara/models/search_model.dart';
import 'package:rxdart/rxdart.dart';

import '../resources/save.dart';
import '../resources/repository.dart';

class MapBloc {
  final _repository = Repository();
  //////////
  final _searchFetcher = PublishSubject<List<SearchModel>>();

  Observable<List<SearchModel>> get getSearchList => _searchFetcher.stream;

  fetchSearchList(String name, LatLng loc) async {
    _searchFetcher.add(null);

    List<SearchModel> itemModel = await _repository.fetchSearchList(name, loc);
    _searchFetcher.add(itemModel);
  }

  dispose() {
    _searchFetcher.add(null);
  }

  void initialize() {
    _searchFetcher.sink.add([]);
  }

  

}

final mapBloc = MapBloc();
