import 'dart:convert';

import 'package:http/http.dart';
import 'package:arzaan_sara/global/check/bloc_check.dart';
import 'package:arzaan_sara/global/check/checks.dart';
import 'package:arzaan_sara/models/request_model.dart';
import 'package:rxdart/rxdart.dart';

import '../resources/repository.dart';

class RequestBloc<T> {
  Repository _repository = Repository<T>();

  BehaviorSubject _publishSubject = BehaviorSubject<T>();

  ValueObservable<T> get actions => _publishSubject.stream;

  fetchActiveRequest(
      {String apiAddress,
      var dummy,
      Map<String, String> body,
      Function(String) directResult,
      Function(Response) fullResponse}) async {
    _publishSubject.add(null);

    if (dummy != null) {
      Future.delayed(const Duration(milliseconds: 1000), () {
        _publishSubject.add(RequestModel.fromJson(dummy));
      });
    } else {
      T model = await _repository.fetchActiveRequest(apiAddress, body, (res) {
        if (BlocCheck.hasError(res) == null) {
          if (directResult != null) directResult(res);
          return RequestModel.fromJson(json.decode(res));
        } else {
          _publishSubject.addError(BlocCheck.hasError(res));
          return null;
        }
      }, fullResponse);

      if (model != null) {
        _publishSubject.add(model);
      }
    }
  }

  fetchPendingRequest(
      {String apiAddress,
      var dummy,
      Map<String, String> body,
      Function(String) directResult,
      Function(Response) fullResponse}) async {
    _publishSubject.add(null);

    if (dummy != null) {
      Future.delayed(const Duration(milliseconds: 1000), () {
        _publishSubject.add(RequestModel.fromJson(dummy));
      });
    } else {
      T model = await _repository.fetchPendingRequest(apiAddress, body, (res) {
        if (BlocCheck.hasError(res) == null) {
          if (directResult != null) directResult(res);
          return RequestModel.fromJson(json.decode(res));
        } else {
          _publishSubject.addError(BlocCheck.hasError(res));
          return null;
        }
      }, fullResponse);

      if (model != null) {
        _publishSubject.add(model);
      }
    }
  }

  fetchAddRequest(
      {String apiAddress,
      var dummy,
      Map<String, String> body,
      Function(String) directResult,
      Function(Response) fullResponse}) async {
    _publishSubject.add(null);

    if (dummy != null) {
      Future.delayed(const Duration(milliseconds: 1000), () {
        //_publishSubject.add(RequestModel.fromJson(dummy));
      });
    } else {
      T model = await _repository.fetchAddRequest(apiAddress, body, (res) {
        if (BlocCheck.hasError(res) == null) {
          if (directResult != null) directResult(res);
          return res.toString();
        } else {
          _publishSubject.addError(BlocCheck.hasError(res));
          return null;
        }
      }, fullResponse);

      if (model != null) {
        _publishSubject.add(model);

        requestBlocPendingRequests.fetchPendingRequest(
            apiAddress: "get-driver-hover-requests", body: {});
      }
    }
  }

  fetchAcceptRequestByDriver(
      {String apiAddress,
      var dummy,
      Map<String, String> body,
      Function(String) directResult,
      Function(Response) fullResponse}) async {
    _publishSubject.add(null);

    if (dummy != null) {
      Future.delayed(const Duration(milliseconds: 1000), () {
        //_publishSubject.add(RequestModel.fromJson(dummy));
      });
    } else {
      T model =
          await _repository.fetchAcceptRequestByDriver(apiAddress, body, (res) {
        if (BlocCheck.hasError(res) == null) {
          if (directResult != null) directResult(res);
          return res.toString();
        } else {
          _publishSubject.addError(BlocCheck.hasError(res));
          return null;
        }
      }, fullResponse);

      if (model != null) {
        _publishSubject.add(model);

        requestBlocPendingRequests.fetchPendingRequest(
            apiAddress: "get-driver-hover-requests", body: {});

        requestBlocActiveRequests.fetchActiveRequest(
            apiAddress: "get-driver-active-requests", body: {});
      }
    }
  }

  fetchCancelOwner(
      {String apiAddress,
      var dummy,
      Map<String, String> body,
      Function(String) directResult,
      Function(Response) fullResponse}) async {
    _publishSubject.add(null);

    if (dummy != null) {
      Future.delayed(const Duration(milliseconds: 1000), () {
        //_publishSubject.add(RequestModel.fromJson(dummy));
      });
    } else {
      T model = await _repository.fetchCancelOwner(apiAddress, body, (res) {
        if (BlocCheck.hasError(res) == null) {
          if (directResult != null) directResult(res);
          return res.toString();
        } else {
          _publishSubject.addError(BlocCheck.hasError(res));
          return null;
        }
      }, fullResponse);

      if (model != null) {
        _publishSubject.add(model);

        requestBlocPendingRequests.fetchPendingRequest(
            apiAddress: "get-driver-hover-requests", body: {});

        requestBlocActiveRequests.fetchActiveRequest(
            apiAddress: "get-driver-active-requests", body: {});
      }
    }
  }

  fetchStart(
      {String apiAddress,
      var dummy,
      Map<String, String> body,
      Function(String) directResult,
      Function(Response) fullResponse}) async {
    _publishSubject.add(null);

    if (dummy != null) {
      Future.delayed(const Duration(milliseconds: 1000), () {
        //_publishSubject.add(RequestModel.fromJson(dummy));
      });
    } else {
      T model = await _repository.fetchStart(apiAddress, body, (res) {
        if (BlocCheck.hasError(res) == null) {
          if (directResult != null) directResult(res);
          return res.toString();
        } else {
          _publishSubject.addError(BlocCheck.hasError(res));
          return null;
        }
      }, fullResponse);

      if (model != null) {
        _publishSubject.add(model);

        requestBlocActiveRequests.fetchActiveRequest(
            apiAddress: "get-driver-active-requests", body: {});
      }
    }
  }

  Future<String> fetchAddComment(
      {String apiAddress,
      var dummy,
      Map<String, String> body,
      Function(String) directResult,
      Function(Response) fullResponse}) async {
    _publishSubject.add(null);

    if (dummy != null) {
      Future.delayed(const Duration(milliseconds: 1000), () {
        //_publishSubject.add(RequestModel.fromJson(dummy));
      });
    } else {
      T model = await _repository.fetchAddComment(apiAddress, body, (res) {
        if (BlocCheck.hasError(res) == null) {
          if (directResult != null) directResult(res);
          return res.toString();
        } else {
          _publishSubject.addError(BlocCheck.hasError(res));
          return null;
        }
      }, fullResponse);

      if (model != null) {
        _publishSubject.add(model);
      }
    }
  }

  Future<String> fetchEnd(
      {String apiAddress,
      var dummy,
      Map<String, String> body,
      Function(String) directResult,
      Function(Response) fullResponse}) async {
    _publishSubject.add(null);

    if (dummy != null) {
      Future.delayed(const Duration(milliseconds: 1000), () {
        //_publishSubject.add(RequestModel.fromJson(dummy));
      });
    } else {
      T model = await _repository.fetchEnd(apiAddress, body, (res) {
        if (BlocCheck.hasError(res) == null) {
          if (directResult != null) directResult(res);
          return res.toString();
        } else {
          _publishSubject.addError(BlocCheck.hasError(res));
          return null;
        }
      }, fullResponse);

      if (model != null) {
        _publishSubject.add(model);

        requestBlocActiveRequests.fetchActiveRequest(
            apiAddress: "get-driver-active-requests", body: {});
      }
    }
  }

  dispose() {
    _publishSubject.add(null);
  }
}

final requestBlocActiveRequests = RequestBloc<RequestModel>();
final requestBlocPendingRequests = RequestBloc<RequestModel>();
final requestBlocAddRequest = RequestBloc<String>();
final requestBlocAcceptRequestByDriver = RequestBloc<String>();
final requestBlocCancelOwner = RequestBloc<String>();
final requestBlocStart = RequestBloc<String>();
final requestBlocEnd = RequestBloc<String>();
final requestBlocAddComment = RequestBloc<String>();
