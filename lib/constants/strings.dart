
import 'dart:ui';

class Strings{
  static const AppName = 'اسپرلوس';
  static const lorm =
      "لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد.";

  static const cedarMapToken = "ed07895610d88f5210e17503f067f488d711cc14";
  static const int resendSmsTime = 60;

  static const String supportPhone = "09212098662";

  static const String aboutUsUrl = "http://asas";

  //static const siteUrl = "http://obar.app-esperlos.ir/";
  //static const siteUrl = "http://194.5.175.196/";
  static const siteUrl = "http://adminghaafse.app-esperlos.ir/public";
  //static const baseFileUrl = "http://obar.app-esperlos.ir";
  static const baseFileUrl = "http://194.5.175.196";
  //static const apiUrl = "http://obar.app-esperlos.ir/api/v1/driver/";
  static const apiUrl = "http://adminghaafse.app-esperlos.ir/api/v1/buyer/";
  //static const apiUrl = "http://10.0.2.2:8000/api/v1/buyer/";
  static const Color cardItemColor = Color(0xfffbfbfb);

  static const greenLight = Color(0xff7DB93F);
  static const greenDark = Color(0xff52881A);
  static const bgColor = Color(0xffF6F6F6);
  static const green = Color(0xff435138);
}
