import 'package:arzaan_sara/constants/enums.dart';
import 'package:arzaan_sara/models/request_model.dart';

class StatusConverter{

  static String getTransactionStr(String num){
    int _num = int.parse(num);

    switch(_num){

      case 0:
        return "پرداخت پورسانت";
      case 1:
        return "پرداخت از کیف پول";
      case 2:
        return "افزایش اعتبار کیف پول";
    }
  }

  static String getTransactionTypeStr(String num){
    int _num = int.parse(num);

    switch(_num){

      case 0:
        return "پرداخت آنلاین";
      case 1:
        return "کیف پول";
      case 2:
        return "کیف پول";
    }
  }

  static TransactionType getTransactionType(String num){
    int _num = int.parse(num);

    switch(_num){

      case 0:
        return TransactionType.onlinePay;
      case 1:
        return TransactionType.walletPay;
      case 2:
        return TransactionType.chargingWallet;
    }
  }

  static ReceiverType getReceiverType(String num){
    if(num != "null") {
      int _num = int.parse(num);

      switch (_num) {
        case 0:
          return ReceiverType.driver;
        case 1:
          return ReceiverType.carrier;
      }
    }
    return null;
  }


  static FCMType getFcmType(String status){
    if(status != "null") {
      int _num = int.parse(status);

      switch (_num) {
        case 0:
          return FCMType.message;
        case 1:
          return FCMType.request;
      }
    }
    return null;
  }

}