enum ProfileFormType { register, update ,cantUpdateTime}
enum DriverVerifyType { notRegister, registered ,confirmed}
enum RequestType { add, update, canUpdateTime }
enum MapAddressingStep { firstMarker, secondMarker, finish }
enum PaymentType { credit, online }
enum ReceiverType { driver, carrier }
enum RequestSteps {
  add,
  acceptByDriver,
  acceptByCarrier,
  introductionDriverByCarrier,
  acceptCarrierByOwner,
  acceptDriverByOwner,
  cancelByOwner,
  cancelByDriver,
  cancelByCarrier,
  start,
  end,
  confirmByOwner
}
enum TransactionType {onlinePay,chargingWallet,walletPay}
enum FCMType {message,request}