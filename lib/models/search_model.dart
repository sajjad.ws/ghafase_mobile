class SearchModel {
  var id;
  var name;
  var address;
  var city;
  var province;
  double lat;
  double lon;

  SearchModel({
    this.id,
    this.name,
    this.address,
    this.city,
    this.province,
    this.lat,
    this.lon,
  });

  static List<SearchModel> parsJson(Map<String, dynamic> parsedJson) {
    List<SearchModel> _data = [];
    for (int i = 0; i < parsedJson['results'].length; i++) {
      List<String> loc = parsedJson['results'][i]['location']['center'].toString().split(",");
      _data.add(

        new SearchModel(
          id: parsedJson['results'][i]['id'],
          name: parsedJson['results'][i]['name'],
          address: parsedJson['results'][i]['address'],
          city: parsedJson['results'][i]['components']['city'],
          province: parsedJson['results'][i]['components']['province'],
          lat: double.parse(loc[0]),
          lon: double.parse(loc[1]),
        ),
      );
    }
    return _data;
  }
}
