import 'package:arzaan_sara/models/const_model/vehicle_model.dart';
import 'package:arzaan_sara/resources/save.dart';

class VehicleGroupModel{

  List<VehicleGroupModelDetail> vehicleGroups = [];

  List<_Data> worktimes = [
    _Data("1" ,'پاره وقت'),
    _Data("2" ,'تمام وقت'),
    _Data("3" ,'پروژه ای _ پیمان کاری')

  ];

  String returnData(String status){
    int i = int.parse(status);
    switch(i){
      case 1 : return 'پاره وقت';
      case 2 : return 'تمام وقت';
      case 3 : return 'پروژه ای _ پیمان کاری';
    }

    return "";
  }


  static Future<String> returnCommission(String vehicleGroupId)async{
    int i = int.parse(vehicleGroupId);
    print(vehicleGroupId);
    VehicleGroupModel model = await Save().getVehiclesGroupData();
    String commissionAmount = "";
    for(int i = 0; i<model.vehicleGroups.length ; i++){
      if(vehicleGroupId == model.vehicleGroups[i].id){
        commissionAmount = model.vehicleGroups[i].commission_amount;
      }
    }




    return commissionAmount;
  }

  static Future<String> returnCommissionByVehicleId(String vehicle_id)async{


    VehicleModel model = await Save().getVehiclesData();
    String vehicleGroup = "";
    for(int i = 0; i<model.vehicles.length ; i++){
      if(vehicle_id == model.vehicles[i].id){
        vehicleGroup = model.vehicles[i].vehicle_group_id;
      }
    }

    String commission = await VehicleGroupModel.returnCommission(vehicleGroup);

    return commission;
  }


  VehicleGroupModel.fromJson(data){

    List list = data;
    for( int i = 0 ; i< list.length; i++){

      VehicleGroupModelDetail _result = VehicleGroupModelDetail(list[i]);
      vehicleGroups.add(_result);
    }

  }

}
class VehicleGroupModelDetail{

  var id, name, max_capacity, commission_amount;

  VehicleGroupModelDetail(res){

    id = res['id'].toString();
    name = res['name'].toString();
    max_capacity = res['max_capacity'].toString();
    commission_amount = res['commission_amount'].toString();
  }



}


class _Data{
  String _id, _title;

  _Data(this._id, this._title);

  get title => _title;

  String get id => _id;


}



