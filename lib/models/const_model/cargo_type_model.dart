import 'package:arzaan_sara/resources/save.dart';

class CargoTypeModel{

  List<CargoTypeDetail> cargoeTypes = [];



  static Future<CargoTypeDetail> returnData(String id)async{
    if(id != "null"){


    CargoTypeModel model = await Save().getCargoTypeData();
    CargoTypeDetail selected;
    for( int i = 0 ; i< model.cargoeTypes.length; i++){

      if(model.cargoeTypes[i].id == id){
        selected = model.cargoeTypes[i];
        break;
      }

    }


    return selected;

    }
    return null;
  }


  CargoTypeModel.fromJson(data){

    List list = data;
    for( int i = 0 ; i< list.length; i++){

      CargoTypeDetail _result = CargoTypeDetail(list[i]);
      cargoeTypes.add(_result);
    }

  }
}

class CargoTypeDetail{

  var id, name, commission_amount;

  CargoTypeDetail(res){

    id = res['id'].toString();
    name = res['name'].toString();
    commission_amount = res['commission_amount'].toString();
  }



}



class _Data{
  String _id, _title;

  _Data(this._id, this._title);

  get title => _title;

  String get id => _id;


}



