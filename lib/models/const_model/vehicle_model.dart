
import 'dart:convert';

import 'package:arzaan_sara/resources/save.dart';

class VehicleModel{

  List<VehicleModelDetail> vehicles = List<VehicleModelDetail>();


  static Future<VehicleModelDetail> returnData(String id)async{

    if(id != "null") {
      VehicleModel model = await Save().getVehiclesData();
      VehicleModelDetail selected;
      for (int i = 0; i < model.vehicles.length; i++) {
        if (model.vehicles[i].id == id) {
          selected = model.vehicles[i];
          break;
        }
      }


      return selected;
    }
    return null;
  }

  static Future<String> returnVehicleName(String id)async{

    if(id != "null") {
      VehicleModel model = await Save().getVehiclesData();
      VehicleModelDetail selected;
      for (int i = 0; i < model.vehicles.length; i++) {
        if (model.vehicles[i].id == id) {
          selected = model.vehicles[i];
          break;
        }
      }

      return selected.name;
    }
    return null;
  }


  VehicleModel.fromJson(data){

    List list = data;
    for( int i = 0 ; i< list.length; i++){

      VehicleModelDetail _result = VehicleModelDetail(list[i]);
      vehicles.add(_result);
    }

  }


}


class VehicleModelDetail{

  var id, name, max_capacity, vehicle_group_id;


  VehicleModelDetail(res){

    id = res['id'].toString();
    name = res['name'].toString();
    max_capacity = res['max_capacity'].toString();
    vehicle_group_id = res['vehicle_group_id'].toString();
  }



}
