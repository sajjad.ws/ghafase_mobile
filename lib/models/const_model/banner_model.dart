import 'dart:convert';


import 'package:arzaan_sara/resources/save.dart';

class BannerModel{

  List<_Result> banners = [];


  BannerModel.fromJson(dynamic data){

    List list = data;
    for( int i = 0 ; i< list.length; i++){

      _Result _result = _Result(list[i]);
      banners.add(_result);
    }

}


}



class _Result{

  var  id, url, type, created_at, updated_at;

  _Result(res){

    id = res['id'].toString();
    url = res['url'].toString();
    type = res['type'].toString();
    created_at = res['created_at'].toString();
    updated_at = res['updated_at'].toString();
  }



}