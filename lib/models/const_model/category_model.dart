import 'dart:convert';


import 'package:arzaan_sara/resources/save.dart';

class CategoryModel{

  List<_Result> categories = [];


  CategoryModel.fromJson(dynamic data){

    List list = data;
    for( int i = 0 ; i< list.length; i++){

      _Result _result = _Result(list[i]);
      categories.add(_result);
    }

}


}



class _Result{

  var    id, name, created_at, updated_at;
  _Result(res){

    id = res['id'].toString();
    name = res['name'].toString();
    created_at = res['created_at'].toString();
    updated_at = res['updated_at'].toString();
  }



}