
class SettingModel {
  var id,
      driver_regulation,
      driver_guid,
      carrier_regulation,
      carrier_guid,
      cargo_owner_guid,
      marketer_guid,
      cargo_owner_regulation,
      driver_confirmation_text,
      marketer_regulation,
      cargo_registration_regulation,
      driver_confirmation_regulation,
      cost_estimation_status,
      cost_estimation_value,
      carrier_driver_introduction,
      cargo_cancelable_time,
      driver_confirmation_time,
      system_status,
      is_active_registrar_commission,
      is_active_receiver_commission,
      created_at,
      updated_at;

  SettingModel.fromJson(data) {
    Map<String, dynamic> map = data;

    id = map['id'].toString();
    driver_guid = map['driver_guid'].toString();
    carrier_guid = map['carrier_guid'].toString();
    cargo_owner_guid = map['cargo_owner_guid'].toString();
    marketer_guid = map['marketer_guid'].toString();
    cargo_owner_regulation = map['cargo_owner_regulation'].toString();
    marketer_regulation = map['marketer_regulation'].toString();
    carrier_regulation = map['carrier_regulation'].toString();
    cargo_registration_regulation =
        map['cargo_registration_regulation'].toString();
    driver_confirmation_regulation =
        map['driver_confirmation_regulation'].toString();
    cost_estimation_status = map['cost_estimation_status'].toString();
    carrier_driver_introduction = map['carrier_driver_introduction'].toString();
    cargo_cancelable_time = map['cargo_cancelable_time'].toString();
    driver_confirmation_time = map['driver_confirmation_time'].toString();
    system_status = map['system_status'].toString();
    is_active_registrar_commission =
        map['is_active_registrar_commission'].toString();
    is_active_receiver_commission =
        map['is_active_receiver_commission'].toString();
    created_at = map['created_at'].toString();
    driver_confirmation_text = map['driver_confirmation_text'].toString();
  }
}
