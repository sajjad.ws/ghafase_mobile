import 'dart:convert';


import 'package:arzaan_sara/resources/save.dart';

class CityModel{

  List<_Result> cities = [];


  CityModel.fromJson(dynamic data){

    List list = data;
    for( int i = 0 ; i< list.length; i++){

      _Result _result = _Result(list[i]);
      cities.add(_result);
    }

}

  static Future<String> returnData(String id)async{

    if(id != "null"){
      CityModel model = await Save().getCityData();

      for( int i = 0 ; i<model.cities.length ; i++){
        if(model.cities[i].id == id)
          return model.cities[i].name;
      }

    }

    return "";
  }

}



class _Result{

  var id, name, pid;

  _Result(res){

    id = res['id'].toString();
    name = res['name'].toString();
    pid = res['pid'].toString();
  }



}