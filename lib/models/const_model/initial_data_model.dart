import 'dart:convert';

import 'package:arzaan_sara/models/const_model/cargo_type_model.dart';
import 'package:arzaan_sara/models/const_model/vehicle_group_model.dart';
import 'package:arzaan_sara/models/const_model/vehicle_model.dart';
import 'package:arzaan_sara/resources/save.dart';

class InitialDataModel{

  InitialDataModel.fromJson(res){

    Save().setCityData(jsonEncode(res['city']).toString());
    Save().setBannerData(jsonEncode(res['banner']).toString());
    Save().setCategoryData(jsonEncode(res['category']).toString());


  }


}