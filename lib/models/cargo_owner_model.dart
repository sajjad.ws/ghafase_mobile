
class CargoOwnerModel {


  var id,
      first_name,
      last_name,
      company_name,
      national_code,
      address,
      latitude,
      longitude,
      postal_code,
      phone_number,
      email,
      status,
      validation_code,
      credit,
      api_token,
      push_notification_token,
      marketer_id,
      city_id,
      cargo_request_count,
      created_at,
      updated_at;


  CargoOwnerModel.fromJson(parsedJson){
    Map<String, dynamic> data = parsedJson;


    id = data['id'].toString();
    first_name = data['first_name'].toString();
    last_name = data['last_name'].toString();
    company_name = data['company_name'].toString();
    national_code = data['national_code'].toString();
    address = data['address'].toString();
    latitude = data['latitude'].toString();
    longitude = data['longitude'].toString();
    postal_code = data['postal_code'].toString();
    phone_number = data['phone_number'].toString();
    email = data['email'].toString();
    status = data['status'].toString();
    validation_code = data['validation_code'].toString();
    credit = data['credit'].toString();
    api_token = data['api_token'].toString();
    push_notification_token = data['push_notification_token'].toString();
    marketer_id = data['marketer_id'].toString();
    city_id = data['city_id'].toString();
    cargo_request_count = data['cargo_request_count'].toString();
    created_at = data['created_at'].toString();
    updated_at = data['updated_at'].toString();


  }


}

