class TransactionModel {

  List<_Result> transactions = [];

  TransactionModel.fromJson(res){

    List data = res;

    for( int i = 0 ; i<data.length ; i++){

      _Result _result = _Result(data[i]);

      transactions.add(_result);
    }


  }
}


class _Result {
  var id ,user_type ,user_id ,type ,amount ,discount_percent ,summation ,value ,ref_id, created_at, updated_at ;

  _Result(res){

    id = res['id'].toString();
    user_type = res['user_type'].toString();
    user_id = res['user_id'].toString();
    type = res['type'].toString();
    amount = res['amount'].toString();
    discount_percent = res['discount_percent'].toString();
    summation = res['summation'].toString();
    value = res['value'].toString();
    ref_id = res['ref_id'].toString();
    created_at = res['created_at'].toString();
    updated_at = res['updated_at'].toString();

  }


}