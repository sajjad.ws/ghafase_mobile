class MessageModel {

  List<_Result> messages = [];

  MessageModel.fromJson(res){

    List data = res;

    for( int i = 0 ; i<data.length ; i++){

      _Result _result = _Result(data[i]);

      messages.add(_result);
    }


  }
}


class _Result {
  var id, title, content, sent_at, status, for_carriers, for_drivers, for_cargo_owners, for_marketers, operator_id, created_at, updated_at ;

  _Result(res){

    id = res['id'].toString();
    title = res['title'].toString();
    content = res['content'].toString();
    sent_at = res['sent_at'].toString();
    status = res['status'].toString();
    for_carriers = res['for_carriers'].toString();
    for_drivers = res['for_drivers'].toString();
    for_cargo_owners = res['for_cargo_owners'].toString();
    for_marketers = res['for_marketers'].toString();
    operator_id = res['operator_id'].toString();
    created_at = res['created_at'].toString();
    updated_at = res['updated_at'].toString();

  }


}