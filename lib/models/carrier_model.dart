
class CarrierModel {
  var id,
      freight_name,
      username,
      password,
      owner_first_name,
      owner_last_name,
      owner_phone_number,
      telephone_number,
      status,
      credit,
      point,
      api_token,
      push_notification_token,
      marketer_id,
      city_id,
      all_cargo_request_count,
      done_cargo_request_count,
      created_at,
      updated_at;


  CarrierModel.fromJson(parsedJson) {
    Map<String, dynamic> data = parsedJson;

    id = data['id'].toString();
    freight_name = data['freight_name'].toString();
    username = data['username'].toString();
    password = data['password'].toString();
    owner_first_name = data['owner_first_name'].toString();
    owner_last_name = data['owner_last_name'].toString();
    owner_phone_number = data['owner_phone_number'].toString();
    telephone_number = data['telephone_number'].toString();
    status = data['status'].toString();
    credit = data['credit'].toString();
    point = data['point'].toString();
    api_token = data['api_token'].toString();
    push_notification_token = data['push_notification_token'].toString();
    marketer_id = data['marketer_id'].toString();
    city_id = data['city_id'].toString();
    all_cargo_request_count = data['all_cargo_request_count'].toString();
    done_cargo_request_count = data['done_cargo_request_count'].toString();
    created_at = data['created_at'].toString();
    updated_at = data['updated_at'].toString();
  }
}
