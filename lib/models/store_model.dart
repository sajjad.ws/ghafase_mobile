import 'package:arzaan_sara/constants/enums.dart';

class StoreModel {
  List<Products> stores = [];

  StoreModel.fromJson(res) {
    List data = res;

    for (int i = 0; i < data.length; i++) {
      Products _result = Products(data[i]);

      stores.add(_result);
    }
  }
}

class Products {
  var id,
      name,
      address,
      lat_long,
      sms_validation,
      status,
      payment_status,
      owner_name,
      owner_phone,
      image_url,
      product_count,
      marked,
      created_at,
      updated_at;

  Products(res) {
    id = res['id'].toString();
    name = res['name'].toString();
    address = res['address'].toString();
    lat_long = res['lat_long'].toString();
    sms_validation = res['sms_validation'].toString();
    status = res['status'].toString();
    payment_status = res['payment_status'].toString();
    owner_name = res['owner_name'].toString();
    owner_phone = res['owner_phone'].toString();
    image_url = res['image_url'].toString();
    product_count = res['product_count'].toString();
    marked = res['marked'].toString();
    created_at = res['created_at'].toString();
    updated_at = res['updated_at'].toString();
  }
}
