import 'package:arzaan_sara/constants/enums.dart';

class ProductModel {
  List<Products> products = [];

  ProductModel.fromJson(res) {
    List data = res;

    for (int i = 0; i < data.length; i++) {
      Products _result = Products(data[i]);

      products.add(_result);
    }
  }
}

class Products {
  var id,
      name,
      image_uri,
      price,
      p_date,
      e_date,
      discount,
      number,
      category_id,
      store_id,
      discount_price,
      time_to_expire,
      created_at,
      updated_at;

  CategoryDetail categoryDetail;
  StoreDetail storeDetail;

  Products(res) {
    id = res['id'].toString();
    name = res['name'].toString();
    image_uri = res['image_uri'].toString();
    price = res['price'].toString();
    p_date = res['p_date'].toString();
    e_date = res['e_date'].toString();
    discount = res['discount'].toString();
    number = res['number'].toString();
    category_id = res['category_id'].toString();
    store_id = res['store_id'].toString();
    discount_price = res['discount_price'].toString();
    time_to_expire = res['time_to_expire'].toString();
    created_at = res['created_at'].toString();
    updated_at = res['updated_at'].toString();

    Map<String, dynamic> category = res['category'];
    Map<String, dynamic> store = res['store'];

    categoryDetail = CategoryDetail(
      id: category['id'].toString(),
      name: category['name'].toString(),
      created_at: category['created_at'].toString(),
      updated_at: category['updated_at'].toString(),
    );

    storeDetail = StoreDetail(
      id: store['id'].toString(),
      name: store['name'].toString(),
      address: store['address'].toString(),
      lat_long: store['lat_long'].toString(),
      sms_validation: store['sms_validation'].toString(),
      status: store['status'].toString(),
      payment_status: store['payment_status'].toString(),
      owner_name: store['owner_name'].toString(),
      owner_phone: store['owner_phone'].toString(),
      image_url: store['image_url'].toString(),
      product_count: store['product_count'].toString(),
      marked: store['marked'].toString(),
      created_at: store['created_at'].toString(),
      updated_at: store['updated_at'].toString(),
    );
  }
}

class CategoryDetail {
  var id, name, created_at, updated_at;

  CategoryDetail({
    this.id,
    this.name,
    this.created_at,
    this.updated_at,
  });
}

class StoreDetail {
  var id,
      name,
      address,
      lat_long,
      sms_validation,
      status,
      payment_status,
      owner_name,
      owner_phone,
      image_url,
      product_count,
      marked,
      created_at,
      updated_at;

  StoreDetail(
      {this.id,
      this.name,
      this.address,
      this.lat_long,
      this.sms_validation,
      this.status,
      this.payment_status,
      this.owner_name,
      this.owner_phone,
      this.image_url,
      this.product_count,
      this.marked,
      this.created_at,
      this.updated_at});
}
