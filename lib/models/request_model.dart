import 'package:arzaan_sara/constants/enums.dart';

class RequestModel {
  List<Reqeusts> requests = [];

  List<_Data> requestStates = [
    _Data("0", 'متاهل', RequestSteps.add),
    _Data("1", 'فرقی نمیکند', RequestSteps.acceptByDriver),
    _Data("2", 'مجرد', RequestSteps.acceptByCarrier),
    _Data("3", 'مجرد', RequestSteps.introductionDriverByCarrier),
    _Data("4", 'مجرد', RequestSteps.acceptDriverByOwner),
    _Data("5", 'مجرد', RequestSteps.acceptCarrierByOwner),
    _Data("6", 'مجرد', RequestSteps.cancelByOwner),
    _Data("7", 'مجرد', RequestSteps.cancelByDriver),
    _Data("8", 'مجرد', RequestSteps.cancelByCarrier),
    _Data("9", 'مجرد', RequestSteps.start),
    _Data("10", 'مجرد', RequestSteps.end),
    _Data("11", 'مجرد', RequestSteps.confirmByOwner),
  ];

  String returnTitle(String status) {
    int i = int.parse(status);
    switch (i) {
      case 0:
        return 'در حال بررسی رانندگان و باربری ها';
      case 1:
        return 'در انتظار تایید صاحب بار';
      case 2:
        return 'تایید شده توسط باربری';
      case 3:
        return 'در حال واگذاری بار به راننده';
      case 4:
        return 'تایید راننده توسط صاحب بار';
      case 5:
        return 'تایید باربری توسط صاحب بار';
      case 6:
        return 'لغو شده توسط صاحب بار';
      case 7:
        return 'لفو شده توسط راننده';
      case 8:
        return 'لغو شده توسط باربری';
      case 9:
        return 'در حال بارگیری';
      case 10:
        return 'صاحب بار هنوز پایان سفر را تایید نکرد است';
      case 11:
        return 'با موفقیت انجام شده';
    }
  }

  RequestSteps returnStep(String status) {
    int i = int.parse(status);
    switch (i) {
      case 0:
        return RequestSteps.add;
      case 1:
        return RequestSteps.acceptByDriver;
      case 2:
        return RequestSteps.acceptByCarrier;
      case 3:
        return RequestSteps.introductionDriverByCarrier;
      case 4:
        return RequestSteps.acceptDriverByOwner;
      case 5:
        return RequestSteps.acceptCarrierByOwner;
      case 6:
        return RequestSteps.cancelByOwner;
      case 7:
        return RequestSteps.cancelByDriver;
      case 8:
        return RequestSteps.cancelByCarrier;
      case 9:
        return RequestSteps.start;
      case 10:
        return RequestSteps.end;
      case 11:
        return RequestSteps.confirmByOwner;
    }

    return RequestSteps.add;
  }

  RequestModel();

  RequestModel.fromJson(res) {
    List data = res;

    for (int i = 0; i < data.length; i++) {
      Reqeusts _result = Reqeusts(data[i]);

      requests.add(_result);
    }
  }
}

class Reqeusts {
  var id,
      origin_address,
      destination_address,
      origin_latitude,
      origin_longitude,
      destination_latitude,
      destination_longitude,
      cargo_weight,
      cargo_cost,
      owner_type,
      owner_id,
      price,
      cargo_weight_min,
      cargo_weight_max,
      cargo_cost_max,
      cargo_cost_min,
      description,
      message_to_driver,
      loading_start_time,
      loading_end_time,
      evacuation_start_time,
      evacuation_end_time,
      last_status,
      receiver_type,
      receiver_id,
      cargo_type_id,
      vehicle_id,
      origin_city_id,
      destination_city_id,
      created_at,
      updated_at;

  CargoOwnerDetail cargoOwnerDetail;
  CarrierDetail carrierDetail;

  Reqeusts(res) {
    id = res['id'].toString();
    origin_address = res['origin_address'].toString();
    destination_address = res['destination_address'].toString();
    origin_latitude = res['origin_latitude'].toString();
    origin_longitude = res['origin_longitude'].toString();
    destination_latitude = res['destination_latitude'].toString();
    destination_longitude = res['destination_longitude'].toString();
    cargo_weight = res['cargo_weight'].toString();
    cargo_cost = res['cargo_cost'].toString();
    owner_type = res['owner_type'].toString();
    price = res['price'].toString();
    cargo_weight_min = res['cargo_weight_min'].toString();
    cargo_weight_max = res['cargo_weight_max'].toString();
    cargo_cost_max = res['cargo_cost_max'].toString();
    cargo_cost_min = res['cargo_cost_min'].toString();
    owner_id = res['owner_id'].toString();
    description = res['description'].toString();
    message_to_driver = res['message_to_driver'].toString();
    loading_start_time = res['loading_start_time'].toString();
    loading_end_time = res['loading_end_time'].toString();
    evacuation_start_time = res['evacuation_start_time'].toString();
    evacuation_end_time = res['evacuation_end_time'].toString();
    last_status = res['last_status'].toString();
    receiver_type = res['receiver_type'].toString();
    receiver_id = res['receiver_id'].toString();
    cargo_type_id = res['cargo_type_id'].toString();
    vehicle_id = res['vehicle_id'].toString();
    origin_city_id = res['origin_city_id'].toString();
    destination_city_id = res['destination_city_id'].toString();
    created_at = res['created_at'].toString();
    updated_at = res['updated_at'].toString();

    Map<String, dynamic> ownerMap = res['owner'];

    if (ownerMap != null) {
      if (owner_type == "0")
        cargoOwnerDetail = CargoOwnerDetail(
          id: ownerMap['id'].toString(),
          first_name: ownerMap['first_name'].toString(),
          last_name: ownerMap['last_name'].toString(),
          phone_number: ownerMap['phone_number'].toString(),
          national_code: ownerMap['national_code'].toString(),
          updated_at: ownerMap['updated_at'].toString(),
          status: ownerMap['status'].toString(),
          push_notification_token: ownerMap['push_notification_token'].toString(),
          marketer_id: ownerMap['marketer_id'].toString(),
          credit: ownerMap['credit'].toString(),
          created_at: ownerMap['created_at'].toString(),
          city_id: ownerMap['city_id'].toString(),
          api_token: ownerMap['api_token'].toString(),
          address: ownerMap['address'].toString(),
          cargo_request_count: ownerMap['cargo_request_count'].toString(),
          company_name: ownerMap['company_name'].toString(),
          email: ownerMap['email'].toString(),
          latitude: ownerMap['latitude'].toString(),
          longitude: ownerMap['longitude'].toString(),
          postal_code: ownerMap['postal_code'].toString(),
          validation_code: ownerMap['validation_code'].toString(),
        );
      if (owner_type == "1") {

        carrierDetail = CarrierDetail(
          id: ownerMap['id'].toString(),
          api_token: ownerMap['api_token'].toString(),
          city_id: ownerMap['city_id'].toString(),
          created_at: ownerMap['created_at'].toString(),
          credit: ownerMap['credit'].toString(),
          freight_name: ownerMap['freight_name'].toString(),
          marketer_id: ownerMap['marketer_id'].toString(),
          owner_first_name: ownerMap['owner_first_name'].toString(),
          owner_last_name: ownerMap['owner_last_name'].toString(),
          owner_phone_number: ownerMap['owner_phone_number'].toString(),
          password: ownerMap['password'].toString(),
          point: ownerMap['point'].toString(),
          push_notification_token:
              ownerMap['push_notification_token'].toString(),
          status: ownerMap['status'].toString(),
          telephone_number: ownerMap['telephone_number'].toString(),
          updated_at: ownerMap['updated_at'].toString(),
          username: ownerMap['username'].toString(),
        );



        //CarrierDetail.fromJson(res);

      }
    }
  }
}

class CargoOwnerDetail {
  var id,
      first_name,
      last_name,
      company_name,
      national_code,
      address,
      latitude,
      longitude,
      postal_code,
      phone_number,
      email,
      status,
      validation_code,
      credit,
      api_token,
      push_notification_token,
      marketer_id,
      city_id,
      cargo_request_count,
      created_at,
      updated_at;

  CargoOwnerDetail({
    this.id,
    this.first_name,
    this.last_name,
    this.company_name,
    this.national_code,
    this.address,
    this.latitude,
    this.longitude,
    this.postal_code,
    this.phone_number,
    this.email,
    this.status,
    this.validation_code,
    this.credit,
    this.api_token,
    this.push_notification_token,
    this.marketer_id,
    this.city_id,
    this.cargo_request_count,
    this.created_at,
    this.updated_at,
  });
}

class CarrierDetail {
  var id,
      freight_name,
      username,
      password,
      owner_first_name,
      owner_last_name,
      owner_phone_number,
      telephone_number,
      status,
      credit,
      point,
      api_token,
      push_notification_token,
      marketer_id,
      city_id,
      created_at,
      updated_at;

  CarrierDetail({
    this.id,
    this.freight_name,
    this.username,
    this.password,
    this.owner_first_name,
    this.owner_last_name,
    this.owner_phone_number,
    this.telephone_number,
    this.status,
    this.credit,
    this.point,
    this.api_token,
    this.push_notification_token,
    this.marketer_id,
    this.city_id,
    this.created_at,
    this.updated_at,
  });

  CarrierDetail.fromJson(res) {
    Map<String, dynamic> carrierDriverMap = res['carrier_driver'];

    if (carrierDriverMap != null) {}
  }
}

class _Data {
  String id, title;
  RequestSteps steps;

  _Data(this.id, this.title, this.steps);
}
