import 'dart:convert';

import 'package:arzaan_sara/resources/save.dart';

class UserModel {
  var id,
      first_name,
      last_name,
      phone_number,
      sms_validation,
      status,
      email,
      email_verified_at,
      validation_code,
      credit,
      city_id,
      api_token,
      push_notification_token,
      remember_token,
      discount_price,
      created_at,
      updated_at;

  UserModel.fromJson(parsedJson) {
    Map<String, dynamic> data = parsedJson;

    id = data['id'].toString();
    first_name = data['first_name'].toString();
    last_name = data['last_name'].toString();
    phone_number = data['phone_number'].toString();
    sms_validation = data['sms_validation'].toString();
    status = data['status'].toString();
    email = data['email'].toString();
    email_verified_at = data['email_verified_at'].toString();
    validation_code = data['validation_code'].toString();
    credit = data['credit'].toString();
    api_token = data['api_token'].toString();
    push_notification_token = data['push_notification_token'].toString();
    remember_token = data['remember_token'].toString();
    discount_price = data['discount_price'].toString();
    created_at = data['created_at'].toString();
    updated_at = data['updated_at'].toString();

    Save().setToken(api_token);
  }
}
