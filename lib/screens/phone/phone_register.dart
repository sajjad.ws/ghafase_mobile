import 'dart:async';
import 'dart:convert';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:arzaan_sara/blocs/logIn_bloc.dart';
import 'package:arzaan_sara/constants/enums.dart';
import 'package:arzaan_sara/constants/strings.dart';
import 'package:arzaan_sara/global/alerts/alerts.dart';
import 'package:arzaan_sara/global/check/checks.dart';

import 'package:arzaan_sara/global/common_views/button_text.dart';
import 'package:arzaan_sara/global/common_views/subtitle_text.dart';
import 'package:arzaan_sara/global/common_views/title_text.dart';
import 'package:arzaan_sara/global/modules/overly_alert.dart';
import 'package:arzaan_sara/global/views/easy_button.dart';
import 'package:arzaan_sara/global/views/edit_text.dart';
import 'package:arzaan_sara/global/views/edit_text_form.dart';
import 'package:arzaan_sara/global/views/nonet_dialog.dart';
import 'package:arzaan_sara/models/const_model/setting_model.dart';
import 'package:arzaan_sara/resources/save.dart';
import 'package:arzaan_sara/screens/profile/profile_form.dart';
import 'package:url_launcher/url_launcher.dart';
//import 'package:sms/sms.dart';

import '../home.dart';

class PhoneRegister extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return PhoneRegisterState();
  }
}

class PhoneRegisterState extends State<PhoneRegister>
    with SingleTickerProviderStateMixin {
  EditTextController phoneCtrl = EditTextController();
  TextEditingController _controller = TextEditingController();

  int steps = 0;
  int maxLength = 11;
  bool subWidgetVisibility = false;
  bool progressBarVisibility = false;
  bool shouldCheckRegex = true;
  String phoneHStr = "شماره موبایل";
  String titleStr = "شماره همراه";
  String subtitleStr = "لطفا شماره تماس خود را وارد نمایید";
  String buttonStr = "دریافت کد فعال سازی";
  String phone = "";
  String headerImage = "assets/images/login_1.jpeg";
  Color resendSmsColor = Colors.black38;

  Timer _timer;
  int _start = Strings.resendSmsTime;
  bool isTimerFinish = false;
  bool isLoading = false;

  final firebaseMessaging = FirebaseMessaging();

  @override
  void dispose() {
    _timer?.cancel();
    super.dispose();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    generateFirebaseToken();
    //getSMS();
  }

  generateFirebaseToken() {
    firebaseMessaging.requestNotificationPermissions(
        const IosNotificationSettings(sound: true, alert: true, badge: true));

    firebaseMessaging.getToken().then((token) {
      Save().setFirebaseToken(token);
      print(token);
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      resizeToAvoidBottomInset: true,
      resizeToAvoidBottomPadding: true,
      backgroundColor: Strings.bgColor,
      body: ListView(
        children: <Widget>[
          SizedBox(height: 8,),
          Container(
            height: MediaQuery.of(context).size.height / 3,
            alignment: Alignment.center,
            child: Image.asset(headerImage),
          ),
          Padding(
            padding: EdgeInsets.all(16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                TitleText(titleStr),
                Padding(
                  padding: EdgeInsets.all(8),
                ),
                SubtitleText(subtitleStr),
                Padding(
                  padding: EdgeInsets.all(8),
                ),
                EditText.checker(
                  hint: phoneHStr,
                  textInput: TextInputType.phone,
                  textAlign: TextAlign.left,
                  maxLength: maxLength,
                  editTextController: phoneCtrl,
                  controller: _controller,
                  checkRegex: shouldCheckRegex,
                ),
                Padding(
                  padding: EdgeInsets.all(8),
                ),
                subWidget(),
                EasyButton(
                  onPressed: isLoading?null:okClick,
                  isFilled: true,
                  text:isLoading?"لطفا کمی صبر کنید...":buttonStr,
                )
              ],
            ),
          ),
          IntrinsicHeight(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                InkWell(onTap: ()async{
                  SettingModel setting = await Save().getSettingData();
                  String url = setting.driver_guid;
                  launch(url);
                },child: ButtonText("راهنما"),),
                Container(
                  color: Colors.black38,
                  width: 2,
                  margin:
                  EdgeInsets.only(top: 2, bottom: 2, right: 8, left: 8),
                ),
                InkWell(onTap: (){
                  launch("tel:"+Strings.supportPhone);
                },child: ButtonText("تماس با پشتیبانی"),),
              ],
            ),
          )
        ],
      ),
    );
  }

  ///////////////////////////////// widgets /////////////////////////

  Widget subWidget() {
    return Visibility(
      visible: subWidgetVisibility,
      child: Container(
        margin: EdgeInsets.only(bottom: 8),
        child: IntrinsicHeight(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              InkWell(
                onTap: isTimerFinish ? resendSmsClick : null,
                child: Text(
                  isTimerFinish
                      ? "ارسال مجدد"
                      : "ثانیه تا ارسال مجدد"+" $_start ",
                  style: TextStyle(color: resendSmsColor),
                ),
              ),
              Container(
                color: Colors.black38,
                width: 2,
                margin: EdgeInsets.only(top: 2, bottom: 2, right: 8, left: 8),
              ),
              InkWell(
                onTap: backStep,
                child: Text(
                  "ویرایش شماره همراه",
                  style: TextStyle(color: Theme.of(context).accentColor),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  /////////////////////////////// steps work ////////////////////////////////
  void okClick() async{
    setState(() {
      progressBarVisibility = true;
      isLoading = true;
    });

    if (steps == 0) {
      if (phoneCtrl.isAccepted) {
        //String pushNotificationToken = await Save().getFirebaseToken();

        logInReceivePhoneNumber.fetchReceivePhoneNumber(
          apiAddress: "receive-phone-number",
          body: {"phone_number": _controller.text,
            "push_notification_token": "-"},
        ).then((res) {
          setState(() {
            isLoading = false;

          });

          if (res == "success") {
            step0();

          }else{
            connectionError();
          }
        });
      } else {
        textFieldIsNotAccepted();
      }
    } else if (steps == 1) {
      if (phoneCtrl.isAccepted) {
        logInReceivePhoneNumber.fetchVerifyPhoneNumber(
          apiAddress: "verify-phone-number",
          body: {"phone_number": phone, "sms_code": _controller.text},
        ).then((res) {

          setState(() {
            isLoading = false;

          });


          if(res != "fail"){
            var userData = json.decode(res);




            Save().setUserData(res);
            Save().setPhoneInserted(true);
            Navigator.pop(context);
/*


            if (userData['first_name'] != null || userData['status'] == 1) {
              Save().setFormFilled(true);
              Save().setLogin(true);
              Save().setCarType(userData['vehicle_id'].toString());

              Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                      builder: (context) => Home(
                        driverVerifyType: DriverVerifyType.confirmed,
                      )));
            } else {
              Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                      builder: (context) => ProfileForm(
                        type: ProfileFormType.register,
                      )));
            }
*/

          }else{
            connectionError();
          }

        });
      } else {
        textFieldIsNotAccepted();
      }
    }
  }

  step0() {
    startTimer();

    setState(() {
      phoneCtrl.isAccepted = false;
      phone = _controller.text;
      phoneHStr = "کد تایید";
      buttonStr = "تایید";
      titleStr = "تایید شماره همراه";
      headerImage = "assets/images/login_2.jpeg";
      subtitleStr = "کد تایید شما به شماره همراه " +
          phone +
          " ارسال شد." +
          "\n" +
          "لطفا آن را وارد نمایید.";
      _controller.text = "";
      steps = 1;
      maxLength = 5;
      shouldCheckRegex = false;
      progressBarVisibility = false;
      subWidgetVisibility = true;
    });
  }

  step1(String value) {
    if (value != "nok") {
      /*Save().setUserData(value).whenComplete(navigate);*/

    } else {
      OverlyAlert.show("کد تایید اشتباه است", context,
          duration: OverlyAlert.LENGTH_LONG,
          gravity: OverlyAlert.BOTTOM,
          backgroundColor: Colors.red);
    }
  }

  navigate() async {
    /* Navigator.of(context)..pop()..pushReplacement( new MaterialPageRoute(builder: ((context) => Forms())));*/
  }

  //////////////////////////////// actions ////////////////////////////////
  void resendSmsClick() {
    if (isTimerFinish) {
      setState(() {
        _start = Strings.resendSmsTime;
      });
      isTimerFinish = false;
      resendSmsColor = Colors.black38;
      startTimer();


      logInReceivePhoneNumber.fetchReceivePhoneNumber(apiAddress: "receive-phone-number",body: {
        "phone_number": phone,
      },).then((res){
        setState(() {
          isLoading = false;

        });

      });
    }
  }

  backStep() {
    _timer.cancel();

    setState(() {
      phone = "";
      phoneHStr = "شماره موبایل";
      buttonStr = "دریافت کد فعال سازی";
      titleStr = "شماره همراه";
      headerImage = "assets/images/login_1.jpeg";
      subtitleStr = "لطفا شماره تماس خود را وارد نمایید";
      _controller.text = "";
      steps = 0;
      maxLength = 11;
      shouldCheckRegex = true;
      progressBarVisibility = false;
      subWidgetVisibility = false;

      resendSmsColor = Colors.black38;
    });
  }

  ///////////////////////////////// timer Work //////////////////////////////
  startTimer() {
    const oneSec = const Duration(seconds: 1);
    _timer = new Timer.periodic(
        oneSec,
        (Timer timer) => setState(() {
              if (_start < 1) {
                timer.cancel();
                isTimerFinish = true;
                timerFinish();
              } else {
                _start = _start - 1;
              }
            }));
  }

  timerFinish() {
    resendSmsColor = Theme.of(context).accentColor;
  }

/*
  void getSMS() async {
    SmsReceiver receiver = new SmsReceiver();
    receiver.onSmsReceived.listen((SmsMessage msg) {
      setState(() {
        if (Checks.isNumeric(msg.body) && msg.body.length == 5) {
          _controller.text = msg.body;
          phoneCtrl.isAccepted = true;

          */
/* loginBloc
                  .sendPhoneWithCode(
                  phone: phone, code: _controller.text, context: context)
                  .then(step1);*//*

          step1("aaa");
        }
      });
    });
  }
*/




  void connectionError() {
    if(steps == 0){

      setState(() {
        phone = "";
        _controller.text = "";
        buttonStr = "دریافت کد فعال سازی";
      });
      Alerts.error(context, "مشکلی در ارتباط با سرور بوجود آمده است");
    }else{


      setState(() {
        phoneCtrl.isAccepted = false;
        phoneHStr = "کد تایید";
        buttonStr = "تایید";
        titleStr = "تایید شماره همراه";
        _controller.text = "";
      });

      Alerts.error(context, "کد تایید بدرستی وارد نشده است");
    }



  }

  void textFieldIsNotAccepted() {

    if(steps == 0){
      setState(() {
        isLoading = false;
        buttonStr = "دریافت کد فعال سازی";
        progressBarVisibility = false;
      });
      Alerts.error(context,"لطفا شماره همراه خود را بدرستی وارد نمایید");
    }else{

      setState(() {
        isLoading = false;
        buttonStr = "تایید";
        progressBarVisibility = false;
      });

      Alerts.error(context,"لطفا کد تایید را بدرستی وارد نمایید");

    }

  }
}
