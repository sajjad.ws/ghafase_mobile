import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:arzaan_sara/blocs/logIn_bloc.dart';
import 'package:arzaan_sara/constants/strings.dart';
import 'package:arzaan_sara/global/check/checks.dart';
import 'package:arzaan_sara/global/common_views/button_text.dart';
import 'package:arzaan_sara/global/common_views/subtitle_text.dart';
import 'package:arzaan_sara/global/common_views/title_text.dart';
import 'package:arzaan_sara/global/modules/overly_alert.dart';
import 'package:arzaan_sara/global/views/easy_button.dart';
import 'package:arzaan_sara/global/views/edit_text.dart';
import 'package:arzaan_sara/global/views/edit_text_form.dart';
import 'package:arzaan_sara/resources/save.dart';
//import 'package:sms/sms.dart';

class PhoneUpdate extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return PhoneUpdateState();
  }
}

class PhoneUpdateState extends State<PhoneUpdate> with SingleTickerProviderStateMixin {
  EditTextController phoneCtrl = EditTextController();
  TextEditingController _controller = TextEditingController();


  int steps = 0;
  int maxLength = 11;
  bool subWidgetVisibility = false;
  bool progressBarVisibility = false;
  bool shouldCheckRegex = true;
  String phoneHStr = "شماره موبایل";
  String titleStr = "شماره همراه";
  String subtitleStr = "لطفا شماره تماس خود را وارد نمایید";
  String buttonStr = "دریافت کد فعال سازی";
  String phone = "";
  Color resendSmsColor = Colors.black38;


  Timer _timer;
  int _start = Strings.resendSmsTime;
  bool isTimerFinish = false;

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    //getSMS();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return WillPopScope(
      onWillPop: onBackPress,
      child: Scaffold(
        resizeToAvoidBottomInset: true,
        resizeToAvoidBottomPadding: true,
        body: SafeArea(
          child: ListView(
            children: <Widget>[
              SizedBox(
                height: MediaQuery.of(context).size.height/4,
              ),
              Container(
                child: Card(
                  margin: EdgeInsets.all(16),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8)),
                  elevation: 4,
                  child: Padding(
                    padding: EdgeInsets.all(16),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: <Widget>[
                        TitleText(titleStr),
                        Padding(
                          padding: EdgeInsets.all(8),
                        ),
                        SubtitleText(subtitleStr),
                        Padding(
                          padding: EdgeInsets.all(8),
                        ),
                        EditText.checker(
                          hint: phoneHStr,
                          textInput: TextInputType.phone,
                          textAlign: TextAlign.left,
                          maxLength: maxLength,
                          editTextController: phoneCtrl,
                          controller: _controller,
                          checkRegex: shouldCheckRegex,

                        ),
                        Padding(
                          padding: EdgeInsets.all(8),
                        ),
                        subWidget(),
                        EasyButton(
                          onPressed: okClick,
                          isFilled: true,
                          text: buttonStr,
                        )
                      ],
                    ),
                  ),
                ),
              ),

            ],
          ),
        ),
      ),
    );
  }


  ///////////////////////////////// widgets /////////////////////////

  Widget subWidget() {
    return Visibility(
      visible: subWidgetVisibility,
      child: Container(
        margin: EdgeInsets.only(bottom: 8),
        child: IntrinsicHeight(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              InkWell(
                onTap: isTimerFinish?resendSmsClick:null,
                child: Text(
                  isTimerFinish?"ارسال مجدد":"$_start " + "ثانیه تا ارسال مجدد",
                  style: TextStyle(color: resendSmsColor),
                ),
              ),
              Container(
                color: Colors.black38,
                width: 2,
                margin: EdgeInsets.only(top: 2, bottom: 2,right: 8,left: 8),
              ),
              InkWell(
                onTap: backStep,
                child: Text(
                  "ویرایش شماره همراه",
                  style: TextStyle(color: Theme.of(context).accentColor),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  /////////////////////////////// steps work ////////////////////////////////
  void okClick() {

    setState(() {
      progressBarVisibility = true;
    });

    if (steps == 0) {
      if(phoneCtrl.isAccepted) {
        logInReceivePhoneNumber.fetchReceivePhoneNumber(apiAddress: "receive-phone-number",body: {
          "phone_number": _controller.text
        },).then((res){
          if(res == "success"){
            step0();
          }
        });

      }else{
        setState(() {
          progressBarVisibility = false;
        });

        alert("لطفا شماره همراه خود را وارد نمایید");

      }

    } else if (steps == 1) {

      if(phoneCtrl.isAccepted) {

        logInReceivePhoneNumber.fetchVerifyPhoneNumber(apiAddress: "verify-phone-number",body: {
          "phone_number": phone,
          "sms_code": _controller.text
        },).then((res){
          var userData = json.decode(res);


          Save().setUserData(res);



        });

      }else{

        setState(() {
          progressBarVisibility = false;
        });

        alert("لطفا کد تایید را وارد نمایید");

      }

    }

  }

  step0() {

    startTimer();

    setState(() {
      phoneCtrl.isAccepted = false;
      phone = _controller.text;
      phoneHStr = "کد تایید";
      buttonStr = "تایید";
      titleStr = "تایید شماره همراه";
      subtitleStr = "کد تایید شما به شماره همراه " +
          phone +
          " ارسال شد." +
          "\n" +
          "لطفا آن را وارد نمایید.";
      _controller.text = "";
      steps = 1;
      maxLength = 5;
      shouldCheckRegex = false;
      progressBarVisibility = false;
      subWidgetVisibility = true;
    });

  }

  step1(String value) {
    if (value != "nok") {

      /*Save().setUserData(value).whenComplete(navigate);*/
    } else {
      OverlyAlert.show("کد تایید اشتباه است", context,
          duration: OverlyAlert.LENGTH_LONG,
          gravity: OverlyAlert.BOTTOM,
          backgroundColor: Colors.red);
    }
  }

  navigate() async {

    /* Navigator.of(context)..pop()..pushReplacement( new MaterialPageRoute(builder: ((context) => Forms())));*/

  }

  //////////////////////////////// actions ////////////////////////////////
  void resendSmsClick() {
    if (isTimerFinish) {
      setState(() {
        _start = Strings.resendSmsTime;
      });
      isTimerFinish = false;
      resendSmsColor = Colors.black38;
      startTimer();
      /*loginBloc.sendPhone(phone: phone);*/
    }
  }

  backStep() {
    _timer.cancel();

    setState(() {
      phone = "";
      phoneHStr = "شماره موبایل";
      buttonStr = "دریافت کد فعال سازی";
      titleStr = "شماره همراه";
      subtitleStr = "لطفا شماره تماس خود را وارد نمایید";
      _controller.text = "";
      steps = 0;
      maxLength = 11;
      shouldCheckRegex = true;
      progressBarVisibility = false;
      subWidgetVisibility = false;

      resendSmsColor = Colors.black38;
    });
  }

  ///////////////////////////////// timer Work //////////////////////////////
  startTimer() {
    const oneSec = const Duration(seconds: 1);
    _timer = new Timer.periodic(
        oneSec,
            (Timer timer) => setState(() {
          if (_start < 1) {
            timer.cancel();
            isTimerFinish = true;
            timerFinish();
          } else {
            _start = _start - 1;
          }
        }));
  }

  timerFinish() {
    resendSmsColor = Theme.of(context).accentColor;
  }


/*
  void getSMS()async {
    SmsReceiver receiver = new SmsReceiver();
    receiver.onSmsReceived.listen(
            (SmsMessage msg) {

          setState(() {


            if(Checks.isNumeric(msg.body) && msg.body.length == 5){
              _controller.text = msg.body;
              phoneCtrl.isAccepted = true;

             */
/* loginBloc
                  .sendPhoneWithCode(
                  phone: phone, code: _controller.text, context: context)
                  .then(step1);*//*

              step1("aaa");
            }
          });

        });
  }
*/



  Future<bool> onBackPress()async {
    //bool isLogin = await Save().isLogin();
    //Navigator.of(context)..pop()..pushReplacement( new MaterialPageRoute(builder: ((context) => MainAds(isLogin: isLogin,))));
  }

  void alert(String text) {

    OverlyAlert.show(text, context,
        duration: OverlyAlert.LENGTH_LONG,
        gravity: OverlyAlert.BOTTOM,
        backgroundColor: Colors.red,
        textColor: Colors.white);

  }


}
