import 'package:arzaan_sara/constants/strings.dart';
import 'package:arzaan_sara/global/common_views/subtitle_text.dart';
import 'package:arzaan_sara/global/common_views/title_text.dart';
import 'package:arzaan_sara/global/converter/date_time_converter.dart';
import 'package:arzaan_sara/global/formatter/num_fornat.dart';
import 'package:arzaan_sara/global/views/easy_button.dart';
import 'package:arzaan_sara/models/product_model.dart';
import 'package:arzaan_sara/resources/save.dart';
import 'package:arzaan_sara/screens/phone/phone_register.dart';
import 'package:arzaan_sara/tests/login_form.dart';
import 'package:arzaan_sara/widgets/outside_clipper_appbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ProductDetail extends StatefulWidget {
  Products model;

  ProductDetail({this.model});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return ProductDetailState();
  }
}

class ProductDetailState extends State<ProductDetail> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        backgroundColor: Strings.bgColor,
        body: Stack(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(
                  top: MediaQuery.of(context).size.width * 2 / 7 -
                      MediaQuery.of(context).size.width / 12,
                  bottom: MediaQuery.of(context).size.height / 8 -
                      MediaQuery.of(context).size.width / 18),
              child: pageContent(),
            ),
            ClipPath(
              clipper: OutsideClipperAppbar(),
              child: Container(
                height: MediaQuery.of(context).size.width * 2 / 7,
                padding: EdgeInsets.only(bottom: 8),
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                        begin: Alignment.topRight,
                        end: Alignment.bottomLeft,
                        colors: [Strings.greenLight, Strings.greenDark])),
                child: appBar(),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget pageContent() {
    return ListView(
      children: <Widget>[
        Container(
          height: MediaQuery.of(context).size.height / 4,
          child: Stack(
            fit: StackFit.expand,
            children: <Widget>[
              Image.asset(
                "assets/images/product.png",
                height: MediaQuery.of(context).size.height / 4,
              ),
              Align(
                alignment: Alignment.topLeft,
                child: Container(
                  margin: EdgeInsets.only(left: 16),
                  padding:
                      EdgeInsets.only(right: 8, left: 8, top: 8, bottom: 4),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(50),
                      color: Colors.red),
                  child: Text(
                    "%${widget.model.discount}",
                    style: TextStyle(color: Colors.white, fontSize: 16),
                  ),
                ),
              )
            ],
          ),
        ),
        SizedBox(
          height: 8,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(left: 8, right: 8, bottom: 2, top: 2),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(50)),
                  border: Border.all(color: Strings.greenLight)),
              child: Row(
                children: <Widget>[
                  Image.asset(
                    "assets/images/recycle.jpeg",
                    height: 40,
                    width: 40,
                  ),
                  SizedBox(
                    width: 8,
                  ),
                  Text(
                    "یک سهم در جلوگیری از اسراف",
                    style: TextStyle(color: Strings.greenLight),
                  ),
                ],
              ),
            )
          ],
        ),
        SizedBox(
          height: 16,
        ),
        Container(
          padding: EdgeInsets.only(left: 16, right: 16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              TitleText(
                widget.model.name,
                textAlign: TextAlign.right,
              ),
              SizedBox(
                height: 4,
              ),
              SubtitleText(
                "دسته بندی محصول : "+widget.model.categoryDetail.name,
                textAlign: TextAlign.right,
              ),
              SizedBox(
                height: 16,
              ),
              rows("ویژگی ها : ", "12 kg"),
              SizedBox(
                height: 8,
              ),
              rows("فروشنده : ", widget.model.storeDetail.name),
              SizedBox(
                height: 8,
              ),
              rows("تاریخ تولید : ", DateTimeConverter.getToJalDate(widget.model.p_date)),
              SizedBox(
                height: 8,
              ),
              rows("تاریخ انفضا : ", DateTimeConverter.getToJalDate(widget.model.e_date)),
              SizedBox(
                height: 8,
              ),
              Row(
                children: <Widget>[
                  TitleText("قیمت :  "),
                  Expanded(
                    child: Text(
                      "${NumFormat().getPriceFormat(int.parse(widget.model.price))}" +
                          " تومان",
                      textAlign: TextAlign.right,
                      style: TextStyle(
                          color: Colors.red,
                          decoration: TextDecoration.lineThrough,
                          fontSize: 12),
                    ),
                  ),
                  Text(
                    "${NumFormat().getPriceFormat(int.parse(widget.model.discount_price))}" +
                        " تومان",
                    textAlign: TextAlign.right,
                    style: TextStyle(fontSize: 12),
                  )
                ],
              ),
              SizedBox(
                height: 16,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  FlatButton(
                      onPressed: ()async {
                        bool isLogin = await Save().isLogin();
                        if(isLogin){

                        }else{
                          Navigator.push(context, MaterialPageRoute(builder: (context)=>PhoneRegister()));
                        }


                      },
                      color: Theme.of(context).accentColor,
                      shape: OutlineInputBorder(
                          borderSide:
                          BorderSide(color: Theme.of(context).primaryColor),
                          borderRadius: BorderRadius.circular(50)),
                      padding:
                      EdgeInsets.only(top: 10, bottom: 10, right: 11, left: 11),
                      child: Row(
                        children: <Widget>[
                          SizedBox(width: 16,),
                          Image.asset("assets/images/handle.png",height: 30,),
                          SizedBox(width: 8,),
                          Text(
                            "افزودن به سبد خرید",
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 16),
                            textDirection: TextDirection.rtl,
                            textAlign: TextAlign.center,
                          ),
                          SizedBox(width: 16,),
                        ],
                      ))
                ],
              )
            ],
          ),
        )
      ],
    );
  }

  Widget rows(String title, String content) {
    return Row(
      children: <Widget>[
        TitleText(title),
        SubtitleText(content),
      ],
    );
  }

  Widget appBar() {
    return Row(
      children: <Widget>[
        Expanded(
          child: Padding(
            padding: EdgeInsets.only(right: 16),
            child: Text(
              "موجود در فروشگاه",
              style: TextStyle(color: Colors.white),
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(right: 8),
          child: IconButton(
            onPressed: (){
              Navigator.pop(context);
            },
            icon: Icon(
              Icons.arrow_forward,
              color: Colors.white,
            ),
          ),
        ),
      ],
    );
  }
}
