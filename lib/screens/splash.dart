
import 'package:arzaan_sara/constants/strings.dart';
import 'package:arzaan_sara/tests/home_test.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:arzaan_sara/blocs/logIn_bloc.dart';
import 'package:arzaan_sara/constants/enums.dart';
import 'package:arzaan_sara/global/views/nonet_dialog.dart';
import 'package:arzaan_sara/models/const_model/initial_data_model.dart';
import 'package:arzaan_sara/models/user_model.dart';
import 'package:arzaan_sara/resources/save.dart';
import 'package:arzaan_sara/screens/intro.dart';
import 'package:arzaan_sara/screens/phone/phone_register.dart';

import 'home.dart';

class Splash extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return SplashState();
  }
}

class SplashState extends State<Splash> {
  Stream myStream;
  final firebaseMessaging = FirebaseMessaging();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();


    checkStateForFetch();

    generateFirebaseToken();
  }

  generateFirebaseToken() {


    firebaseMessaging.requestNotificationPermissions(
        const IosNotificationSettings(sound: true, alert: true, badge: true));


    firebaseMessaging.getToken().then((token) {
      Save().setFirebaseToken(token);
      print(token);
    });


  }


  checkStateForFetch() async {
    bool isLogin = await Save().isLogin();

    if (isLogin) {
      setState(() {
        myStream = logInBlocUserData.actions;
      });
      sendUserDataRequest();
    } else {

      setState(() {
        myStream = logInBlocInitialData.actions;
      });
      logInBlocInitialData.fetchInitialData(
        apiAddress: "get-initial-data",
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: StreamBuilder(
          stream: myStream,
          builder: (context, AsyncSnapshot snapshot) {


            if (snapshot.hasError && (snapshot.error =="nc"|| snapshot.error == "to")) {
              return NonetDialog(
                onPress: () async {
                  bool isLogin = await Save().isLogin();

                  if (isLogin) {
                    sendUserDataRequest();
                  } else {
                    logInBlocInitialData.fetchInitialData(
                      apiAddress: "get-initial-data",
                    );
                  }
                },
              );
            }
            if (snapshot.hasError && snapshot.error == "null") {

              WidgetsBinding.instance.addPostFrameCallback((res){
                Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                        builder: (context) => Home(
                          driverVerifyType: DriverVerifyType.notRegister,
                        )));
              });
              return Container();
            }
            if (snapshot.hasData) {

              WidgetsBinding.instance.addPostFrameCallback((dur) {
                nextPage(snapshot.data);
              });

              return Container();
            }

            return Stack(
              children: <Widget>[
                Image.asset(
                  'assets/images/splash_bg.png',
                  fit: BoxFit.fill,
                  width: double.maxFinite,
                ),
                Center(
                  child: Column(
                    children: <Widget>[
                      Expanded(
                        flex: 10,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Image.asset("assets/images/logo_splash.png"),
                            Container(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text("قفسه"+" | ", style: TextStyle(fontSize: 22,color: Strings.green,
                                      fontWeight: FontWeight.bold),),
                                  Text("www.ghafaseapp.ir"),


                                ],
                              ),
                            ),

                            Text(
                              "آرمان نهایی ما، ایرانی بدون پسماند",
                              style: TextStyle(
                                  color: Strings.green,
                                  fontSize: 18,),
                            ),
                          ],
                        ),
                      ),
                      Expanded(
                        child: Align(
                          child: Padding(
                            padding: EdgeInsets.only(bottom: 16),
                            child: Text(
                              "نسخه : 1.0.0",
                              style: TextStyle(
                                  color: Theme.of(context).accentColor),
                            ),
                          ),
                          alignment: Alignment.bottomCenter,
                        ),
                      )
                    ],
                  ),
                ),
              ],
            );
          }),
    );
  }

  void nextPage(data) async {
    bool isFirstTime = await Save().isFirstTime();
    bool isLogin = await Save().isLogin();


    if (isFirstTime && !isLogin) {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => Intro()));
    } else if (!isFirstTime && !isLogin) {
      Navigator.pushReplacement(
          context,
          MaterialPageRoute(
              builder: (context) => HomeTest(
                driverVerifyType: DriverVerifyType.notRegister,
              )));
    } else if (!isFirstTime && isLogin) {
      UserModel um = data;
      if (um.status == "1") {
        Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (context) => HomeTest(
                      driverVerifyType: DriverVerifyType.confirmed,
                    )));
      } else if (um.status == "0") {
        Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (context) => HomeTest(
                      driverVerifyType: DriverVerifyType.registered,
                    )));
      }else if (um.status == "null") {
        Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (context) => HomeTest(
                  driverVerifyType: DriverVerifyType.registered,
                )));
      }
    }
  }

  sendUserDataRequest(){


    logInBlocUserData.fetchUserData(
      body: {},
      fullResponse: (Response res){
        print(res.statusCode);
        if(res.statusCode == 407 || res.statusCode == 500) {


          Save().clearSavedData();
          Save().setFirstTime(false);
          Navigator.pushReplacement(
              context, MaterialPageRoute(builder: (context) => Home(driverVerifyType: DriverVerifyType.notRegister,)));

        }
      },
      directResult: (res) async {

        Save().setUserData(res);
      },
      apiAddress: "get-driver",
    );
  }


}
