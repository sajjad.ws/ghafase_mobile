import 'package:arzaan_sara/screens/phone/phone_register.dart';
import 'package:arzaan_sara/tests/home_test.dart';
import 'package:arzaan_sara/tests/login_phone.dart';
import 'package:arzaan_sara/widgets/intro/intro_component.dart';
import 'package:flutter/material.dart';
import 'package:arzaan_sara/constants/strings.dart';
import 'package:arzaan_sara/global/modules/material_intro.dart';
import 'package:arzaan_sara/resources/save.dart';

import 'home.dart';

class Intro extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return IntroState();
  }
}

class IntroState extends State<Intro> {


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
        padding: EdgeInsets.only(top: 8,),
        child:IntroComponent(
          dataList: [
            {'image': "assets/images/sp1.png", 'text': "فقسه به شما این امکان را می دهد تا در کمترین زمان ممکن ارزان ترین خرید را داشته باشید",'title': "تا به حال یک خرید با تخفیف %50 رو تجربه کردی؟"},
            {'image': "assets/images/sp2.png", 'text': "با استفاده از قفسه و خرید ارزان مواد غذایی که نزدیک به تاریخ انقضاء هستند، میتونید نقش مهمی در جلوگیری از این اتفاق داشته باشید",'title': "میدونستی هر سال 33% مواد غذایی تولید شده در کره زمین تبدیل به زباله میشه؟"},
            {'image': "assets/images/sp3.png", 'text': "قفسه یک راه حل مناسب برای احیای درصد زیادی از منابع و سرمایه های کشور است",'title': "واقعا برات مهم نیست منابع کشور در حال تموم شدنه؟"},
          ],
          lastPageTap: () {
            Save().setFirstTime(false);
            Navigator.pushReplacement(
                context, MaterialPageRoute(builder: (context) => HomeTest()));
          },
        )
    );
  }
}
