import 'dart:async';
import 'dart:io';

import 'package:android_intent/android_intent.dart';
import 'package:autocomplete_textfield/autocomplete_textfield.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:arzaan_sara/blocs/map_bloc.dart';
import 'package:arzaan_sara/constants/enums.dart';
import 'package:arzaan_sara/global/map/map_utils.dart';
import 'package:arzaan_sara/global/views/easy_button.dart';
import 'package:arzaan_sara/global/views/flat_button_icon.dart';
import 'package:arzaan_sara/models/search_model.dart';
import 'package:url_launcher/url_launcher.dart';

class MapCargoRoute extends StatefulWidget {
  final LatLng originLatLng;
  final LatLng destinationLatLng;
  final RequestSteps steps;

  MapCargoRoute({this.originLatLng, this.destinationLatLng, this.steps});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return MapCargoRouteState();
  }
}

class MapCargoRouteState extends State<MapCargoRoute> {
  Marker m1;
  Marker m2;
  MarkerId mId1 = MarkerId("1");
  MarkerId mId2 = MarkerId("2");

  List<Marker> markers = [];

  GoogleMapController _controller;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    setMarkers();
  }

  setMarkers() async {
    m1 = await MapUtils.addMarkerSimply(
        widget.originLatLng, mId1, "assets/images/origin_marker.png");
    m2 = await MapUtils.addMarkerSimply(
        widget.destinationLatLng, mId2, "assets/images/destination_marker.png");

    setState(() {
      markers.add(m1);
      markers.add(m2);
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        body: SafeArea(
          child: Stack(
            children: <Widget>[
              GoogleMap(
                onMapCreated: _onMapCreated,
                markers: Set.from(markers),
                initialCameraPosition: CameraPosition(
                  target: widget.originLatLng,
                  zoom: 15.0,
                ),
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Container(
                      alignment: Alignment.centerRight,
                      margin: EdgeInsets.only(right: 24),
                      child: FloatingActionButton(
                        onPressed: () async {
                          var location = new Location();
                          LocationData currentLocation =
                              await location.getLocation();

                          if (_controller != null) {
                            _controller.animateCamera(CameraUpdate.newLatLng(
                                LatLng(currentLocation.latitude,
                                    currentLocation.longitude)));
                          }
                        },
                        child: Icon(
                          Icons.my_location,
                          color: Colors.black,
                        ),
                        backgroundColor: Colors.white,
                      ),
                    ),
                    SizedBox(
                      height: 24,
                    ),
                    Padding(
                      padding: EdgeInsets.only(right: 24,bottom: 8,left: 24),
                      child: EasyButton(
                        text: "مسیریابی روی نقشه",
                        isFilled: true,
                        textColor: Colors.white,
                        onPressed: () async {
                          String origin =
                              widget.originLatLng.latitude.toString() +
                                  "," +
                                  widget.originLatLng.longitude
                                      .toString(); // lat,long like 123.34,68.56
                          String destination =
                              widget.destinationLatLng.latitude.toString() +
                                  "," +
                                  widget.destinationLatLng.longitude.toString();
                          if (Platform.isAndroid) {
                            final AndroidIntent intent = new AndroidIntent(
                                action: 'action_view',
                                data: Uri.encodeFull(
                                    "https://www.google.com/maps/dir/?api=1&origin=" +
                                        origin +
                                        "&destination=" +
                                        destination +
                                        "&travelmode=driving&dir_action=navigate"),
                                package: 'com.google.android.apps.maps');
                            intent.launch();
                          } else {
                            String url =
                                "https://www.google.com/maps/dir/?api=1&origin=" +
                                    origin +
                                    "&destination=" +
                                    destination +
                                    "&travelmode=driving&dir_action=navigate";
                            if (await canLaunch(url)) {
                              await launch(url);
                            } else {
                              throw 'Could not launch $url';
                            }
                          }
                        },
                      ),
                    )
                  ],
                ),
              ),
              IconButton(
                icon: Icon(
                  Icons.arrow_back,
                ),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _onMapCreated(GoogleMapController controller) async {
    _controller = controller;
  }
}
