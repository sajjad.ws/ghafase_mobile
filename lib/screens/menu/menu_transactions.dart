import 'package:flutter/material.dart';
import 'package:arzaan_sara/blocs/menu_bloc.dart';
import 'package:arzaan_sara/global/common_views/list_empty_view.dart';
import 'package:arzaan_sara/global/common_views/list_net_error.dart';
import 'package:arzaan_sara/models/trasaction_model.dart';
import 'package:arzaan_sara/widgets/menu/transaction_items.dart';

class MenuTransactions extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return MenuTransactionsState();
  }
}

class MenuTransactionsState extends State<MenuTransactions> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    menuBlocTransactions.fetchTransactions(
        apiAddress: "get-driver-transactions-history", body: {});
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text(
            "تراکنش ها",
            style: Theme.of(context).textTheme.headline,
          ),
        ),
        body: StreamBuilder(
          stream: menuBlocTransactions.actions,
          builder: (context, AsyncSnapshot<TransactionModel> snapshot) {
            if (snapshot.hasError) {
              return ListNetError(
                onRefresh: () {
                  menuBlocTransactions.fetchTransactions(
                      apiAddress: "get-driver-transactions-history",
                      body: {});
                },
              );
            }

            if (snapshot.hasData) {
              if (snapshot.data.transactions.isEmpty) {
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Expanded(
                      child: ListEmptyView(
                        onRefresh: () {
                          menuBlocTransactions.fetchTransactions(
                              apiAddress: "get-driver-transactions-history",
                              body: {});
                        },
                      ),
                    )
                  ],
                );
              }

              return ListView.builder(
                  padding: EdgeInsets.only(top: 16),
                  itemCount: snapshot.data.transactions.length,
                  itemBuilder: (context, index) {
                    return TransactionItems(
                      index: index,
                      model: snapshot.data,
                    );
                  });
            }

            return Center(
              child: CircularProgressIndicator(),
            );
          },
        ),
      ),
    );
  }
}
