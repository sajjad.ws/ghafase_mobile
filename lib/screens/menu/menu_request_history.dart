import 'package:flutter/material.dart';
import 'package:liquid_pull_to_refresh/liquid_pull_to_refresh.dart';
import 'package:arzaan_sara/blocs/menu_bloc.dart';
import 'package:arzaan_sara/constants/enums.dart';
import 'package:arzaan_sara/dialogs/dialog_filter.dart';
import 'package:arzaan_sara/dialogs/dialog_filter_history.dart';
import 'package:arzaan_sara/global/common_views/list_empty_view.dart';
import 'package:arzaan_sara/global/common_views/list_net_error.dart';
import 'package:arzaan_sara/global/common_views/subtitle_text.dart';
import 'package:arzaan_sara/global/converter/date_time_converter.dart';
import 'package:arzaan_sara/global/views/flat_button_icon.dart';
import 'package:arzaan_sara/models/const_model/filter_model.dart';
import 'package:arzaan_sara/models/request_model.dart';
import 'package:arzaan_sara/resources/save.dart';
import 'package:arzaan_sara/widgets/menu/request_hitory_items.dart';

class MenuRequestHistory extends StatefulWidget {

  DriverVerifyType verifyType;

  static FilterModel carType = FilterModel("", "");
  static FilterModel originCity = FilterModel("", "");
  static FilterModel destinationCity = FilterModel("", "");
  static FilterModel carryDate = FilterModel("", "");
  static FilterModel evacuationDate = FilterModel("", "");
  static FilterModel cargoType = FilterModel("", "");
  static FilterModel cargoMinWeight = FilterModel("", "");
  static FilterModel cargoMaxWeight = FilterModel("", "");


  MenuRequestHistory({this.verifyType});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return MenuRequestHistoryState();
  }
}

class MenuRequestHistoryState extends State<MenuRequestHistory> {

  List<String> filtersList = [];
  Map<String, String> filtersMap = {};

  bool shouldShowFilterWidget = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    sendSearchRequest();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return WillPopScope(
      onWillPop: ()async{
        MenuRequestHistory.carType = FilterModel("", "");
        MenuRequestHistory.originCity = FilterModel("", "");
        MenuRequestHistory.destinationCity = FilterModel("", "");
        MenuRequestHistory.carryDate = FilterModel("", "");
        MenuRequestHistory.evacuationDate = FilterModel("", "");
        MenuRequestHistory.cargoType = FilterModel("", "");
        MenuRequestHistory.cargoMinWeight = FilterModel("", "");
        MenuRequestHistory.cargoMaxWeight = FilterModel("", "");
        return true;
      },
      child: Directionality(
        textDirection: TextDirection.rtl,
        child: Scaffold(
          appBar: AppBar(
            centerTitle: true,
            title: Text(
              "سوابق سفارش",
              style: Theme.of(context).textTheme.headline,
            ),
          ),
          body:Column(
            children: <Widget>[
              SizedBox(
                height: 8,
              ),
              Padding(
                padding: EdgeInsets.only(left: 8, right: 8),
                child: FlatButtonIcon(
                  fillColor: Colors.grey,
                  borderColor: Colors.grey,
                  icon: Icons.filter_list,
                  iconColor: Colors.white,
                  text: "فیلتر گذاری",
                  textColor: Colors.white,
                  useIcon: true,
                  onTap: onFilterClick,
                ),
              ),
              shouldShowFilterWidget
                  ? Container(
                color: Colors.white,
                height: 50,
                child: ListView.builder(
                    itemCount: filtersList.length,
                    scrollDirection: Axis.horizontal,
                    itemBuilder: (context, index) {
                      return Container(
                        margin: EdgeInsets.all(8),
                        padding: EdgeInsets.fromLTRB(8, 4, 8, 4),
                        decoration: BoxDecoration(
                            border: Border.all(
                              color: Colors.black38,
                              width: 1,
                            ),
                            borderRadius: BorderRadius.all(
                                Radius.elliptical(50, 50))),
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            InkWell(
                              onTap: () => deleteFilter(index),
                              child: Icon(
                                Icons.clear,
                                color: Colors.black38,
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(right: 4,top: 4),
                              child: SubtitleText(
                                filtersList[index],
                              ),
                            ),

                          ],
                        ),
                      );
                    }),
              )
                  : Container(),
              StreamBuilder(
                stream: menuBlocRequestHistory.actions,
                builder: (context, AsyncSnapshot<RequestModel> snapshot) {
                  if(snapshot.hasError){
                    return ListNetError(
                      onRefresh: (){

                        sendSearchRequest();

                      },
                    );
                  }
                  if (snapshot.hasData) {
                    if(snapshot.data.requests.isEmpty){
                      return  Expanded(
                        child: ListEmptyView(
                          onRefresh: (){

                            sendSearchRequest();

                          },
                        ),
                      );
                    }

                    return Expanded(
                      child: LiquidPullToRefresh(
                        onRefresh: () {
                          sendSearchRequest();

                          sendSearchRequest();
                          return;
                        },
                        child: ListView.builder(
                            padding: EdgeInsets.only(top: 16),
                            itemCount: snapshot.data.requests.length,
                            itemBuilder: (context, index) {
                              return RequestHistoryItems(
                                index: index,
                                model: snapshot.data,
                                steps: RequestModel().returnStep(
                                    snapshot.data.requests[index].last_status),
                                verifyType: widget.verifyType,
                              );
                            }), // scroll view
                      ),
                    );
                  }

                  return Expanded(
                    child: Center(
                      child: CircularProgressIndicator(),
                    ),
                  );
                },
              )
            ],
          ),


          /*    StreamBuilder(
          stream: menuBlocRequestHistory.actions,
          builder: (context, AsyncSnapshot<RequestModel> snapshot) {
            print(snapshot.data);

            if (snapshot.hasData) {
              return ListView.builder(
                  padding: EdgeInsets.only(top: 16),
                  itemCount: snapshot.data.requests.length,
                  itemBuilder: (context, index) {
                    return RequestHistoryItems(
                      index: index,
                      model: snapshot.data,
                      steps: RequestModel().returnStep(
                          snapshot.data.requests[index].last_status),
                      verifyType: widget.verifyType,
                    );
                  });
            }

            return Center(
              child: CircularProgressIndicator(),
            );
          },
        ),*/
        ),
      )
    );
  }

  onFilterClick() {
    showDialog(
        context: context,
        builder: (context) => DialogFilterHistory(
          afterOk: (map) {
            if (map.isNotEmpty) {
              filtersMap.clear();
              filtersList.clear();

              filtersMap = map;

              map.forEach((k, v) {
                if (v.isNotEmpty) {
                  filtersList.add(v);
                  setState(() {
                    shouldShowFilterWidget = true;
                  });
                }
              });

              sendSearchRequest();
            } else {
              setState(() {
                shouldShowFilterWidget = false;
              });
            }
          },
        ));
  }

  sendSearchRequest() async{


    menuBlocRequestHistory.fetchRequestHistory(
        apiAddress: "get-driver-request-history", body: {

      "vehicle_id": MenuRequestHistory.carType.id,
      "origin_city_id": MenuRequestHistory.originCity.id,
      "destination_city_id": MenuRequestHistory.destinationCity.id,
      "loading_start_time": DateTimeConverter.getGreDate(MenuRequestHistory.carryDate.title),
      "evacuation_start_time": DateTimeConverter.getGreDate(MenuRequestHistory.evacuationDate.title),
      "cargo_weight_min": MenuRequestHistory.cargoMinWeight.title,
      "cargo_weight_max": MenuRequestHistory.cargoMaxWeight.title,



    });


  }

  void deleteFilter(int index) {
    setState(() {
      shouldShowFilterWidget = true;
    });

    if (filtersMap['carTypeStr'] == filtersList[index]) {
      MenuRequestHistory.carType.id = "";
      MenuRequestHistory.carType.title = "";
    } else if (filtersMap['originCityStr'] == filtersList[index]) {
      MenuRequestHistory.originCity.id = "";
      MenuRequestHistory.originCity.title = "";
    } else if (filtersMap['destinationCityStr'] == filtersList[index]) {
      MenuRequestHistory.destinationCity.id = "";
      MenuRequestHistory.destinationCity.title = "";
    } else if (filtersMap['carryDateStr'] == filtersList[index]) {
      MenuRequestHistory.carryDate.id = "";
      MenuRequestHistory.carryDate.title = "";
    } else if (filtersMap['cargoTypeStr'] == filtersList[index]) {
      MenuRequestHistory.cargoType.id = "";
      MenuRequestHistory.cargoType.title = "";
    } else if (filtersMap['cargoWeightStr'] == filtersList[index]) {
      MenuRequestHistory.cargoMinWeight.title = "";
      MenuRequestHistory.cargoMaxWeight.title = "";
    }
    setState(() {
      filtersList.removeAt(index);
    });

    if (filtersList.isEmpty) {
      setState(() {
        shouldShowFilterWidget = false;
      });
    }
    sendSearchRequest();
  }


}
