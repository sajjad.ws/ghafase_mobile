import 'package:flutter/material.dart';
import 'package:arzaan_sara/constants/strings.dart';
import 'package:arzaan_sara/global/common_views/title_text.dart';
import 'package:arzaan_sara/global/views/flat_button_icon.dart';
import 'package:url_launcher/url_launcher.dart';

class MenuContactUs extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text(
            "تماس با ما",
            style: Theme
                .of(context)
                .textTheme
                .headline,
          ),
        ),
        body: ListView(
          padding: EdgeInsets.only(top: 16),
          children: <Widget>[
            containerContent(
                child: Column(
                  children: <Widget>[
                    Text(
                      "سامانه حمل نقل اوبار",
                      style: TextStyle(
                          fontSize: 20, color: Theme
                          .of(context)
                          .primaryColor),
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    Text(
                      Strings.lorm,
                      textAlign: TextAlign.center,
                    )
                  ],
                )),
            containerContent(
                child: InkWell(
                  onTap: () => {print("")},
                  child: Row(
                    children: <Widget>[
                      Icon(Icons.location_on),
                      Expanded(
                        child: TitleText(
                          "کرمان، خیابان طالقانی",
                          textAlign: TextAlign.center,),
                      ),
                      SizedBox(width: 24,)
                    ],
                  ),
                )),
            containerContent(
                child: InkWell(
                  onTap: () => {print("")},
                  child: Row(
                    children: <Widget>[
                      Icon(Icons.email),
                      Expanded(
                        child: TitleText(
                          "sajjad.ws@gmail.com", textAlign: TextAlign.center,),
                      ),
                      SizedBox(width: 24,)
                    ],
                  ),
                )),
            containerContent(
                child: InkWell(
                  onTap: () => {print("")},
                  child: Row(
                    children: <Widget>[
                      Icon(Icons.phone),
                      Expanded(
                        child: TitleText(
                          "09212098662", textAlign: TextAlign.center,),
                      ),
                      SizedBox(width: 24,)
                    ],
                  ),
                )),
            Padding(padding: EdgeInsets.only(left: 54, right: 54),
                child: FlatButtonIcon(
                  onTap: () {
                    launch("tel:"+Strings.supportPhone);
                  },
                  text: "تماس با پشتیبانی",
                  borderColor: Theme
                      .of(context)
                      .primaryColor,
                  textColor: Colors.white,
                  useIcon: false,
                  fillColor: Theme
                      .of(context)
                      .primaryColor,
                ), )
          ],
        ),
      ),
    );
  }

  Widget containerContent({Widget child}) {
    return Container(
      margin: EdgeInsets.only(bottom: 16, left: 16, right: 16),
      padding: EdgeInsets.all(8),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(8)),
          border: Border.all(color: Colors.black38, width: 1)),
      child: child,
    );
  }
}
