import 'package:flutter/material.dart';
import 'package:arzaan_sara/blocs/menu_bloc.dart';
import 'package:arzaan_sara/constants/dummy.dart';
import 'package:arzaan_sara/global/common_views/list_empty_view.dart';
import 'package:arzaan_sara/global/common_views/list_net_error.dart';
import 'package:arzaan_sara/models/message_model.dart';
import 'package:arzaan_sara/models/trasaction_model.dart';
import 'package:arzaan_sara/widgets/menu/message_items.dart';
import 'package:arzaan_sara/widgets/menu/transaction_items.dart';

class MenuMessages extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return MenuMessagesState();
  }
}

class MenuMessagesState extends State<MenuMessages> {
  
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    
    menuBlocMessages.fetchMessages(apiAddress: "get-driver-messages",body: {});
    
  }
  
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text("پیام ها", style: Theme.of(context).textTheme.headline,),
        ),
        body: StreamBuilder(
          stream: menuBlocMessages.actions,
          builder: (context, AsyncSnapshot<MessageModel> snapshot) {

            if(snapshot.hasError){
              return ListNetError(
                onRefresh: (){

                  menuBlocMessages.fetchMessages(apiAddress: "get-driver-messages",body: {});


                },
              );
            }

            if(snapshot.hasData){
              if(snapshot.data.messages.isEmpty){
                return  Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Expanded(
                      child: ListEmptyView(
                        onRefresh: (){

                          menuBlocMessages.fetchMessages(apiAddress: "get-driver-messages",body: {});


                        },
                      ),
                    )
                  ],
                );
              }


              return ListView.builder(
                  padding: EdgeInsets.only(top: 16),
                  itemCount: snapshot.data.messages.length,
                  itemBuilder: (context, index) {
                    return MessageItems(
                      index: index,
                      model: snapshot.data,
                    );
                  });
            }

            return Center(child: CircularProgressIndicator(),);
          },
        ),
      ),
    );
  }
}
