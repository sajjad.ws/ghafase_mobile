import 'dart:convert';
import 'dart:io';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:arzaan_sara/blocs/logIn_bloc.dart';
import 'package:arzaan_sara/constants/enums.dart';
import 'package:arzaan_sara/constants/status_converter.dart';
import 'package:arzaan_sara/constants/strings.dart';
import 'package:arzaan_sara/global/alerts/alerts.dart';
import 'package:arzaan_sara/global/common_views/button_text.dart';
import 'package:arzaan_sara/global/common_views/subtitle_text.dart';
import 'package:arzaan_sara/global/common_views/title_text.dart';
import 'package:arzaan_sara/global/converter/date_time_converter.dart';
import 'package:arzaan_sara/global/modules/image_selector/image_selector.dart';
import 'package:arzaan_sara/global/modules/car_plate_form/car_plate_form.dart';
import 'package:arzaan_sara/global/views/easy_button.dart';
import 'package:arzaan_sara/global/views/icon_button_streamer.dart';
import 'package:arzaan_sara/models/const_model/cargo_type_model.dart';
import 'package:arzaan_sara/models/const_model/vehicle_model.dart';
import 'package:arzaan_sara/models/request_model.dart';
import 'package:arzaan_sara/models/user_model.dart';
import 'package:arzaan_sara/resources/save.dart';
import 'package:arzaan_sara/screens/map/map_cargo_route.dart';
import 'package:arzaan_sara/screens/phone/phone_register.dart';
import 'package:arzaan_sara/screens/profile/profile_form.dart';
import 'package:url_launcher/url_launcher.dart';

class ProfileShow extends StatefulWidget {
  final DriverVerifyType verifyType;

  ProfileShow({this.verifyType});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return ProfileShowState();
  }
}

class ProfileShowState extends State<ProfileShow> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return null;
  }/*

  FormCarPlateController _foreignCarPlateController = FormCarPlateController();

  String vehicleName = "";
  String userImage;

  File image;

  bool showForeignCarPlate = false;



  @override
  void initState() {
    super.initState();

    getUserData();
  }

  Future<UserModel> getUserData() async {
    UserModel um = await Save().getUserData();
    vehicleName = await VehicleModel.returnVehicleName(um.vehicle_id);

    userImage = Strings.baseFileUrl + um.photo_url;
    return um;
  }

  @override
  Widget build(BuildContext context) {
    double imageSize = MediaQuery
        .of(context)
        .size
        .height / 4;


    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        appBar: appBarWidget(),
        body: Container(
          alignment: Alignment.center,
          margin: EdgeInsets.only(left: 24, right: 24, bottom: 16, top: 16),
          child: widget.verifyType == DriverVerifyType.notRegister
              ? Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              TitleText("اطلاعات حساب کاربری شما در حال بررسی می باشد"),
              SizedBox(height: 16,),
              SubtitleText(
                  "جهت پیگیری بیشتر می توانید با پشتیبانی تماس بگیرید"),
              SizedBox(height: 16,),
              EasyButton(
                isFilled: true,
                textColor: Colors.white,
                text: "ثبت نام | ورود",
                onPressed: () async {
                  bool isPhoneInserted = await Save().isPhoneInserted();
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => PhoneRegister()));
                },
              )
            ],
          )
              : FutureBuilder(
            future: getUserData(),
            builder: (context, AsyncSnapshot<UserModel> snapshot) {
              if (snapshot.hasData) {
                return ListView(
                  children: <Widget>[

                    TitleText("راننده گرامی"),
                    SizedBox(height: 8,),
                    SubtitleText(
                        "شما تنها قادر به ویرایش برخی از اطلاعات خود می باشید جهت ویرایش سایر اطلاعات باید با پشتیبانی تماس حاصل فرمایید"),
                    SizedBox(height: 16,),

                    carImageSelector(imageSize),
                    SizedBox(height: 16,),
                    mainContainer(
                      child: Column(
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Container(
                                height: 8,
                                width: 8,
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: Colors.black38),
                              ),
                              SizedBox(
                                width: 4,
                              ),
                              Text("نام: ",
                                  style: Theme
                                      .of(context)
                                      .textTheme
                                      .title),
                              Text(snapshot.data.first_name,
                                  style: Theme
                                      .of(context)
                                      .textTheme
                                      .subtitle),
                            ],
                          ),
                          SizedBox(
                            height: 8,
                          ),
                          Row(
                            children: <Widget>[
                              Container(
                                height: 8,
                                width: 8,
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: Colors.black38),
                              ),
                              SizedBox(
                                width: 4,
                              ),
                              Text("نام خانوادگی: ",
                                  style: Theme
                                      .of(context)
                                      .textTheme
                                      .title),
                              Text(snapshot.data.last_name,
                                  style: Theme
                                      .of(context)
                                      .textTheme
                                      .subtitle),
                            ],
                          ),
                          SizedBox(
                            height: 8,
                          ),
                          Row(
                            children: <Widget>[
                              Container(
                                height: 8,
                                width: 8,
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: Colors.black38),
                              ),
                              SizedBox(
                                width: 4,
                              ),
                              Text("شماره ملی: ",
                                  style: Theme
                                      .of(context)
                                      .textTheme
                                      .title),
                              Text(snapshot.data.national_code,
                                  style: Theme
                                      .of(context)
                                      .textTheme
                                      .subtitle),
                            ],
                          ),
                          SizedBox(
                            height: 8,
                          ),
                          Row(
                            children: <Widget>[
                              Container(
                                height: 8,
                                width: 8,
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: Colors.black38),
                              ),
                              SizedBox(
                                width: 4,
                              ),
                              Text("شماره موبایل: ",
                                  style: Theme
                                      .of(context)
                                      .textTheme
                                      .title),
                              Text(snapshot.data.phone_number,
                                  style: Theme
                                      .of(context)
                                      .textTheme
                                      .subtitle),
                            ],
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 16,),
                    mainContainer(
                      child: Column(
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Container(
                                height: 8,
                                width: 8,
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: Colors.black38),
                              ),
                              SizedBox(
                                width: 4,
                              ),
                              Text("نوع خودرو: ",
                                  style: Theme
                                      .of(context)
                                      .textTheme
                                      .title),
                              Text(vehicleName,
                                  style: Theme
                                      .of(context)
                                      .textTheme
                                      .subtitle),
                            ],
                          ),
                          SizedBox(
                            height: 8,
                          ),
                          Row(
                            children: <Widget>[
                              Container(
                                height: 8,
                                width: 8,
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: Colors.black38),
                              ),
                              SizedBox(
                                width: 4,
                              ),
                              Text("شماره پلاک: ",
                                  style: Theme
                                      .of(context)
                                      .textTheme
                                      .title),
                              Text(plateMaker(
                                snapshot.data.plate1,
                                snapshot.data.plate2,
                                snapshot.data.plate3,
                                snapshot.data.plate4,
                              ),
                                  style: Theme
                                      .of(context)
                                      .textTheme
                                      .subtitle),
                            ],
                          ),

                        ],
                      ),
                    ),
                    mainContainer(child:
                    foreignTravel(snapshot.data)),
                    InkWell(onTap: (){
                      launch("tel:"+Strings.supportPhone);
                    },child: ButtonText("تماس با پشتیبانی"),),
                  ],
                );
              }
              return Container();
            },
          ),
        ),
      ),
    );
  }

  Widget appBarWidget() {
    return AppBar(
      actions: <Widget>[
        Center(
          child: IconButtonStreamer<String>(
            icon: Icons.done,
            iconColor: Colors.white,
            stream: logInBlocEditProfile.actions,
            onPressed: () {
              if (image == null) {
                logInBlocEditProfile.fetchEditProfile(
                    directResult: (res) {
                      String vehicleId =
                          json.decode(res)['vehicle_id'].toString();
                      Save().setCarType(vehicleId);
                      Save().setUserData(res);
                      Save().setFormFilled(true);
                    },
                    apiAddress: "edit-profile",
                    body: {
                      "international_plate1": _foreignCarPlateController.plate1,
                      "international_plate2": _foreignCarPlateController.plate2,
                      "international_plate3": _foreignCarPlateController.plate3,
                      "international_plate4": _foreignCarPlateController.plate4,
                    });
              } else {
                logInBlocUpdateProfilePhoto.fetchUpdateProfilePhoto(
                    apiAddress: "update-profile-photo",
                    body: {"photo": image}).then((res) {
                  logInBlocEditProfile.fetchEditProfile(
                      directResult: (result) {
                        String vehicleId =
                            json.decode(result)['vehicle_id'].toString();
                        Save().setCarType(vehicleId);
                        Save().setUserData(result);

                      },
                      apiAddress: "edit-profile",
                      body: {
                        "international_plate1": _foreignCarPlateController.plate1,
                        "international_plate2": _foreignCarPlateController.plate2,
                        "international_plate3": _foreignCarPlateController.plate3,
                        "international_plate4": _foreignCarPlateController.plate4,
                      });
                });
              }
            },
            afterFinish: (AsyncSnapshot snapshot) async {
              Alerts.success(context, "عملیات با موفقیت انجام شد");
              logInBlocUpdateProfilePhoto.dispose();
              logInBlocEditProfile.dispose();
              Navigator.pop(context);
            },
          ),
        )
      ],
      iconTheme: IconThemeData(color: Colors.white),
      title: Center(
        child: AutoSizeText(
          'جزییات درخواست',
          style: TextStyle(
              color: Colors.white,
              fontSize: Theme.of(context).textTheme.title.fontSize),
          maxLines: 1,
        ),
      ),
    );
  }

  Widget mainContainer({Widget child}) {
    return Container(
      padding: EdgeInsets.only(bottom: 16, top: 16, right: 8, left: 8),
      margin: EdgeInsets.only(bottom: 16),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(8)),
          border: Border.all(color: Colors.black38, width: 1)),
      child: child,
    );
  }


  Widget carImageSelector(double imageSize) {
    return Align(
      alignment: Alignment.bottomCenter,
      child: ImageSelector(
          child: Stack(
            children: <Widget>[
              ClipOval(
                child: Container(
                    width: imageSize,
                    height: imageSize,
                    padding: EdgeInsets.all(4),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      shape: BoxShape.circle,
                    ),
                    child: Container(
                        padding: EdgeInsets.all(4),
                        decoration: BoxDecoration(
                          color: Color(0xffFFE7B1),
                          shape: BoxShape.circle,
                        ),
                        child: ClipOval(
                          child: image == null
                              ? userImage == null
                              ? Image.asset("assets/images/logo_splash.png")
                              : CachedNetworkImage(
                              imageUrl: userImage,
                              placeholder: (context, url) =>
                                  Center(
                                    child: CircularProgressIndicator(),
                                  ),
                              errorWidget: (context, url, error) =>
                                  Image.asset(
                                      "assets/profile_logo.png"))
                              : Image.file(image),
                        ))),
              ),
              Container(
                width: imageSize,
                height: imageSize,
                alignment: Alignment.bottomRight,
                child: Padding(
                  padding: EdgeInsets.only(
                      right: imageSize - (imageSize * 0.8509) - 16,
                      bottom: imageSize - (imageSize * 0.8509) - 16),
                  child: Container(
                    decoration: BoxDecoration(
                        shape: BoxShape.circle, color: Colors.white),
                    child: Container(
                      margin: EdgeInsets.all(3),
                      padding: EdgeInsets.all(4),
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: Theme
                              .of(context)
                              .primaryColor),
                      child: Icon(
                        Icons.add,
                        size: 24,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
          onFileAdded: (file) {
            setState(() {
              image = file;
            });
          }),
    );
  }

  Widget foreignTravel(UserModel data) {
    bool isForeignTravelInserted = data.international_plate1 != "null" ||
        data.international_plate2 != "null" ||
        data.international_plate3 != "null" ||
        data.international_plate4 != "null";


    if (isForeignTravelInserted) {
      return Row(
        children: <Widget>[
          Container(
            height: 8,
            width: 8,
            decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: Colors.black38),
          ),
          SizedBox(
            width: 4,
          ),
          Text("شماره پلاک خارج از کشور: ",
              style: Theme
                  .of(context)
                  .textTheme
                  .title),
          Text(plateMaker(data.international_plate1,
            data.international_plate2,
            data.international_plate3,
            data.international_plate4,
          ),
              style: Theme
                  .of(context)
                  .textTheme
                  .subtitle),
        ],
      );
    }else{

      return  Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          InkWell(
            onTap: (){
              setState(() {
                showForeignCarPlate = !showForeignCarPlate;
              });
            },
            child: Row(
              children: <Widget>[
                Checkbox(
                  value: showForeignCarPlate,
                  onChanged: (value){
                    setState(() {
                      showForeignCarPlate = !showForeignCarPlate;
                    });
                  },

                ),
                Text("مایل به سفر های خارج از کشور هستم")
              ],
            ),
          ),

          Visibility(
            visible: showForeignCarPlate,
            child:CarPlateForm(
              initialValue: false,
              carPlateController: _foreignCarPlateController,
              validator: (validate) {
                return null;

              },
            ),
          ),
        ],
      );
    }
  }



  String plateMaker(String plate1, String plate2, String palate3,
      String plate4) {
    return plate1 + "ایران" + plate2 + palate3 + plate4;
  }
*/

}
