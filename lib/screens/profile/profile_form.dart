import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:autocomplete_textfield/autocomplete_textfield.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:arzaan_sara/blocs/logIn_bloc.dart';
import 'package:arzaan_sara/constants/enums.dart';
import 'package:arzaan_sara/dialogs/dialog_car_type.dart';
import 'package:arzaan_sara/global/alerts/alerts.dart';
import 'package:arzaan_sara/global/common_views/button_text.dart';
import 'package:arzaan_sara/global/common_views/title_text.dart';
import 'package:arzaan_sara/global/modules/image_selector/image_selector.dart';
import 'package:arzaan_sara/global/modules/test_icon_button.dart';
import 'package:arzaan_sara/global/modules/car_plate_form/car_plate_form.dart';
import 'package:arzaan_sara/global/views/auto_complete_form.dart';
import 'package:arzaan_sara/global/views/check_box_form.dart';
import 'package:arzaan_sara/global/views/edit_text_form.dart';
import 'package:arzaan_sara/global/views/flat_button_form.dart';
import 'package:arzaan_sara/global/views/flat_button_icon.dart';
import 'package:arzaan_sara/global/views/icon_button_streamer.dart';
import 'package:arzaan_sara/models/const_model/city_model.dart';
import 'package:arzaan_sara/models/const_model/setting_model.dart';
import 'package:arzaan_sara/models/user_model.dart';
import 'package:arzaan_sara/resources/save.dart';
import 'package:arzaan_sara/screens/home.dart';
import 'package:arzaan_sara/screens/phone/phone_update.dart';
import 'package:arzaan_sara/tests/test.dart';
import 'package:url_launcher/url_launcher.dart';

class ProfileForm extends StatefulWidget {
  final ProfileFormType type;

  ProfileForm({this.type});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return ProfileFormState();
  }
}

class ProfileFormState extends State<ProfileForm> {
  final _formKey = GlobalKey<FormState>();
  GlobalKey<AutoCompleteTextFieldState<String>> keyCity = GlobalKey();

  final _ctrlName = TextEditingController();
  final _ctrlFamily = TextEditingController();
  final _ctrlNationalCode = TextEditingController();
  final _ctrlCity = TextEditingController();

  final _acCtrlCity = FormAutoCompleteController();

  FormFlatButtonController _flatButtonController = FormFlatButtonController();
  FormCheckBoxController _checkBoxController = FormCheckBoxController();
  final _carPlateController = FormCarPlateController();
  final _foreignCarPlateController = FormCarPlateController();

  List<String> _cityItems = List<String>();

  final _fName = FocusNode();
  final _fFamily = FocusNode();
  final _fNationalCode = FocusNode();
  final _fCity = FocusNode();

  ScrollController _scrollController = ScrollController();

  File image;

  String phone = "";
  String carId = "";
  String userImage;
  String cityId = "";

  bool showForeignCarPlate = false;

  @override
  void initState() {
    super.initState();

    setFirstData();

/*
    _ctrlName.text = "sajjad";
    _ctrlFamily.text = "arvin";
    _ctrlNationalCode.text = "3410308301";
*/

    if (widget.type == ProfileFormType.update) {
      setUserValuesInFields();
    }

    if (widget.type == ProfileFormType.update) {
      //setFirstData();
    } else if (widget.type == ProfileFormType.register) {
      WidgetsBinding.instance.addPostFrameCallback((res) {
        Future.delayed(Duration(milliseconds: 1000), () {
          _scrollController.animateTo(
              _scrollController.position.maxScrollExtent,
              curve: Curves.ease,
              duration: Duration(milliseconds: 1000));
          Future.delayed(Duration(milliseconds: 1000), () {
            _scrollController.animateTo(
                _scrollController.position.minScrollExtent,
                curve: Curves.ease,
                duration: Duration(milliseconds: 1000));
          });
        });
      });
    }
  }

  setUserValuesInFields() async {
    UserModel _um = await Save().getUserData();

    _ctrlName.text = _um.first_name;
    _ctrlFamily.text = _um.last_name;
  }

  @override
  Widget build(BuildContext context) {
    double imageSize = MediaQuery.of(context).size.height / 4;

    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        appBar: appBarWidget(),
        body: Container(
          margin: EdgeInsets.only(left: 24, right: 24, bottom: 16),
          child: Form(
              key: _formKey,
              child: ListView(
                controller: _scrollController,
                children: <Widget>[
                  SizedBox(
                    height: 24,
                  ),
                  headTitle(),
                  carImageSelector(imageSize),
                  SizedBox(
                    height: 24,
                  ),
                  EditTextForm(
                    focusNode: _fName,
                    nextFocusNode: _fFamily,
                    hint: "نام*",
                    textInput: TextInputType.text,
                    controller: _ctrlName,
                    border: true,
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  EditTextForm(
                    focusNode: _fFamily,
                    nextFocusNode: _fNationalCode,
                    hint: "نام خانوادگی*",
                    textInput: TextInputType.text,
                    controller: _ctrlFamily,
                    border: true,
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  FutureBuilder(
                    future: getCityData(),
                    builder: (context, snapshot) {
                      if (snapshot.hasData)
                        return AutoCompleteForm(
                          controller: _acCtrlCity,
                          focusNode: _fCity,
                          nextFocusNode: _fNationalCode,
                          initialValue: "",
                          lableText: "شهر محل سکونت*",
                          suggestions: snapshot.data,
                          textSubmitted: (value) async {
                            CityModel cm = await Save().getCityData();
                            for (int i = 0; i < cm.cities.length; i++) {
                              if (cm.cities[i].name == value) {
                                cityId = cm.cities[i].id;
                              }
                            }
                          },
                          textEditingController: _ctrlCity,
                          validator: (value) {
                            if (value.isEmpty && _ctrlCity.text.isEmpty) {
                              return "این قسمت باید پر شود";
                            }

                            bool isAccepted = false;
                            for (int i = 0; i < snapshot.data.length; i++) {
                              if (value == snapshot.data[i] ||
                                  _ctrlCity.text == snapshot.data[i])
                                isAccepted = true;
                            }

                            if (isAccepted) {
                              return null;
                            } else {
                              return "نام شهر بدرستی انتخاب نشده است";
                            }

                            return null;
                          },
                        );
                      return Center(
                        child: CircularProgressIndicator(),
                      );
                    },
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  EditTextForm(
                    focusNode: _fNationalCode,
                    nextFocusNode: _fNationalCode,
                    hint: "کد ملی*",
                    textInput: TextInputType.numberWithOptions(
                        signed: false, decimal: false),
                    controller: _ctrlNationalCode,
                    maxLength: 10,
                    border: true,
                  ),

                  SizedBox(
                    height: 8,
                  ),
                  FlatButtonForm(
                    icon: Icons.directions_car,
                    initialValue: "نوع خودرو شما*",
                    onPress: () async {
                      String s = await Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => DialogCarType(
                                onValueSelected: (carId, carName) {
                                  this.carId = carId;
                                },
                              )));
                      return s;
                    },
                    controller: _flatButtonController,
                    validator: (value) {
                      if (value ==
                          "نوع خودرو شما*") {
                        return "نوع وسیله نقلیه باید مشخص شود";
                      }

                      return null;
                    },
                  ),

                  SizedBox(
                    height: 8,
                  ),
                  CarPlateForm(
                    initialValue: false,
                    needValidate: true,
                    carPlateController: _carPlateController,
                    validator: (validate) {
                      if (validate)
                        return null;
                      else
                        return "شماره پلاک به درستی وارد نشده است";
                    },
                  ),
                  foreignTravel(),
                  SizedBox(
                    height: 16,
                  ),
                  InkWell(
                    onTap: () async {
                      SettingModel settingModel = await Save().getSettingData();
                      String cargoOwnerRegulation = settingModel.cargo_owner_regulation;
                      launch(cargoOwnerRegulation);
                    },
                    child: ButtonText("مطالعه قوانین و مقررات"),
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  widget.type == ProfileFormType.register
                      ? CheckBoxForm(
                    text: Text(
                      "ضمن مطالعه قوانین و مقررات اوبار آن را مطالعه و تایید می نمایم",
                      textAlign: TextAlign.right,
                      style: Theme.of(context).textTheme.subtitle,
                    ),
                    initialValue: false,
                    controller: _checkBoxController,
                    validator: (value) {
                      if (value == false) {
                        return "قوانین و مقررات باید پذیرفته شود";
                      }

                      return null;
                    },
                  )
                      : Container(),
                  SizedBox(
                    height: 24,
                  ),
                ],
              )),
        ),
      ),
    );
  }

  Widget appBarWidget() {
    return AppBar(
      iconTheme: IconThemeData(color: Colors.white),
      actions: <Widget>[checkState()],
      title: Center(
        child: AutoSizeText(
          widget.type == ProfileFormType.register
              ? "تکمیل فرم ثبت نام"
              : "ویرایش حساب کاربری",
          style: TextStyle(
              color: Colors.white,
              fontSize: Theme.of(context).textTheme.title.fontSize),
          maxLines: 1,
        ),
      ),
    );
  }

  Widget checkState() {
    //image = File('/data/user/0/ir.esperlos.arzaan_sara/cache/image_crop_5c06ab17-6e82-418d-a798-fb8fa54ec2183248477703345561455.jpg');
    if (widget.type == ProfileFormType.register) {
      return IconButtonStreamer<String>(
        icon: Icons.done,
        iconColor: Colors.white,
        stream: logInBlocEditProfile.actions,
        formKey: _formKey,
        onPressed: () {

          if(image == null){


            logInBlocEditProfile.fetchEditProfile(
                directResult: (res) {
                  String vehicleId = json.decode(res)['vehicle_id'].toString();
                  Save().setCarType(vehicleId);
                  Save().setUserData(res);
                  Save().setFormFilled(true);
                },
                apiAddress: "edit-profile",
                body: {
                  "first_name": _ctrlName.text,
                  "last_name": _ctrlFamily.text,
                  "national_code": _ctrlNationalCode.text,
                  "vehicle_id": carId,
                  "city_id": cityId,
                  "plate1": _carPlateController.plate1,
                  "plate2": _carPlateController.plate2,
                  "plate3": _carPlateController.plate3,
                  "plate4": _carPlateController.plate4,
                  "international_plate1": _foreignCarPlateController.plate1,
                  "international_plate2": _foreignCarPlateController.plate2,
                  "international_plate3": _foreignCarPlateController.plate3,
                  "international_plate4": _foreignCarPlateController.plate4,
                });


          }else{

            logInBlocUpdateProfilePhoto.fetchUpdateProfilePhoto(
                apiAddress: "update-profile-photo",
                body: {"photo": image}).then((res) {
              logInBlocEditProfile.fetchEditProfile(
                  directResult: (res) {
                    String vehicleId = json.decode(res)['vehicle_id'].toString();
                    Save().setCarType(vehicleId);
                    Save().setUserData(res);
                    Save().setFormFilled(true);
                  },
                  apiAddress: "edit-profile",
                  body: {
                    "first_name": _ctrlName.text,
                    "last_name": _ctrlFamily.text,
                    "national_code": _ctrlNationalCode.text,
                    "vehicle_id": carId,
                    "city_id": cityId,
                    "plate1": _carPlateController.plate1,
                    "plate2": _carPlateController.plate2,
                    "plate3": _carPlateController.plate3,
                    "plate4": _carPlateController.plate4,
                    "international_plate1": _foreignCarPlateController.plate1,
                    "international_plate2": _foreignCarPlateController.plate2,
                    "international_plate3": _foreignCarPlateController.plate3,
                    "international_plate4": _foreignCarPlateController.plate4,
                  });
            });
          }


        },



        afterFinish: (AsyncSnapshot snapshot) async {
          Alerts.success(context, "عملیات با موفقیت انجام شد");
          logInBlocUpdateProfilePhoto.dispose();
          logInBlocEditProfile.dispose();
          Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                  builder: (context) => Home(
                        driverVerifyType: DriverVerifyType.registered,
                      )));
        },
      );
    } else if (widget.type == ProfileFormType.update) {
      return IconButtonStreamer<String>(
        icon: Icons.done,
        iconColor: Colors.white,
        stream: logInBlocEditProfile.actions,
        formKey: _formKey,
        onPressed: () {
          logInBlocEditProfile.fetchEditProfile(
              directResult: (res) {
                Save().setUserData(res);
              },
              apiAddress: "edit-profile",
              body: {
                "first_name": _ctrlName.text,
                "last_name": _ctrlFamily.text,
                "national_code": _ctrlNationalCode.text,
                "vehicle_id": carId,
                "city_id": cityId,
                "plate1": _carPlateController.plate1,
                "plate2": _carPlateController.plate2,
                "plate3": _carPlateController.plate3,
                "plate4": _carPlateController.plate4,
                "international_plate1": _foreignCarPlateController.plate1,
                "international_plate2": _foreignCarPlateController.plate2,
                "international_plate3": _foreignCarPlateController.plate3,
                "international_plate4": _foreignCarPlateController.plate4,
              });
        },
        afterFinish: (AsyncSnapshot snapshot) async {
          Alerts.success(context, "عملیات با موفقیت انجام شد");
          logInBlocEditProfile.dispose();
          logInBlocUpdateProfilePhoto.dispose();
          Navigator.pop(context);
        },
      );
    }

    return Container();
  }

  Future<bool> onBackPress() async {
    /*   Alerts.confidenceCustom(context,
        title: "بازگشت",
        content: "آیا مطمئنید که می خواهید به صفحه قبل بازگردید؟",
        onOk: () async {
      Navigator.pop(context);

      if (widget.type == "update") {
        UserModel um = await Save().getUserData();
        Navigator.pop(context, Constants().siteUrlUpload + um.img_url);
      } else {
        Navigator.pop(context);
      }
    });*/
  }

  changePhoneNumber() {
    if (widget.type == ProfileFormType.register) {
      return Container();
    } else if (widget.type == ProfileFormType.update) {
      return Column(
        children: <Widget>[
          FlatButtonIcon(
            onTap: () async {
/*              phone = await  Navigator.push(context,
                  MaterialPageRoute(builder: (context) => PhoneUpdate()));*/
            },
            icon: Icons.mobile_screen_share,
            text: phone,
            borderColor: Theme.of(context).primaryColor,
            iconColor: Theme.of(context).primaryColor,
            textColor: Colors.black,
          ),
          SizedBox(
            height: 8,
          ),
        ],
      );
    }
  }


  Widget foreignTravel() {

    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        InkWell(
          onTap: (){
            setState(() {
              showForeignCarPlate = !showForeignCarPlate;
            });
          },
          child: Row(
            children: <Widget>[
              Checkbox(
                value: showForeignCarPlate,
                onChanged: (value){
                  setState(() {
                    showForeignCarPlate = !showForeignCarPlate;
                  });
                },

              ),
              Text("مایل به سفر های خارج از کشور هستم")
            ],
          ),
        ),

        Visibility(
          visible: showForeignCarPlate,
          child:CarPlateForm(
            initialValue: false,
            carPlateController: _foreignCarPlateController,
            validator: (validate) {
              return null;

            },
          ),
        ),
      ],
    );
  }


  String plateMaker(String plate1, String plate2, String palate3,
      String plate4) {
    return plate1 + "ایران" + plate2 + palate3 + plate4;
  }


  Widget headTitle() {
    if (widget.type == ProfileFormType.register) {
      return TitleText("لطفا تمام قسمت های ستاره دار را تکمیل نمایید");
    } else if (widget.type == ProfileFormType.update) {
      return TitleText("در اینجا می توانید اطلاعات خود را ویرایش کنید");
    }
  }

  Widget carImageSelector(double imageSize) {
    return Align(
      alignment: Alignment.bottomCenter,
      child: ImageSelector(
          child: Stack(
            children: <Widget>[
              ClipOval(
                child: Container(
                    width: imageSize,
                    height: imageSize,
                    padding: EdgeInsets.all(4),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      shape: BoxShape.circle,
                    ),
                    child: Container(
                        padding: EdgeInsets.all(4),
                        decoration: BoxDecoration(
                          color: Color(0xffFFE7B1),
                          shape: BoxShape.circle,
                        ),
                        child: ClipOval(
                          child: image == null
                              ? userImage == null
                                  ? Image.asset("assets/images/logo_splash.png")
                                  : CachedNetworkImage(
                                      imageUrl: userImage,
                                      placeholder: (context, url) => Center(
                                            child: CircularProgressIndicator(),
                                          ),
                                      errorWidget: (context, url, error) =>
                                          Image.asset(
                                              "assets/profile_logo.png"))
                              : Image.file(image),
                        ))),
              ),
              Container(
                width: imageSize,
                height: imageSize,
                alignment: Alignment.bottomRight,
                child: Padding(
                  padding: EdgeInsets.only(
                      right: imageSize - (imageSize * 0.8509) - 16,
                      bottom: imageSize - (imageSize * 0.8509) - 16),
                  child: Container(
                    decoration: BoxDecoration(
                        shape: BoxShape.circle, color: Colors.white),
                    child: Container(
                      margin: EdgeInsets.all(3),
                      padding: EdgeInsets.all(4),
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: Theme.of(context).primaryColor),
                      child: Icon(
                        Icons.add,
                        size: 24,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
          onFileAdded: (file) {
            setState(() {
              image = file;
            });
          }),
    );
  }


  setFirstData() async {
    CityModel cm = await Save().getCityData();

    for (int i = 0; i < cm.cities.length; i++) {
      _cityItems.add(cm.cities[i].name);
    }
  }


  Future<List<String>> getCityData() async {
    List<String> cities = [];
    CityModel cm = await Save().getCityData();

    for (int i = 0; i < cm.cities.length; i++) {
      cities.add(cm.cities[i].name);
    }

    return cities;
  }
}
