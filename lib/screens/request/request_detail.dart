import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:arzaan_sara/constants/enums.dart';
import 'package:arzaan_sara/constants/status_converter.dart';
import 'package:arzaan_sara/global/common_views/subtitle_text.dart';
import 'package:arzaan_sara/global/common_views/title_text.dart';
import 'package:arzaan_sara/global/converter/date_time_converter.dart';
import 'package:arzaan_sara/global/views/easy_button.dart';
import 'package:arzaan_sara/models/const_model/cargo_type_model.dart';
import 'package:arzaan_sara/models/const_model/city_model.dart';
import 'package:arzaan_sara/models/const_model/vehicle_model.dart';
import 'package:arzaan_sara/models/request_model.dart';
import 'package:arzaan_sara/screens/map/map_cargo_route.dart';

import '../../global/converter/nullter.dart';

class RequestDetail extends StatefulWidget {
  final Reqeusts request;
  final DriverVerifyType verifyType;
  final RequestSteps steps;
  RequestDetail({this.steps, this.request, this.verifyType});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return RequestDetailState();
  }
}

class RequestDetailState extends State<RequestDetail> {
  String cargoTypeName = "";
  String vehicleName = "";

  @override
  void initState() {
    super.initState();

    getData();
  }

  getData() async {
    CargoTypeDetail cargoTypeDetail =
        await CargoTypeModel.returnData(widget.request.cargo_type_id);

    VehicleModelDetail vehicleTypeDetail =
        await VehicleModel.returnData(widget.request.cargo_type_id);

    setState(() {
      if (cargoTypeDetail != null) cargoTypeName = cargoTypeDetail.name;
      if (vehicleTypeDetail != null) vehicleName = vehicleTypeDetail.name;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        appBar: appBarWidget(),
        body: Container(
          margin: EdgeInsets.only(left: 24, right: 24, bottom: 16, top: 16),
          child: ListView(
            children: <Widget>[
              mainContainer(
                  child: address()),
              mainContainer(
                child: Column(
                  children: <Widget>[
                    //carry time
                    Row(
                      children: <Widget>[
                        Icon(
                          Icons.access_time,
                          size: 18,
                          color: Colors.black38,
                        ),
                        SizedBox(
                          width: 4,
                        ),
                        Text("زمان بارگیری: ",
                            style: Theme.of(context).textTheme.title),
                        Text(
                            DateTimeConverter.getToJalBetweenTimeDate(
                                widget.request.loading_start_time,
                                widget.request.loading_end_time),
                            style: Theme.of(context).textTheme.subtitle),
                      ],
                    ),
                    //
                    SizedBox(
                      height: 8,
                    ),
                    //load time
                    Row(
                      children: <Widget>[
                        Icon(
                          Icons.access_time,
                          size: 18,
                          color: Colors.black38,
                        ),
                        SizedBox(
                          width: 4,
                        ),
                        Text("زمان تخیله بار: ",
                            style: Theme.of(context).textTheme.title),
                        Text(
                            DateTimeConverter.getToJalBetweenTimeDate(
                                widget.request.evacuation_start_time,
                                widget.request.evacuation_end_time),
                            style: Theme.of(context).textTheme.subtitle),
                      ],
                    ),
                    //
                  ],
                ),
              ),
              mainContainer(
                child: Row(
                  children: <Widget>[
                    Icon(
                      Icons.credit_card,
                      size: 18,
                      color: Colors.black38,
                    ),
                    SizedBox(
                      width: 4,
                    ),
                    Text("قیمت: ", style: Theme.of(context).textTheme.title),
                    Text(price(), style: Theme.of(context).textTheme.subtitle),
                  ],
                ),
              ),
              mainContainer(
                child: Row(
                  children: <Widget>[
                    Icon(
                      Icons.directions_car,
                      size: 18,
                      color: Colors.black38,
                    ),
                    SizedBox(
                      width: 4,
                    ),
                    Text("نوع خودرو: ",
                        style: Theme.of(context).textTheme.title),
                    Text(vehicleName,
                        style: Theme.of(context).textTheme.subtitle),
                  ],
                ),
              ),
              mainContainer(
                child: Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Container(
                          height: 8,
                          width: 8,
                          decoration: BoxDecoration(
                              shape: BoxShape.circle, color: Colors.black38),
                        ),
                        SizedBox(
                          width: 4,
                        ),
                        Text("نوع بار: ",
                            style: Theme.of(context).textTheme.title),
                        Text(cargoTypeName,
                            style: Theme.of(context).textTheme.subtitle),
                      ],
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    Row(
                      children: <Widget>[
                        Container(
                          height: 8,
                          width: 8,
                          decoration: BoxDecoration(
                              shape: BoxShape.circle, color: Colors.black38),
                        ),
                        SizedBox(
                          width: 4,
                        ),
                        Text("وزن بار: ",
                            style: Theme.of(context).textTheme.title),
                        Text(
                            "بین " +
                                widget.request.cargo_weight_min +
                                " و " +
                                widget.request.cargo_weight_max +
                                " کیلوگرم",
                            style: Theme.of(context).textTheme.subtitle),
                      ],
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    Row(
                      children: <Widget>[
                        Container(
                          height: 8,
                          width: 8,
                          decoration: BoxDecoration(
                              shape: BoxShape.circle, color: Colors.black38),
                        ),
                        SizedBox(
                          width: 4,
                        ),
                        Text("ارزش محموله: ",
                            style: Theme.of(context).textTheme.title),
                        Text(
                            "بین " +
                                widget.request.cargo_cost_min +
                                " و " +
                                widget.request.cargo_cost_max +
                                " تومان",
                            style: Theme.of(context).textTheme.subtitle),
                      ],
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    Row(
                      children: <Widget>[
                        Container(
                          height: 8,
                          width: 8,
                          decoration: BoxDecoration(
                              shape: BoxShape.circle, color: Colors.black38),
                        ),
                        SizedBox(
                          width: 4,
                        ),
                        Text("توضیحات بار: ",
                            style: Theme.of(context).textTheme.title),
                      ],
                    ),
                    Align(
                      alignment: Alignment.centerRight,
                      child: Padding(
                        padding: EdgeInsets.only(right: 12),
                        child: Text(Nullter.toEmpty(widget.request.description),
                            style: Theme.of(context).textTheme.subtitle),
                      ),
                    )
                  ],
                ),
              ),
              mainContainer(
                  child: Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Container(
                        height: 8,
                        width: 8,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle, color: Colors.black38),
                      ),
                      SizedBox(
                        width: 4,
                      ),
                      Text("پیام صاحب بار به شما:",
                          style: Theme.of(context).textTheme.title),
                    ],
                  ),
                  Align(
                    alignment: Alignment.centerRight,
                    child: Padding(
                      padding: EdgeInsets.only(right: 12),
                      child: Text("ارزش بار مشخص شده",
                          style: Theme.of(context).textTheme.subtitle),
                    ),
                  )
                ],
              ))
            ],
          ),
        ),
      ),
    );
  }

  Widget appBarWidget() {
    return AppBar(
      iconTheme: IconThemeData(color: Colors.white),
      actions: <Widget>[
        Opacity(
          opacity: 0,
          child: IconButton(
            onPressed: null,
            icon: Icon(Icons.add),
          ),
        )
      ],
      title: Center(
        child: AutoSizeText(
          "جزییات درخواست",
          style: TextStyle(
              color: Colors.white,
              fontSize: Theme.of(context).textTheme.title.fontSize),
          maxLines: 1,
        ),
      ),
    );
  }

  Widget mainContainer({Widget child}) {
    return Container(
      padding: EdgeInsets.only(bottom: 16, top: 16, right: 8, left: 8),
      margin: EdgeInsets.only(bottom: 16),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(8)),
          border: Border.all(color: Colors.black38, width: 1)),
      child: child,
    );
  }

  Widget showLocationView() {
    bool shouldButtonShow = widget.verifyType == DriverVerifyType.confirmed &&
        widget.steps != RequestSteps.add;

    if (shouldButtonShow)
      return Column(
        children: <Widget>[EasyButton(
          text: "مشاهده آدرس روی نقشه",
          isFilled: true,
          onPressed: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => MapCargoRoute(
                      originLatLng: LatLng(
                          double.parse(widget.request.origin_latitude),
                          double.parse(widget.request.origin_longitude)),
                      destinationLatLng: LatLng(
                          double.parse(widget.request.destination_latitude),
                          double.parse(widget.request.destination_longitude)),
                    )));
          },
        )],
      );

    return Container();
  }

  Widget address() {
    bool showAddress = widget.verifyType == DriverVerifyType.confirmed &&
        widget.steps != RequestSteps.add;
    if (showAddress) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          //origin address
          Row(
            children: <Widget>[
              Icon(
                Icons.location_on,
                size: 18,
                color: Colors.black38,
              ),
              TitleText(
                "آدرس مبدا:",
              )
            ],
          ),
          SizedBox(
            height: 4,
          ),
          Padding(
            padding: EdgeInsets.only(right: 22),
            child: SubtitleText(
              widget.request.origin_address,
              textAlign: TextAlign.right,
            ),
          ),
          SizedBox(
            height: 8,
          ),
          //destination address
          Row(
            children: <Widget>[
              Icon(
                Icons.location_on,
                size: 18,
                color: Colors.black38,
              ),
              TitleText(
                "آدرس مقصد:",
              )
            ],
          ),
          SizedBox(
            height: 4,
          ),
          Padding(
            padding: EdgeInsets.only(right: 22),
            child: SubtitleText(
              widget.request.destination_address,
              textAlign: TextAlign.right,
            ),
          ),
        ],
      );
    } else {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          //origin address
          Row(
            children: <Widget>[
              Icon(
                Icons.location_on,
                size: 18,
                color: Colors.black38,
              ),
              TitleText(
                "شهر مبدا:",
              )
            ],
          ),
          SizedBox(
            height: 4,
          ),
          FutureBuilder(
            future:
            getCityName(widget.request.origin_city_id),
            builder: (context, snapshot) {
              if (snapshot.hasData)
                return Padding(
                  padding: EdgeInsets.only(right: 22),
                  child: SubtitleText(
                    snapshot.data,
                    textAlign: TextAlign.right,
                  ),
                );

              return Container();
            },
          ),
          SizedBox(
            height: 8,
          ),
          //destination address
          Row(
            children: <Widget>[
              Icon(
                Icons.location_on,
                size: 18,
                color: Colors.black38,
              ),
              TitleText(
                "شهر مقصد:",
              )
            ],
          ),
          SizedBox(
            height: 4,
          ),
          FutureBuilder(
            future: getCityName(
              widget.request.destination_city_id,
            ),
            builder: (context, snapshot) {
              if (snapshot.hasData)
                return Padding(
                  padding: EdgeInsets.only(right: 22),
                  child: SubtitleText(
                    snapshot.data,
                    textAlign: TextAlign.right,
                  ),
                );

              return Container();
            },
          ),
        ],
      );
    }
  }
  price() {
    if (widget.request.price == "null" ||
        widget.request.price == "0") return "توافقی";

    return widget.request.price + " تومان";
  }

  getCityName(String id) async {
    String cityName = await CityModel.returnData(id);

    return cityName;
  }
}
