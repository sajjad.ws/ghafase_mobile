import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:latlong/latlong.dart' as latlng;
import 'package:liquid_pull_to_refresh/liquid_pull_to_refresh.dart';
import 'package:location/location.dart';
import 'package:arzaan_sara/blocs/logIn_bloc.dart';
import 'package:arzaan_sara/blocs/request_bloc.dart';
import 'package:arzaan_sara/constants/dummy.dart';
import 'package:arzaan_sara/constants/enums.dart';
import 'package:arzaan_sara/dialogs/dialog_filter.dart';
import 'package:arzaan_sara/global/common_views/list_empty_view.dart';
import 'package:arzaan_sara/global/common_views/list_net_error.dart';
import 'package:arzaan_sara/global/common_views/subtitle_text.dart';
import 'package:arzaan_sara/global/common_views/title_text.dart';
import 'package:arzaan_sara/global/converter/date_time_converter.dart';
import 'package:arzaan_sara/global/views/easy_button.dart';
import 'package:arzaan_sara/global/views/easy_flat_button.dart';
import 'package:arzaan_sara/global/views/flat_button_icon.dart';
import 'package:arzaan_sara/models/const_model/filter_model.dart';
import 'package:arzaan_sara/models/request_model.dart';
import 'package:arzaan_sara/resources/save.dart';
import 'package:arzaan_sara/screens/phone/phone_register.dart';
import 'package:arzaan_sara/screens/profile/profile_form.dart';
import 'package:arzaan_sara/widgets/fcm/fcm.dart';
import 'package:arzaan_sara/widgets/home/custom_appbar.dart';
import 'package:arzaan_sara/widgets/home/home_request_items.dart';
import 'dart:math';

import '../widgets/drawer.dart';
import 'map/map_cargo_route.dart';

class Home extends StatefulWidget {
  DriverVerifyType driverVerifyType;

  static FilterModel carType = FilterModel("", "");
  static FilterModel originCity = FilterModel("", "");
  static FilterModel destinationCity = FilterModel("", "");
  static FilterModel carryDate = FilterModel("", "");
  static FilterModel evacuationDate = FilterModel("", "");
  static FilterModel cargoType = FilterModel("", "");
  static FilterModel cargoMinWeight = FilterModel("", "");
  static FilterModel cargoMaxWeight = FilterModel("", "");

  Home({this.driverVerifyType = DriverVerifyType.notRegister});

  @override
  State<StatefulWidget> createState() => _HomeState();
}

class _HomeState extends State<Home> with SingleTickerProviderStateMixin {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  final firebaseMessaging = FirebaseMessaging();

  TabController _tabController;

  List<String> filtersList = [];
  Map<String, String> filtersMap = {};

  bool isSecondTabLoaded = false;
  bool shouldShowFilterWidget = false;

  var location = new Location();

/*  double preLat = 39.423;
  double preLng = -123.0829983;*/
  double preLat = 0;
  double preLng = 0;

  @override
  void initState() {
    super.initState();

    generateFirebaseToken();

    sendSearchRequest();

    sendUserLatLng();

    _tabController = new TabController(vsync: this, length: 2);
    _tabController.addListener(_handleTabSelection);
  }


  void generateFirebaseToken() {


    firebaseMessaging.configure(
      onLaunch: (Map<String, dynamic> msg) {
        print('onCall');
        print(msg.toString());
      },
      onResume: (Map<String, dynamic> msg) {
        print('onCall2');
        print(msg.toString());
      },
      onMessage: (Map<String, dynamic> msg) {
        FCM.manage(msg,context);
        requestBlocActiveRequests.fetchActiveRequest(
            apiAddress: "get-driver-active-requests", body: {});

        sendSearchRequest();
      },
    );



    firebaseMessaging.getToken().then((token) {
      print(token);
    });


  }



  void _handleTabSelection() {
    setState(() {});

    if (_tabController.index == 1) {
      if (!isSecondTabLoaded) {
        requestBlocActiveRequests.fetchActiveRequest(
            apiAddress: "get-driver-active-requests", body: {});
        isSecondTabLoaded = true;
      }
    } else {}
  }

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: DefaultTabController(
          length: 3,
          child: Scaffold(
            key: _scaffoldKey,
            drawer: buildDrawerLayout(context, widget.driverVerifyType),
            appBar: PreferredSize(
              preferredSize: Size.fromHeight(kToolbarHeight),
              child: CustomAppbar(
                onMenuPress: () {
                  _scaffoldKey.currentState.openDrawer();
                },
              ),
            ),
            body: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(color: Colors.white, boxShadow: [
                    BoxShadow(
                        color: Colors.black12,
                        offset: Offset(0.0, 1.0),
                        spreadRadius: 2)
                  ]),
                  child: TabBar(
                    controller: _tabController,
                    indicatorColor: Theme.of(context).accentColor,
                    tabs: <Widget>[
                      Tab(
                        child: Text("همه درخواست ها",
                            style: TextStyle(
                                color: tabTextColor(0), fontSize: 13)),
                      ),
                      Tab(
                        child: Text("درخواست های فعال",
                            style: TextStyle(
                                color: tabTextColor(1), fontSize: 13)),
                      ),
                    ],
                  ),
                ),
                tabContent(),
              ],
            ),
          )),
    );
  }

  Color tabTextColor(int i) {
    if (_tabController.index == i)
      return Theme.of(context).accentColor;
    else
      return Colors.black26;
  }

  tabContent() {
    return Expanded(
      child: TabBarView(
        controller: _tabController,
        children: [
          Column(
            children: <Widget>[
              SizedBox(
                height: 8,
              ),
              Padding(
                padding: EdgeInsets.only(left: 8, right: 8),
                child: FlatButtonIcon(
                  fillColor: Colors.grey,
                  borderColor: Colors.grey,
                  icon: Icons.filter_list,
                  iconColor: Colors.white,
                  text: "فیلتر گذاری",
                  textColor: Colors.white,
                  useIcon: true,
                  onTap: onFilterClick,
                ),
              ),
              shouldShowFilterWidget
                  ? Container(
                color: Colors.white,
                height: 50,
                child: ListView.builder(
                    itemCount: filtersList.length,
                    scrollDirection: Axis.horizontal,
                    itemBuilder: (context, index) {
                      return Container(
                        margin: EdgeInsets.all(8),
                        padding: EdgeInsets.fromLTRB(8, 4, 8, 4),
                        decoration: BoxDecoration(
                            border: Border.all(
                              color: Colors.black38,
                              width: 1,
                            ),
                            borderRadius: BorderRadius.all(
                                Radius.elliptical(50, 50))),
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            InkWell(
                              onTap: () => deleteFilter(index),
                              child: Icon(
                                Icons.clear,
                                color: Colors.black38,
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(right: 4,top: 4),
                              child: SubtitleText(
                                filtersList[index],
                              ),
                            ),

                          ],
                        ),
                      );
                    }),
              )
                  : Container(),
              StreamBuilder(
                stream: requestBlocPendingRequests.actions,
                builder: (context, AsyncSnapshot<RequestModel> snapshot) {
                  if(snapshot.hasError){
                    return ListNetError(
                      onRefresh: (){

                        sendSearchRequest();

                      },
                    );
                  }
                  if (snapshot.hasData) {
                    if(snapshot.data.requests.isEmpty){
                      return  Expanded(
                        child: ListEmptyView(
                          onRefresh: (){
                            sendSearchRequest();
                          },
                        ),
                      );
                    }


                    return Expanded(
                      child: allRequestList(snapshot.data),
                    );
                  }

                  return Expanded(child: Center(
                    child: CircularProgressIndicator(),
                  ),);
                },
              )
            ],
          ),
          activeTabView()
        ],
      ),
    );
  }

  Widget allRequestList(RequestModel data) {
    return LiquidPullToRefresh(
      onRefresh: () {
        sendSearchRequest();

        requestBlocActiveRequests.fetchActiveRequest(
            apiAddress: "get-driver-active-requests", body: {});
        return;
      },
      child: listViewBuilder(data), // scroll view
    );
  }

  Widget activeList(RequestModel data) {
    return LiquidPullToRefresh(
      onRefresh: () {
        sendSearchRequest();

        requestBlocActiveRequests.fetchActiveRequest(
            apiAddress: "get-driver-active-requests", body: {});

        return;
      },
      child: listViewBuilder(
        data,
      ), // scroll view
    );
  }

  Widget listViewBuilder(
    RequestModel data,
  ) {
    return ListView.builder(
        padding: EdgeInsets.only(top: 16),
        itemCount: data.requests.length,
        itemBuilder: (context, index) {
          return HomeRequestItems(
            model: data,
            index: index,
            steps: RequestModel().returnStep(data.requests[index].last_status),
            verifyType: widget.driverVerifyType,
          );
        });
  }

  activeTabView() {
    if (widget.driverVerifyType == DriverVerifyType.confirmed) {
      return StreamBuilder(
        stream: requestBlocActiveRequests.actions,
        builder: (context, AsyncSnapshot<RequestModel> snapshot) {
          if(snapshot.hasError){
            return ListNetError(
              onRefresh: (){

                requestBlocActiveRequests.fetchActiveRequest(
                    apiAddress: "get-driver-active-requests", body: {});

              },
            );
          }
          if (snapshot.hasData) {
            if(snapshot.data.requests.isEmpty){
              return  ListEmptyView(
                onRefresh: (){
                  requestBlocActiveRequests.fetchActiveRequest(
                      apiAddress: "get-driver-active-requests", body: {});
                },
              );
            }


            return activeList(snapshot.data);
          }

          return Center(
            child: CircularProgressIndicator(),
          );
        },
      );
    } else if (widget.driverVerifyType == DriverVerifyType.registered) {
      return Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          TitleText("اطلاعات حساب کاربری شما در حال بررسی می باشد"),
          SizedBox(
            height: 16,
          ),
          SubtitleText("جهت پیگیری بیشتر می توانید با پشتیبانی تماس بگیرید"),
          SizedBox(
            height: 16,
          ),
          EasyButton(
            isFilled: true,
            textColor: Colors.white,
            text: "ثبت نام | ورود",
            onPressed: () async {
              bool isPhoneInserted = await Save().isPhoneInserted();
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => PhoneRegister()));
            },
          )
        ],
      );
    } else if (widget.driverVerifyType == DriverVerifyType.notRegister) {
      return Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          TitleText("شما قادر به مشاهده لیست درخواست ها نیستید"),
          SizedBox(
            height: 16,
          ),
          SubtitleText(
              "لطفا وارد حساب کاربری خود شوید \nو یا در صورت عدم ثبت نام، ثبت نام نمایید"),
          SizedBox(
            height: 16,
          ),
          EasyButton(
            isFilled: true,
            textColor: Colors.white,
            text: "ثبت نام | ورود",
            onPressed: () async {
              bool isPhoneInserted = await Save().isPhoneInserted();
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => PhoneRegister()));
            },
          )
        ],
      );
    }
  }

  onFilterClick() {
    showDialog(
        context: context,
        builder: (context) => DialogFilter(
              afterOk: (map) {
                if (map.isNotEmpty) {
                  filtersMap.clear();
                  filtersList.clear();

                  filtersMap = map;

                  map.forEach((k, v) {
                    if (v.isNotEmpty) {
                      filtersList.add(v);
                      setState(() {
                        shouldShowFilterWidget = true;
                      });
                    }
                  });

                  sendSearchRequest();
                } else {
                  setState(() {
                    shouldShowFilterWidget = false;
                  });
                }
              },
            ));
  }

  sendSearchRequest() async {
    String vehicleId = "";
    String carType = await Save().getCarType();

    if ((carType != null && Home.carType.id.isEmpty)) {
      vehicleId = carType;
    } else if ((carType != null && Home.carType.id.isNotEmpty)) {
      vehicleId = Home.carType.id;
    } else if ((carType == null && Home.carType.id.isNotEmpty)) {
      vehicleId = Home.carType.id;
    }

    print({
      "vehicle_id": vehicleId,
      "origin_city_id": Home.originCity.id,
      "destination_city_id": Home.destinationCity.id,
      "loading_start_time": DateTimeConverter.getGreDate(Home.carryDate.title),
      "evacuation_start_time":
      DateTimeConverter.getGreDate(Home.evacuationDate.title),
      "cargo_weight_min": Home.cargoMinWeight.title,
      "cargo_weight_max": Home.cargoMaxWeight.title,
    });

    requestBlocPendingRequests
        .fetchPendingRequest(apiAddress: "get-driver-hover-requests", body: {
      "vehicle_id": vehicleId,
      "origin_city_id": Home.originCity.id,
      "destination_city_id": Home.destinationCity.id,
      "loading_start_time": DateTimeConverter.getGreDate(Home.carryDate.title),
      "evacuation_start_time":
          DateTimeConverter.getGreDate(Home.evacuationDate.title),
      "cargo_weight_min": Home.cargoMinWeight.title,
      "cargo_weight_max": Home.cargoMaxWeight.title,
    });
  }

  void deleteFilter(int index) {
    setState(() {
      shouldShowFilterWidget = true;
    });
    print(filtersMap['carTypeStr']);
    if (filtersMap['carTypeStr'] == filtersList[index]) {
      Home.carType.id = "";
      Home.carType.title = "";
      Home.carType.hint = "";
    } else if (filtersMap['originCityStr'] == filtersList[index]) {
      Home.originCity.id = "";
      Home.originCity.title = "";
      Home.originCity.hint = "";
    } else if (filtersMap['destinationCityStr'] == filtersList[index]) {
      Home.destinationCity.id = "";
      Home.destinationCity.title = "";
      Home.destinationCity.hint = "";
    } else if (filtersMap['carryDateStr'] == filtersList[index]) {
      Home.carryDate.id = "";
      Home.carryDate.title = "";
      Home.carryDate.hint = "";
    } else if (filtersMap['cargoTypeStr'] == filtersList[index]) {
      Home.cargoType.id = "";
      Home.cargoType.title = "";
      Home.cargoType.hint = "";
    } else if (filtersMap['cargoWeightStr'] == filtersList[index]) {
      Home.cargoMinWeight.title = "";
      Home.cargoMaxWeight.title = "";
      Home.cargoMaxWeight.hint = "";
      Home.cargoMinWeight.hint = "";
    }
    setState(() {
      filtersList.removeAt(index);
    });

    if (filtersList.isEmpty) {
      setState(() {
        shouldShowFilterWidget = false;
      });
    }
    sendSearchRequest();
  }

  void sendUserLatLng() {

    location.onLocationChanged().listen((LocationData currentLocation) {
      latlng.Distance d = new latlng.Distance();

      latlng.LatLng p1 = new latlng.LatLng(
          currentLocation.latitude, currentLocation.longitude);
      latlng.LatLng p2 = new latlng.LatLng(preLat, preLng);

      double meter = d.distance(p1, p2);

      if (meter > 20) {
        preLat = currentLocation.latitude;
        preLng = currentLocation.longitude;

        logInBlocUpdateProfilePhoto
            .fetchEditProfile(apiAddress: "edit-profile", body: {
          "latitude": currentLocation.latitude.toString(),
          "longitude": currentLocation.longitude.toString()
        });
      }
    });


  }
}
