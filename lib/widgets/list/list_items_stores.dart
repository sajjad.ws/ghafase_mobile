import 'package:arzaan_sara/constants/strings.dart';
import 'package:arzaan_sara/global/common_views/subtitle_text.dart';
import 'package:arzaan_sara/global/common_views/title_text.dart';
import 'package:arzaan_sara/global/formatter/num_fornat.dart';
import 'package:arzaan_sara/models/const_model/category_model.dart';
import 'package:arzaan_sara/models/product_model.dart';
import 'package:arzaan_sara/models/store_model.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class ListItemsStore extends StatefulWidget {
  StoreModel model;
  int index;

  ListItemsStore({this.model, this.index});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return ListItemsStoreState();
  }
}

class ListItemsStoreState extends State<ListItemsStore> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      margin: EdgeInsets.only(right: 16),
      width: MediaQuery.of(context).size.width * 3 / 7,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(16)),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Stack(
            children: <Widget>[
              ClipRRect(
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(16),
                    topLeft: Radius.circular(16)),
                child: Padding(
                  padding: EdgeInsets.all(16),
                  child: Image.asset(
                    "assets/images/product.png",
                    width: MediaQuery.of(context).size.width / 3,
                    height: MediaQuery.of(context).size.width / 4,
                  ),
                ),
              ),
              Align(
                alignment: Alignment.topRight,
                child: InkWell(
                  onTap: () {},
                  child: Container(
                    margin: EdgeInsets.only(top: 8, right: 8),
                    padding: EdgeInsets.all(4),
                    decoration: BoxDecoration(
                        shape: BoxShape.circle, color: Strings.green),
                    child: Icon(
                      Icons.location_on,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            ],
          ),
          Padding(
            padding: EdgeInsets.only(right: 4),
            child: TitleText(
              widget.model.stores[widget.index].name,
              textAlign: TextAlign.right,
            ),
          ),
          Padding(
            padding: EdgeInsets.only(right: 4),
            child: SubtitleText(
              "${widget.model.stores[widget.index].product_count} محصول موجود است",
              textAlign: TextAlign.right,
            ),
          ),
          Container(
            height: 1,
            color: Colors.black12,
          ),
          SizedBox(
            height: 4,
          ),
          Padding(
            padding: EdgeInsets.only(right: 4),
            child: Text(
              widget.model.stores[widget.index].address,
              textAlign: TextAlign.right,
            ),
          )
        ],
      ),
    );
  }


}