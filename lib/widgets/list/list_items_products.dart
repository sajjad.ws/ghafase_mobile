import 'package:arzaan_sara/constants/strings.dart';
import 'package:arzaan_sara/global/common_views/subtitle_text.dart';
import 'package:arzaan_sara/global/common_views/title_text.dart';
import 'package:arzaan_sara/global/formatter/num_fornat.dart';
import 'package:arzaan_sara/models/const_model/category_model.dart';
import 'package:arzaan_sara/models/product_model.dart';
import 'package:arzaan_sara/screens/product/product_detail.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class ListItemsProduct extends StatefulWidget {
  ProductModel model;
  int index;

  ListItemsProduct({this.model, this.index});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return ListItemsProductState();
  }
}

class ListItemsProductState extends State<ListItemsProduct> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return InkWell(
      child: Container(
        margin: EdgeInsets.only(right: 16),
        width: MediaQuery.of(context).size.width * 3 / 7,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(16)),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Stack(
              children: <Widget>[
                ClipRRect(
                  borderRadius: BorderRadius.only(
                      topRight: Radius.circular(16),
                      topLeft: Radius.circular(16)),
                  child: Padding(
                    padding: EdgeInsets.all(16),
                    child: Image.asset(
                      "assets/images/product.png",
                      width: MediaQuery.of(context).size.width / 3,
                      height: MediaQuery.of(context).size.width / 4,
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.topLeft,
                  child: Container(
                    margin: EdgeInsets.only(top: 4, left: 4),
                    padding: EdgeInsets.only(right: 4, left: 4, top: 4),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(50),
                        color: Colors.red),
                    child: Text(
                      "%${widget.model.products[widget.index].discount}",
                      style: TextStyle(color: Colors.white, fontSize: 16),
                    ),
                  ),
                ),
              ],
            ),
            Padding(
              padding: EdgeInsets.only(right: 4),
              child: TitleText(
                widget.model.products[widget.index].name,
                textAlign: TextAlign.right,
              ),
            ),
            Padding(
              padding: EdgeInsets.only(right: 4),
              child: SubtitleText(
                "${widget.model.products[widget.index].time_to_expire} روز مهلت استفاده",
                textAlign: TextAlign.right,
              ),
            ),
            Container(
              height: 1,
              color: Colors.black12,
            ),
            SizedBox(
              height: 4,
            ),
            Padding(
              padding: EdgeInsets.only(left: 4, right: 4),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Expanded(
                    child: Text(
                      "${NumFormat().getPriceFormat(int.parse(widget.model.products[widget.index].price))}" + " تومان",
                      textAlign: TextAlign.right,style: TextStyle(fontSize: 12),
                    ),
                  ),
                  Expanded(
                    child: Text(
                      "${NumFormat().getPriceFormat(int.parse(widget.model.products[widget.index].discount_price))}" + " تومان",
                      textAlign: TextAlign.left,
                      style: TextStyle(
                          color: Colors.red,
                          decoration: TextDecoration.lineThrough,
                          fontSize: 12
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
      onTap: (){
        Navigator.push(context, MaterialPageRoute(builder: (context)=>ProductDetail(
          model: widget.model.products[widget.index],
        )));
      },

    );
  }


}