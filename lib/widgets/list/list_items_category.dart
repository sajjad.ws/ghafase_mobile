import 'package:arzaan_sara/constants/strings.dart';
import 'package:arzaan_sara/models/const_model/category_model.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class ListItemsCategory extends StatefulWidget {
  CategoryModel model;
  int index;

  ListItemsCategory({this.model, this.index});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return ListItemsCategoryState();
  }
}

class ListItemsCategoryState extends State<ListItemsCategory> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      margin: EdgeInsets.all(8),
      padding: EdgeInsets.fromLTRB(24, 4, 0, 4),
      decoration: BoxDecoration(
          color: Theme.of(context).accentColor,
          border: Border.all(
            width: 0,
          ),
          borderRadius: BorderRadius.all(Radius.elliptical(50, 50))),
      child: Padding(
        padding: EdgeInsets.only(right: 24, top: 4),
        child: Text(
          widget.model.categories[widget.index].name,
          style: TextStyle(color: Colors.white),
        ),
      ),
    );
  }


}