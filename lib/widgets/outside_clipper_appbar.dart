import 'package:flutter/cupertino.dart';

class OutsideClipperAppbar extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = Path();

    path.lineTo(0, size.height);

    var firstCurvePointEnd = Offset(size.width * .08, size.height * .75);
    var firstCurveController = Offset(0, size.height * .75);
    path.quadraticBezierTo(firstCurveController.dx, firstCurveController.dy,
        firstCurvePointEnd.dx, firstCurvePointEnd.dy);

    path.lineTo(size.width * .92, size.height * .75);

    var secondCurvePointEnd = Offset(size.width, size.height);
    var secondCurveController = Offset(size.width, size.height * .75);
    path.quadraticBezierTo(secondCurveController.dx, secondCurveController.dy,
        secondCurvePointEnd.dx, secondCurvePointEnd.dy);

    path.lineTo(size.width, 0);
    path.lineTo(0, 0);

/*    var firstCurvePointEnd = Offset(size.width * .4, 0);
    var firstCurveController = Offset(size.width * .3, size.height * .85);
    path.quadraticBezierTo(firstCurveController.dx, firstCurveController.dy,
        firstCurvePointEnd.dx, firstCurvePointEnd.dy);

    path.lineTo(size.width, 0);*/
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => true;
}
