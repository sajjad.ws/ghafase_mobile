import 'package:flutter/material.dart';
import 'package:arzaan_sara/constants/enums.dart';
import 'package:arzaan_sara/global/alerts/alerts.dart';
import 'package:arzaan_sara/resources/save.dart';
import 'package:arzaan_sara/screens/menu/menu_about_us.dart';
import 'package:arzaan_sara/screens/menu/menu_contact_us.dart';
import 'package:arzaan_sara/screens/menu/menu_credit.dart';
import 'package:arzaan_sara/screens/menu/menu_messages.dart';
import 'package:arzaan_sara/screens/menu/menu_request_history.dart';
import 'package:arzaan_sara/screens/menu/menu_transactions.dart';
import 'package:arzaan_sara/screens/phone/phone_register.dart';
import 'package:arzaan_sara/screens/profile/profile_form.dart';
import 'package:arzaan_sara/screens/profile/profile_show.dart';
import 'package:arzaan_sara/screens/splash.dart';

Widget buildDrawerLayout(
    BuildContext context, DriverVerifyType driverVerifyType) {
  List<MenuListModel> menuItemsListConfirmed = <MenuListModel>[
    MenuListModel("اطلاعات کاربری", Icons.account_box),
    MenuListModel("کیف پول", Icons.credit_card),
    MenuListModel("سوابق سفارش", Icons.shopping_cart),
    MenuListModel("تراکنش ها", Icons.compare_arrows),
    MenuListModel("پیام ها", Icons.message),
    MenuListModel("تماس با ما", Icons.phone),
    MenuListModel("درباره ما", Icons.content_paste),
    MenuListModel("خروج از حساب کاربری", Icons.exit_to_app),
  ];

  List<MenuListModel> menuItemsListNotConfirmed = <MenuListModel>[
    MenuListModel("ثبت نام", Icons.account_box),
    MenuListModel("تماس با ما", Icons.phone),
    MenuListModel("درباره ما", Icons.content_paste),
  ];
  double iconSize = 22;
  Widget drawerListItems() {
    List<Widget> items = List();

    if (driverVerifyType == DriverVerifyType.confirmed) {
      for (int i = 0; i < menuItemsListConfirmed.length; i++) {
        items.add(Container(padding: EdgeInsets.only(right: 8),height: 45,child: InkWell(
          child: Row(
            children: <Widget>[
              Icon(
                menuItemsListConfirmed[i].icon,
                size: iconSize,
                color: Colors.black45,
              ),
              Padding(
                padding: EdgeInsets.only(right: 16),
              ),
              Text(
                menuItemsListConfirmed[i].title,
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: Theme.of(context).textTheme.body1.fontSize),
              ),
            ],
          ),
          onTap: () async {
            switch (i) {
              case 0:
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => ProfileShow(
                          verifyType: driverVerifyType,
                        )));
                break;
              case 1:
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => MenuCredit()));
                break;

              case 2:
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => MenuRequestHistory(
                          verifyType: driverVerifyType,
                        )));
                break;

              case 3:
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => MenuTransactions()));
                break;
              case 4:
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => MenuMessages()));
                break;
              case 5:
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => MenuContactUs()));
                break;
              case 6:
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => MenuAboutUs()));
                break;

              case 7:
                Alerts.confidence(context, onOk: () {
                  Navigator.pop(context);

                  Save().clearSavedData();
                  Save().setLogin(false);
                  Save().setFirstTime(false);
                  Save().setPhoneInserted(false);
                  Navigator.pushReplacement(context,
                      MaterialPageRoute(builder: (context) => Splash()));
                });

                break;
            }
          },
        ),));
      }
    } else {
      for (int i = 0; i < menuItemsListNotConfirmed.length; i++) {
        items.add(Container(padding: EdgeInsets.only(right: 8),height: 45,child: InkWell(
          child: Row(
            children: <Widget>[
              Icon(
                menuItemsListNotConfirmed[i].icon,
                size: iconSize,
                color: Colors.black45,
              ),
              Padding(
                padding: EdgeInsets.only(right: 16),
              ),
              Text(
                menuItemsListNotConfirmed[i].title,
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: Theme.of(context).textTheme.body1.fontSize),
              ),
            ],
          ),
          onTap: () async {
            switch (i) {
              case 0:
                bool isPhoneInserted = await Save().isPhoneInserted();
                if (isPhoneInserted)
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => ProfileForm(
                            type: ProfileFormType.register,
                          )));
                else
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => PhoneRegister()));
                break;

              case 5:
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => MenuContactUs()));
                break;
              case 6:
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => MenuAboutUs()));
                break;
            }
          },
        ),));
      }
    }

    return ListView(
      padding: EdgeInsets.all(0),
      children: items,
    );
  }

  return SizedBox(
    width: MediaQuery.of(context).size.width * 3 / 5,
    child: Drawer(
      child: Column(
        children: <Widget>[
          Container(
            child: Padding(
              padding: EdgeInsets.only(top: kToolbarHeight),
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Image.asset("assets/images/logo_splash.png"),
                    Container(
                      child: Text(
                        "Version 1.0",
                        style: TextStyle(color: Theme.of(context).accentColor),
                      ),
                      width: 150,
                    ),
                    Image.asset("assets/images/obar_text.png"),
                    Padding(
                      padding: EdgeInsets.only(top: 16),
                    ),
                    Text(
                      "www.lormipsom.com",
                      style: TextStyle(color: Theme.of(context).accentColor),
                    ),
                    Container(
                      height: 1,
                      color: Colors.black12,
                      margin: EdgeInsets.only(left: 16, right: 16, top: 24),
                    )
                  ],
                ),
              ),
            ),
          ),
          Expanded(
            child: drawerListItems(),
          )
        ],
      ),
    ),
  );
}

class MenuListModel {
  String _title;
  IconData _icon;

  MenuListModel(this._title, this._icon);

  IconData get icon => _icon;

  String get title => _title;
}
