import 'package:flutter/material.dart';
import 'package:arzaan_sara/constants/enums.dart';
import 'package:arzaan_sara/constants/status_converter.dart';
import 'package:arzaan_sara/constants/strings.dart';
import 'package:arzaan_sara/global/common_views/subtitle_text.dart';
import 'package:arzaan_sara/global/common_views/title_text.dart';
import 'package:arzaan_sara/global/converter/date_time_converter.dart';
import 'package:arzaan_sara/models/trasaction_model.dart';

class TransactionItems extends StatelessWidget {
  TransactionModel model;
  int index;

  TransactionItems({this.model, this.index});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(

      margin: EdgeInsets.only(right: 16, left: 16, bottom: 16),
      decoration: BoxDecoration(
          color: Strings.cardItemColor,
          borderRadius: BorderRadius.all(Radius.circular(8)),
          border: Border.all(color: Colors.black38, width: 1)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.all(8),
            child: Row(
              textDirection: TextDirection.rtl,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Expanded(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      statusIcon(model.transactions[index].type),
                      TitleText(
                        StatusConverter.getTransactionStr(
                            model.transactions[index].type),
                        textAlign: TextAlign.right,
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: SubtitleText(
                      DateTimeConverter.getToJalTimeDate(
                          model.transactions[index].created_at),
                      textAlign: TextAlign.left),
                ),
              ],
            ),
          ),
          Container(
            height: 1,
            color: Colors.black12,
          ),
          Padding(
            padding: EdgeInsets.all(8),
            child: Row(
              textDirection: TextDirection.rtl,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Expanded(
                  child: TitleText(
                    countPrice() + " تومان",
                    textAlign: TextAlign.right,
                  ),
                ),
                Expanded(
                  child: SubtitleText(
                      StatusConverter.getTransactionTypeStr(
                          model.transactions[index].type),
                      textAlign: TextAlign.left),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget statusIcon(type) {
    if (StatusConverter.getTransactionType(type) ==
        TransactionType.chargingWallet) {
      return Icon(
        Icons.arrow_downward,
        color: Colors.green,
      );
    } else {
      return Icon(
        Icons.arrow_upward,
        color: Colors.red,
      );
    }
  }

  String countPrice() {
    int amount = int.parse(model.transactions[index].amount);
    int value = 0;
    if (model.transactions[index].value != "null") {
      value = int.parse(model.transactions[index].value);
    }

    return (amount - value).toString();
  }
}
