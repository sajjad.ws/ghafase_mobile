import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:arzaan_sara/constants/enums.dart';
import 'package:arzaan_sara/constants/status_converter.dart';
import 'package:arzaan_sara/constants/strings.dart';
import 'package:arzaan_sara/global/common_views/subtitle_text.dart';
import 'package:arzaan_sara/global/common_views/title_text.dart';
import 'package:arzaan_sara/global/converter/date_time_converter.dart';
import 'package:arzaan_sara/global/views/easy_button.dart';
import 'package:arzaan_sara/models/request_model.dart';
import 'package:arzaan_sara/screens/map/map_cargo_route.dart';
import 'package:arzaan_sara/screens/request/request_detail.dart';
import 'package:url_launcher/url_launcher.dart';

class RequestHistoryItems extends StatefulWidget {
  final RequestModel model;
  final int index;
  final RequestSteps steps;
  final DriverVerifyType verifyType;

  RequestHistoryItems({this.model, this.index, this.steps, this.verifyType});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return RequestHistoryItemsState();
  }
}

class RequestHistoryItemsState extends State<RequestHistoryItems> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return Card(
      color: Strings.cardItemColor,
      margin: EdgeInsets.only(right: 8, left: 8, bottom: 16),
      elevation: 4,
      clipBehavior: Clip.antiAlias,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(8))),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(
              top: 12,
              bottom: 12,
            ),
            color: widget.steps == RequestSteps.cancelByCarrier ||
                    widget.steps == RequestSteps.cancelByDriver
                ? Colors.red
                : Colors.green,
            child: Text(
              "وضعیت: " +
                  RequestModel().returnTitle(
                      widget.model.requests[widget.index].last_status),
              textAlign: TextAlign.center,
              style: TextStyle(color: Colors.white),
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: 8, right: 8, bottom: 16, top: 16),
            child: subTitleContent(),
          )
        ],
      ),
    );
  }

  Widget subTitleContent() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        //origin address
        Row(
          children: <Widget>[
            Icon(
              Icons.location_on,
              size: 18,
              color: Colors.black38,
            ),
            TitleText(
              "آدرس مبدا:",
            )
          ],
        ),
        SizedBox(
          height: 4,
        ),
        Padding(
          padding: EdgeInsets.only(right: 22),
          child: SubtitleText(
            widget.model.requests[widget.index].origin_address,
            textAlign: TextAlign.right,
          ),
        ),

        //destination address
        Row(
          children: <Widget>[
            Icon(
              Icons.location_on,
              size: 18,
              color: Colors.black38,
            ),
            TitleText(
              "آدرس مقصد:",
            )
          ],
        ),
        SizedBox(
          height: 4,
        ),
        Padding(
          padding: EdgeInsets.only(right: 22),
          child: SubtitleText(
            widget.model.requests[widget.index].destination_address,
            textAlign: TextAlign.right,
          ),
        ),

        //
        SizedBox(
          height: 12,
        ),
        //map
        Column(
          children: <Widget>[
            EasyButton(
              text: "مشاهده آدرس روی نقشه",
              isFilled: true,
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => MapCargoRoute(
                          originLatLng: LatLng(
                              double.parse(widget.model.requests[widget.index]
                                  .origin_latitude),
                              double.parse(widget.model.requests[widget.index]
                                  .origin_longitude)),
                          destinationLatLng: LatLng(
                              double.parse(widget.model.requests[widget.index]
                                  .destination_latitude),
                              double.parse(widget.model.requests[widget.index]
                                  .destination_longitude)),
                        )));
              },
            ),
          ],
        ),
        //
        SizedBox(
          height: 12,
        ),
        //carry time
        Row(
          children: <Widget>[
            Icon(
              Icons.access_time,
              size: 18,
              color: Colors.black38,
            ),
            SizedBox(
              width: 4,
            ),
            Text("زمان بارگیری: ", style: Theme.of(context).textTheme.title),
            Text(
                DateTimeConverter.getToJalBetweenTimeDate(
                    widget.model.requests[widget.index].loading_start_time,
                    widget.model.requests[widget.index].loading_end_time),
                style: Theme.of(context).textTheme.subtitle),
          ],
        ),
        //
        SizedBox(
          height: 8,
        ),
        //load time
        Row(
          children: <Widget>[
            Icon(
              Icons.access_time,
              size: 18,
              color: Colors.black38,
            ),
            SizedBox(
              width: 4,
            ),
            Text("زمان تخیله بار: ", style: Theme.of(context).textTheme.title),
            Text(
                DateTimeConverter.getToJalBetweenTimeDate(
                    widget.model.requests[widget.index].evacuation_start_time,
                    widget.model.requests[widget.index].evacuation_end_time),
                style: Theme.of(context).textTheme.subtitle),
          ],
        ),
        //
        SizedBox(
          height: 8,
        ),
        //price
        Row(
          children: <Widget>[
            Icon(
              Icons.credit_card,
              size: 18,
              color: Colors.black38,
            ),
            SizedBox(
              width: 4,
            ),
            Text("قیمت: ", style: Theme.of(context).textTheme.title),
            Text(price(),
                style: Theme.of(context).textTheme.subtitle),
          ],
        ),
        //
        SizedBox(
          height: 12,
        ),
        buttonState(),
      ],
    );
  }

  Widget buttonState() {
    bool driverConfirmed = widget.verifyType == DriverVerifyType.confirmed;

    if (driverConfirmed) {
      if (widget.steps == RequestSteps.end) {
        return cancelStepButtons();
      }

      return Padding(
        padding: EdgeInsets.only(left: 64, right: 64),
        child: EasyButton(
          text: "جزییات بیشتر",
          isFilled: false,
          textColor: Colors.black,
          onPressed: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => RequestDetail(
                      request: widget.model.requests[widget.index],
                    )));
          },
        ),
      );
    } else {
      return Container();
    }
  }

  Widget cancelStepButtons() {
    return Padding(
      padding: EdgeInsets.only(left: 64, right: 64),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          EasyButton(
            text: "جزییات بیشتر",
            isFilled: false,
            textColor: Colors.black,
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => RequestDetail(
                        request: widget.model.requests[widget.index],
                      )));
            },
          ),
          SizedBox(
            height: 8,
          ),
          EasyButton(
            text: "تماس با صاحب بار",
            textColor: Colors.white,
            isFilled: true,
            onPressed: () {
              launch("tel:09212098662");
            },
          )
        ],
      ),
    );
  }

  price() {
    if (widget.model.requests[widget.index].price == "null" ||
        widget.model.requests[widget.index].price == "0") return "توافقی";

    return widget.model.requests[widget.index].price + " تومان";
  }

}
