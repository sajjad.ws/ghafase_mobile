import 'package:flutter/material.dart';
import 'package:arzaan_sara/constants/strings.dart';
import 'package:arzaan_sara/dialogs/dialog_message_detail.dart';
import 'package:arzaan_sara/global/common_views/button_text.dart';
import 'package:arzaan_sara/global/common_views/subtitle_text.dart';
import 'package:arzaan_sara/global/common_views/title_text.dart';
import 'package:arzaan_sara/global/converter/date_time_converter.dart';
import 'package:arzaan_sara/models/message_model.dart';

class MessageItems extends StatelessWidget {
  MessageModel model;
  int index;

  MessageItems({this.model, this.index});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(

      margin: EdgeInsets.only(right: 16, left: 16, bottom: 16),
      decoration: BoxDecoration(
          color: Strings.cardItemColor,
          borderRadius: BorderRadius.all(Radius.circular(8)),
          border: Border.all(color: Colors.black38, width: 1)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.all(8),
            child: Row(
              textDirection: TextDirection.rtl,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                TitleText(
                  model.messages[index].title,
                  textAlign: TextAlign.right,
                ),
                Expanded(
                  child: SubtitleText(
                      DateTimeConverter.getToJalTimeDate(
                          model.messages[index].created_at),
                      textAlign: TextAlign.left),
                ),
              ],
            ),
          ),
          Container(
            height: 1,
            color: Colors.black12,
          ),
          Padding(
            padding: EdgeInsets.all(8),
            child: Column(
              textDirection: TextDirection.rtl,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Text(
                  model.messages[index].content,
                  textAlign: TextAlign.right,
                  maxLines: 3,
                  overflow: TextOverflow.ellipsis,
                ),
                SizedBox(
                  height: 8,
                ),
                InkWell(
                  child: ButtonText(
                    "متن کامل پیام",
                    textAlign: TextAlign.right,
                  ),
                  onTap: () {
                    showDialog(
                        context: context,
                        builder: (context) => DialogMessageDetail(
                              title: model.messages[index].title,
                              content: model.messages[index].content,
                            ));
                  },
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
