import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:arzaan_sara/constants/enums.dart';
import 'package:arzaan_sara/constants/status_converter.dart';
import 'package:arzaan_sara/global/alerts/alerts.dart';
import 'package:arzaan_sara/widgets/fcm/dialog/dialog_request_message.dart';
import 'package:rxdart/rxdart.dart';

class FCM {
  static manage(msg, BuildContext context) {
    Map notification = msg['notification'];
    Map data = msg['data'];

    FCM fcm = FCM();

    fcm.insideApp(data, context);
  }

  insideApp(Map data, BuildContext context) {
    String type = data['type'].toString();
    if (StatusConverter.getFcmType(type) == FCMType.message)
      message(data, context);
    else if (StatusConverter.getFcmType(type) == FCMType.request)
      request(data, context);
  }

  message(Map data, BuildContext context) async {
    String title = data['title'];
    String sub_title = data['sub_title'];
    String activitydata = data['activitydata'];

    Alerts.info(context, "یک پیام جدید");

    //const bool isWeb = identical(0, 0.0);
  }

  request(Map data, BuildContext context) async {
    String title = data['title'];
    String sub_title = data['sub_title'];

    showDialog(
        context: context,
        builder: (context) => DialogRequestMessage(
              title: title,
              content: sub_title,
            ));

    //const bool isWeb = identical(0, 0.0);
  }
}
