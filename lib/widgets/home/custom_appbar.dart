import 'package:arzaan_sara/constants/strings.dart';
import 'package:flutter/material.dart';

class CustomAppbar extends StatefulWidget {
  VoidCallback onMenuPress;

  CustomAppbar({this.onMenuPress});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _CustomAppbarState();
  }
}

class _CustomAppbarState extends State<CustomAppbar> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return ClipPath(
      clipper: _OutsideClipper(),
      child: Container(
        height: MediaQuery.of(context).size.width*2/5,
          padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.width/10),
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topRight,
                  end: Alignment.bottomLeft,
                  colors: [Strings.greenLight, Strings.greenDark])),
        child: Row(
          children: <Widget>[
            Padding(padding: EdgeInsets.only(right: 8),child: IconButton(
              icon: Icon(Icons.filter_list,color: Colors.white,size: 32,),
              onPressed: widget.onMenuPress,
            ),),
            Expanded(
              child: Padding(
                padding: EdgeInsets.only(left: 16),
                child: TextField(
                  style: TextStyle(color: Colors.white),
                  textAlign: TextAlign.right,
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(
                    suffixIcon: Icon(Icons.search,color: Colors.white,size: 28,),
                    contentPadding: EdgeInsets.only(right: 8,),
                    hintText: 'جستجو در محصولات...',
                    hintStyle: TextStyle(fontSize: 16,color: Colors.white70),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(50),
                      borderSide: BorderSide(
                        width: 0,
                        style: BorderStyle.none,
                      ),
                    ),
                    filled: true,
                    fillColor: Colors.black26,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class _OutsideClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = Path();

    path.lineTo(0, size.height);

    var firstCurvePointEnd = Offset(size.width * .08, size.height * .60);
    var firstCurveController = Offset(0 , size.height * .60);
    path.quadraticBezierTo(firstCurveController.dx, firstCurveController.dy,
        firstCurvePointEnd.dx, firstCurvePointEnd.dy);


    path.lineTo(size.width*.92, size.height * .60);


    var secondCurvePointEnd = Offset(size.width, size.height);
    var secondCurveController = Offset(size.width , size.height * .60);
    path.quadraticBezierTo(secondCurveController.dx, secondCurveController.dy,
        secondCurvePointEnd.dx, secondCurvePointEnd.dy);



    path.lineTo(size.width, 0);
    path.lineTo(0, 0);


/*    var firstCurvePointEnd = Offset(size.width * .4, 0);
    var firstCurveController = Offset(size.width * .3, size.height * .85);
    path.quadraticBezierTo(firstCurveController.dx, firstCurveController.dy,
        firstCurvePointEnd.dx, firstCurvePointEnd.dy);

    path.lineTo(size.width, 0);*/
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => true;
}

class _InsideClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = Path();

    path.lineTo(0, size.height - 8);

    var firstCurvePointEnd = Offset(size.width * .38, 0);
    var firstCurveController = Offset(size.width * .25, size.height * .82);
    path.quadraticBezierTo(firstCurveController.dx, firstCurveController.dy,
        firstCurvePointEnd.dx, firstCurvePointEnd.dy);

    path.lineTo(size.width, 0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => true;
}
