import 'dart:async';

import 'package:arzaan_sara/blocs/product_bloc.dart';
import 'package:arzaan_sara/blocs/store_bloc.dart';
import 'package:arzaan_sara/constants/strings.dart';
import 'package:arzaan_sara/global/common_views/button_text.dart';
import 'package:arzaan_sara/global/common_views/list_empty_view.dart';
import 'package:arzaan_sara/global/common_views/list_net_error.dart';
import 'package:arzaan_sara/global/formatter/num_fornat.dart';
import 'package:arzaan_sara/models/const_model/banner_model.dart';
import 'package:arzaan_sara/models/const_model/category_model.dart';
import 'package:arzaan_sara/models/product_model.dart';
import 'package:arzaan_sara/models/store_model.dart';
import 'package:arzaan_sara/resources/save.dart';
import 'package:arzaan_sara/widgets/list/list_items_category.dart';
import 'package:arzaan_sara/widgets/list/list_items_products.dart';
import 'package:arzaan_sara/widgets/list/list_items_stores.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MainTab extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return MainTabState();
  }
}

class MainTabState extends State<MainTab> {
  int count = 19506696;
  Timer _timer;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    sendProductRequest();
    sendStoreRequest();

    _timer = Timer.periodic(
        Duration(seconds: 1),
        (Timer timer) => setState(() {
              count--;
              if (count < 1) {
                timer.cancel();
              }
            }));
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      color: Strings.bgColor,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Expanded(
            child: ListView(
              padding: EdgeInsets.only(bottom: 16),
              children: <Widget>[
                Container(
                  height: MediaQuery.of(context).size.width * 2 / 5,
                  child: FutureBuilder(
                    future: getBanners(),
                    builder: (context, AsyncSnapshot<List<Widget>> snapshot) {
                      if (snapshot.hasData) {
                        return CarouselSlider(
                          items: snapshot.data,
                          aspectRatio: 1 / 1,
                          initialPage: 0,
                          height: MediaQuery.of(context).size.width * 2 / 5,
                          viewportFraction: 1.0,
                          enableInfiniteScroll: true,
                          reverse: false,
                          autoPlay: true,
                          autoPlayInterval: Duration(seconds: 3),
                          autoPlayAnimationDuration:
                              Duration(milliseconds: 800),
                          autoPlayCurve: Curves.fastOutSlowIn,
                          pauseAutoPlayOnTouch: Duration(seconds: 10),
                          onPageChanged: (res) {},
                          scrollDirection: Axis.horizontal,
                        );
                      }
                      return Center(
                        child: CircularProgressIndicator(),
                      );
                    },
                  ),
                ),
                SizedBox(
                  height: 8,
                ),
                Card(
                  margin: EdgeInsets.only(right: 8, left: 8),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(50),
                  ),
                  child: Padding(
                    padding: EdgeInsets.only(
                        left: 16, right: 16, bottom: 12, top: 12),
                    child: Row(
                      children: <Widget>[
                        Image.asset(
                          "assets/images/apple.jpeg",
                          width: 18,
                          height: 18,
                        ),
                        SizedBox(
                          width: 8,
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 8),
                          child: Text(
                            "وعده های غذایی بازیافت شده: ",
                            style: TextStyle(fontSize: 14),
                          ),
                        ),
                        Expanded(
                          child: Align(
                            alignment: Alignment.centerLeft,
                            child: Padding(
                              padding: EdgeInsets.only(top: 8),
                              child: Text(
                                NumFormat().getPriceFormat(count),
                                style: TextStyle(
                                    color: Theme.of(context).accentColor,
                                    fontSize: 20),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                Container(
                  height: 50,
                  alignment: Alignment.center,
                  child: FutureBuilder(
                    future: getCategoryData(),
                    builder: (context, AsyncSnapshot<CategoryModel> snapshot) {
                      if (snapshot.hasData) {
                        return ListView.builder(
                            scrollDirection: Axis.horizontal,
                            itemCount: snapshot.data.categories.length,
                            itemBuilder: (context, index) {
                              return ListItemsCategory(
                                model: snapshot.data,
                                index: index,
                              );
                            });
                      }
                      return Center(
                        child: CircularProgressIndicator(),
                      );
                    },
                  ),
                ),
                SizedBox(
                  height: 8,
                ),
                Padding(
                  padding: EdgeInsets.only(left: 8, right: 8),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      Expanded(
                        child: Text(
                          "محصولات پر تخفیف",
                          textAlign: TextAlign.right,
                        ),
                      ),
                      Expanded(
                        child: InkWell(
                          onTap: () {},
                          child: ButtonText(
                            "بیشتر",
                            textAlign: TextAlign.left,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                SizedBox(
                  height: 4,
                ),
                Container(
                  height: (MediaQuery.of(context).size.width * 3 / 5),
                  width: MediaQuery.of(context).size.width / 3,
                  alignment: Alignment.center,
                  child: StreamBuilder(
                    stream: productBlocProducts.actions,
                    builder: (context, AsyncSnapshot<ProductModel> snapshot) {
                      if (snapshot.hasError) {
                        return ListNetError(
                          onRefresh: () {
                            sendProductRequest();
                          },
                        );
                      }
                      if (snapshot.hasData) {
                        if (snapshot.data.products.isEmpty) {
                          return ListEmptyView(
                            onRefresh: () {
                              sendProductRequest();
                            },
                          );
                        }

                        return ListView.builder(
                          scrollDirection: Axis.horizontal,
                            itemCount: snapshot.data.products.length,
                            itemBuilder: (context, index) {
                              return ListItemsProduct(model: snapshot.data,index: index,);
                            });
                      }

                      return Center(
                        child: CircularProgressIndicator(),
                      );
                    },
                  ),
                ),

                SizedBox(
                  height: 8,
                ),
                Padding(
                  padding: EdgeInsets.only(left: 8, right: 8),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      Expanded(
                        child: Text(
                          "نزدیکترین فروشگاه ها",
                          textAlign: TextAlign.right,
                        ),
                      ),
                      Expanded(
                        child: InkWell(
                          onTap: () {},
                          child: ButtonText(
                            "بیشتر",
                            textAlign: TextAlign.left,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                SizedBox(
                  height: 4,
                ),
                Container(
                  height: (MediaQuery.of(context).size.width *3/ 5),
                  width: MediaQuery.of(context).size.width / 3,
                  alignment: Alignment.center,
                  child: StreamBuilder(
                    stream: storeBlocStores.actions,
                    builder: (context, AsyncSnapshot<StoreModel> snapshot) {
                      if (snapshot.hasError) {
                        return ListNetError(
                          onRefresh: () {
                            sendStoreRequest();
                          },
                        );
                      }
                      if (snapshot.hasData) {
                        if (snapshot.data.stores.isEmpty) {
                          return ListEmptyView(
                            onRefresh: () {
                              sendStoreRequest();
                            },
                          );
                        }

                        return ListView.builder(
                            scrollDirection: Axis.horizontal,
                            itemCount: snapshot.data.stores.length,
                            itemBuilder: (context, index) {
                              return ListItemsStore(model: snapshot.data,index: index,);
                            });
                      }

                      return Center(
                        child: CircularProgressIndicator(),
                      );
                    },
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  Future<List<Widget>> getBanners() async {
    BannerModel model = await Save().getBannerData();
    List<Widget> images = [];

    for (int i = 0; i < model.banners.length; i++) {
      images.add(CachedNetworkImage(
        imageUrl: Strings.siteUrl + model.banners[i].url,
        placeholder: (context, url) => Center(
          child: CircularProgressIndicator(),
        ),
      ));
    }

    return images;
  }

  Future<CategoryModel> getCategoryData() async {
    CategoryModel model = await Save().getCategoryData();
    return model;
  }

  sendProductRequest() async {
    productBlocProducts.fetchProducts(apiAddress: "get-products", body: {
      "discount": "order",
    });
  }

  sendStoreRequest() async {
    storeBlocStores.fetchStores(apiAddress: "get-stores", body: {
    });
  }
}
