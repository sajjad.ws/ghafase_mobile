import 'package:arzaan_sara/blocs/product_bloc.dart';
import 'package:arzaan_sara/blocs/store_bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HomeAppbarMain extends StatefulWidget {


  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return HomeAppbarMainState();
  }
}

class HomeAppbarMainState extends State<HomeAppbarMain> {

  final ctrlController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Row(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(right: 8),
          child: IconButton(
            icon: Icon(
              Icons.filter_list,
              color: Colors.white,
              size: 32,
            ),
          ),
        ),
        Expanded(
          child: Padding(
            padding: EdgeInsets.only(left: 16),
            child: TextField(
              onChanged: (value){
                productBlocProducts.fetchProducts(apiAddress: "get-products", body: {
                  "discount": "order",
                  "search": value
                });
                storeBlocStores.fetchStores(apiAddress: "get-stores", body: {
                  "search": value
                });

              },
              style: TextStyle(color: Colors.white),
              textAlign: TextAlign.right,
              keyboardType: TextInputType.text,
              decoration: InputDecoration(
                suffixIcon: Icon(
                  Icons.search,
                  color: Colors.white,
                  size: 28,
                ),
                contentPadding: EdgeInsets.only(
                  right: 8,
                ),
                hintText: 'جستجو در محصولات...',
                hintStyle: TextStyle(fontSize: 16, color: Colors.white70),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(50),
                  borderSide: BorderSide(
                    width: 0,
                    style: BorderStyle.none,
                  ),
                ),
                filled: true,
                fillColor: Colors.black26,
              ),
            ),
          ),
        ),
      ],
    );
  }


}