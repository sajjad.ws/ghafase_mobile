import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:arzaan_sara/blocs/request_bloc.dart';
import 'package:arzaan_sara/constants/enums.dart';
import 'package:arzaan_sara/constants/status_converter.dart';
import 'package:arzaan_sara/constants/strings.dart';
import 'package:arzaan_sara/dialogs/dialog_validation.dart';
import 'package:arzaan_sara/dialogs/driver/dialog_driver_confirmed.dart';
import 'package:arzaan_sara/dialogs/driver/dialog_driver_rate.dart';
import 'package:arzaan_sara/dialogs/payment/dialog_payment.dart';
import 'package:arzaan_sara/global/alerts/alerts.dart';
import 'package:arzaan_sara/global/common_views/subtitle_text.dart';
import 'package:arzaan_sara/global/common_views/title_text.dart';
import 'package:arzaan_sara/global/converter/date_time_converter.dart';
import 'package:arzaan_sara/global/views/count_down_timer.dart';
import 'package:arzaan_sara/global/views/easy_button.dart';
import 'package:arzaan_sara/global/views/easy_flat_button.dart';
import 'package:arzaan_sara/global/views/flat_button_icon.dart';
import 'package:arzaan_sara/global/views/primary_button_streamer.dart';
import 'package:arzaan_sara/models/const_model/city_model.dart';
import 'package:arzaan_sara/models/const_model/setting_model.dart';
import 'package:arzaan_sara/models/const_model/vehicle_group_model.dart';
import 'package:arzaan_sara/models/const_model/vehicle_model.dart';
import 'package:arzaan_sara/models/driver_model.dart';
import 'package:arzaan_sara/models/request_model.dart';
import 'package:arzaan_sara/resources/save.dart';
import 'package:arzaan_sara/screens/map/map_cargo_route.dart';
import 'package:arzaan_sara/screens/request/request_detail.dart';
import 'package:url_launcher/url_launcher.dart';

class HomeRequestItems extends StatefulWidget {
  final RequestModel model;
  final int index;
  final RequestSteps steps;
  final DriverVerifyType verifyType;

  HomeRequestItems({this.model, this.index, this.steps, this.verifyType});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState

    
    return HomeRequestItemsState();
  }
}

class HomeRequestItemsState extends State<HomeRequestItems> {
  bool showCancelButton = true;

  timerFinish() {}

  GlobalKey<PrimaryButtonStreamerState> keyStartTravel =
      GlobalKey<PrimaryButtonStreamerState>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Card(
      color: Strings.cardItemColor,
      margin: EdgeInsets.only(right: 8, left: 8, bottom: 16),
      elevation: 4,
      clipBehavior: Clip.antiAlias,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(8))),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          statusTitle(),
          Container(
            padding: EdgeInsets.only(left: 8, right: 8, bottom: 16, top: 16),
            child: subTitleContent(),
          )
        ],
      ),
    );
  }

  Widget subTitleContent() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        cargoOwnerView(),

        addressView(),

        //
        SizedBox(
          height: 12,
        ),
        //map
        showAddressOnMapButton(),
        //
        SizedBox(
          height: 12,
        ),
        //carry time
        Row(
          children: <Widget>[
            Icon(
              Icons.access_time,
              size: 18,
              color: Colors.black38,
            ),
            SizedBox(
              width: 4,
            ),
            Text("زمان بارگیری: ", style: Theme.of(context).textTheme.title),
            Text(
                DateTimeConverter.getToJalBetweenTimeDate(
                    widget.model.requests[widget.index].loading_start_time,
                    widget.model.requests[widget.index].loading_end_time),
                style: Theme.of(context).textTheme.subtitle),
          ],
        ),
        //
        SizedBox(
          height: 8,
        ),
        //load time
        Row(
          children: <Widget>[
            Icon(
              Icons.access_time,
              size: 18,
              color: Colors.black38,
            ),
            SizedBox(
              width: 4,
            ),
            Text("زمان تخیله بار: ", style: Theme.of(context).textTheme.title),
            Text(
                DateTimeConverter.getToJalBetweenTimeDate(
                    widget.model.requests[widget.index].evacuation_start_time,
                    widget.model.requests[widget.index].evacuation_end_time),
                style: Theme.of(context).textTheme.subtitle),
          ],
        ),
        //
        SizedBox(
          height: 8,
        ),
        FutureBuilder(
          future:
              getVehicleName(widget.model.requests[widget.index].vehicle_id),
          builder: (context, snapshot) {
            if (snapshot.hasData)
              return Row(
                children: <Widget>[
                  Icon(
                    Icons.directions_car,
                    size: 18,
                    color: Colors.black38,
                  ),
                  SizedBox(
                    width: 4,
                  ),
                  Text("نوع وسیله نقلیه: ",
                      style: Theme.of(context).textTheme.title),
                  Text(snapshot.data,
                      style: Theme.of(context).textTheme.subtitle),
                ],
              );
            return Container();
          },
        ),

        SizedBox(
          height: 8,
        ),
        //price
        Row(
          children: <Widget>[
            Icon(
              Icons.credit_card,
              size: 18,
              color: Colors.black38,
            ),
            SizedBox(
              width: 4,
            ),
            Text("قیمت: ", style: Theme.of(context).textTheme.title),
            Text(price(), style: Theme.of(context).textTheme.subtitle),
          ],
        ),
        //
        SizedBox(
          height: 12,
        ),
        FutureBuilder(
          future: getSettings(),
          builder: (context, AsyncSnapshot<SettingModel> snapshot) {
            if (snapshot.hasData) {
              return buttonState(snapshot);
            }

            return Center(
              child: CircularProgressIndicator(),
            );
          },
        )
      ],
    );
  }

  Widget buttonState(AsyncSnapshot<SettingModel> snapshot) {
    bool driverConfirmed = widget.verifyType == DriverVerifyType.confirmed;

    if (driverConfirmed) {
      if (widget.steps == RequestSteps.acceptByDriver) {
        return acceptByDriverStepButtons(snapshot);
      } else if (widget.steps == RequestSteps.acceptDriverByOwner) {
        return acceptDriverByOwnerStepButtons();
      } else if (widget.steps == RequestSteps.start) {
        return startStepButtons();
      } else if (widget.steps == RequestSteps.end) {
        return endStepButtons();
      }

      return commonButtons();
    } else {
      return commonButtons();
    }
  }

  //////////////////////////////////// buttons /////////////////////////

  Widget commonButtons() {
    return Row(
      children: <Widget>[
        Expanded(
          child: EasyButton(
            text: "جزییات بیشتر",
            isFilled: false,
            textColor: Colors.black,
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => RequestDetail(
                            request: widget.model.requests[widget.index],
                            verifyType: widget.verifyType,
                        steps: widget.steps,
                          )));
            },
          ),
        ),
        SizedBox(
          width: 8,
        ),
        Expanded(
          child: EasyButton(
            text: "قبول درخواست",
            isFilled: true,
            textColor: Colors.white,
            onPressed: () async {
              if (widget.verifyType == DriverVerifyType.confirmed) {
                SettingModel settings = await Save().getSettingData();
                if (settings.is_active_receiver_commission == "1") {
                  String vehicleId = widget.model.requests[widget.index].vehicle_id;
                  String commition = await VehicleGroupModel.returnCommissionByVehicleId(vehicleId);
                  showDialog(
                      context: context,
                      builder: (context) => DialogPayment(
                          price: commition,
                          requestId: widget.model.requests[widget.index].id));
                } else {}
              } else {
                showDialog(
                    context: context,
                    builder: (context) => DialogDriverConfirmed());
              }
            },
          ),
        ),
      ],
    );
  }

  Widget acceptByDriverStepButtons(AsyncSnapshot<SettingModel> snapshot) {
    checkCancelTimeFinish(snapshot.data.cargo_cancelable_time,
        widget.model.requests[widget.index].updated_at);

    return Padding(
      padding: EdgeInsets.only(right: 64, left: 64),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          EasyButton(
            text: "جزییات بیشتر",
            isFilled: false,
            textColor: Colors.black,
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => RequestDetail(
                            request: widget.model.requests[widget.index],
                            verifyType: widget.verifyType,
                        steps: widget.steps,
                          )));
            },
          ),
          SizedBox(
            height: 8,
          ),
          Visibility(
            visible: showCancelButton,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                PrimaryButtonStreamer<String>(
                  backgroundColor: Colors.red,
                  stream: requestBlocCancelOwner.actions,
                  onPressed: () {
                    primaryButtonStreamerState.setState(() {
                      primaryButtonStreamerState.isLoading = false;
                    });

                    showDialog(
                        context: context,
                        builder: (context) => DialogValidation(
                              title: "آیا از لغو درخواست خود مطمئن هستید؟",
                              content:
                              "در صورت لغو درخواست امکان بازیابی آن وجود نخواهد داشت",
                              onPress: () {
                                Navigator.pop(context);
                                primaryButtonStreamerState.setState(() {
                                  primaryButtonStreamerState.isLoading = true;
                                });
                                requestBlocCancelOwner.fetchCancelOwner(
                                    apiAddress: "cancel-by-owner",
                                    body: {
                                      "request_id":
                                          widget.model.requests[widget.index].id
                                    });
                              },
                            ));
                  },
                  text: "لغو درخواست",
                  afterFinish: (AsyncSnapshot snapshot) async {
                    Alerts.success(
                        context, "درخواست شما به درخواست های معلق انتقال یافت");
                    requestBlocCancelOwner.dispose();
                  },
                ),
                SizedBox(
                  height: 8,
                ),
                CountDownTimer(
                  child: (timer) {
                    return Text(
                      "${DateTimeConverter.formatTime(timer)} " + "فرصت تا لغو",
                      style: TextStyle(
                        color: Colors.red,
                      ),
                      textAlign: TextAlign.center,
                    );
                  },
                  onFinish: (timer) {
                    setState(() {});
                  },
                  date: widget.model.requests[widget.index].updated_at,
                  expTime: snapshot.data.cargo_cancelable_time,
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget acceptDriverByOwnerStepButtons() {
    return Padding(
      padding: EdgeInsets.only(right: 64, left: 64),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          EasyButton(
            text: "جزییات بیشتر",
            isFilled: false,
            textColor: Colors.black,
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => RequestDetail(
                            request: widget.model.requests[widget.index],
                            verifyType: widget.verifyType,
                        steps: widget.steps,
                          )));
            },
          ),
          SizedBox(
            height: 8,
          ),
          PrimaryButtonStreamer<String>(
            backgroundColor: Colors.green,
            stream: requestBlocStart.actions,
            key: keyStartTravel,
            onPressed: () {
              final startTime = DateTime.parse(
                  widget.model.requests[widget.index].loading_start_time);
              final endTime = DateTime.parse(
                  widget.model.requests[widget.index].loading_end_time);
              //final currentTime = DateTime(2019,12,02,05,13,00);
              final currentTime = DateTime.now();

              if (currentTime.isAfter(startTime) &&
                  currentTime.isBefore(endTime)) {
                requestBlocStart.fetchStart(body: {
                  "request_id": widget.model.requests[widget.index].id
                }, apiAddress: "start");
              } else {
                keyStartTravel.currentState.isLoading = false;

                showDialog(
                    context: context,
                    builder: (context) => DialogValidation(
                          title: "خارج از محدوده زمانی",
                          content:
                              "کاربر گرامی، زمان بارگیری کنونی خارج از زمان ثبت شده است. آیا اکنون بارگیری میکنید؟",
                          onPress: () {
                            Navigator.pop(context);
                            keyStartTravel.currentState.isLoading = true;
                            requestBlocStart.fetchStart(body: {
                              "request_id":
                                  widget.model.requests[widget.index].id
                            }, apiAddress: "start");
                          },
                        ));
              }
            },
            text: "شروع سفر",
            afterFinish: (AsyncSnapshot snapshot) async {
              Alerts.success(context, "سفر با موفقیت آغاز شد");
              requestBlocStart.dispose();
            },
          )
        ],
      ),
    );
  }

  Widget startStepButtons() {
    return Padding(
      padding: EdgeInsets.only(right: 64, left: 64),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          EasyButton(
            text: "جزییات بیشتر",
            isFilled: false,
            textColor: Colors.black,
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => RequestDetail(
                            request: widget.model.requests[widget.index],
                            verifyType: widget.verifyType,
                        steps: widget.steps,
                          )));
            },
          ),
          SizedBox(
            height: 8,
          ),
          EasyButton(
            text: "پایان سفر",
            textColor: Colors.white,
            isFilled: true,
            onPressed: () {
              showDialog(
                  context: context,
                  builder: (context) => DialogDriverRate(
                      requestId: widget.model.requests[widget.index].id));
            },
          )
        ],
      ),
    );
  }

  Widget endStepButtons() {
    return Padding(
      padding: EdgeInsets.only(right: 64, left: 64),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          EasyButton(
            text: "جزییات بیشتر",
            isFilled: false,
            textColor: Colors.black,
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => RequestDetail(
                            request: widget.model.requests[widget.index],
                            verifyType: widget.verifyType,
                        steps: widget.steps,
                          )));
            },
          ),
          SizedBox(
            height: 8,
          ),
          EasyButton(
            text: "تماس با صاحب بار",
            textColor: Colors.white,
            isFilled: true,
            onPressed: () {
              launch("tel:09212098662");
            },
          )
        ],
      ),
    );
  }

  ///////////////////// user verified widgets ////////////////////////////

  Widget showAddressOnMapButton() {
    bool shouldButtonShow = widget.verifyType == DriverVerifyType.confirmed &&
        widget.steps != RequestSteps.add;

    if (shouldButtonShow) {
      return Column(
        children: <Widget>[
          EasyButton(
            text: "مشاهده آدرس روی نقشه",
            isFilled: true,
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => MapCargoRoute(
                            originLatLng: LatLng(
                                double.parse(widget.model.requests[widget.index]
                                    .origin_latitude),
                                double.parse(widget.model.requests[widget.index]
                                    .origin_longitude)),
                            destinationLatLng: LatLng(
                                double.parse(widget.model.requests[widget.index]
                                    .destination_latitude),
                                double.parse(widget.model.requests[widget.index]
                                    .destination_longitude)),
                          )));
            },
          )
        ],
      );
    } else {
      return Container();
    }
  }

  Widget statusTitle() {
    bool shouldTitleShow = widget.verifyType == DriverVerifyType.confirmed;

    if (shouldTitleShow) {
      return Container(
        padding: EdgeInsets.only(
          top: 12,
          bottom: 12,
        ),
        color: widget.steps == RequestSteps.end
            ? Colors.red
            : Theme.of(context).primaryColor,
        child: Text(
          "وضعیت: " +
              RequestModel()
                  .returnTitle(widget.model.requests[widget.index].last_status),
          textAlign: TextAlign.center,
          style: TextStyle(color: Colors.white),
        ),
      );
    } else {
      return Container();
    }
  }

  Widget addressView() {
    bool isInAddStep = widget.steps == RequestSteps.add;

    return Container(
      padding: EdgeInsets.all(8),
      decoration: isInAddStep
          ? null
          : BoxDecoration(
              color: Color(0xffCDE7D1),
              borderRadius: BorderRadius.all(Radius.circular(8))),
      child: Container(
        child: address(),
      ),
    );
  }

  Widget cargoOwnerView() {
    bool exceptAddStep = widget.steps != RequestSteps.add;
    if (exceptAddStep) {
      return Container(
        padding: EdgeInsets.all(8),
        margin: EdgeInsets.only(bottom: 8),
        decoration: BoxDecoration(
            color: Color(0xffCDE7D1),
            borderRadius: BorderRadius.all(Radius.circular(8))),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            //origin address
            Row(
              children: <Widget>[
                Container(
                  height: 8,
                  width: 8,
                  decoration: BoxDecoration(
                      shape: BoxShape.circle, color: Colors.black38),
                ),
                SizedBox(
                  width: 4,
                ),
                TitleText(
                  "نام صاحب بار: ",
                  textAlign: TextAlign.right,
                ),
                SubtitleText(
                  ownerName(),
                  textAlign: TextAlign.right,
                ),
              ],
            ),
            SizedBox(
              height: 4,
            ),
            Row(
              children: <Widget>[
                Container(
                  height: 8,
                  width: 8,
                  decoration: BoxDecoration(
                      shape: BoxShape.circle, color: Colors.black38),
                ),
                SizedBox(
                  width: 4,
                ),
                TitleText(
                  "شماره تماس: ",
                  textAlign: TextAlign.right,
                ),
                SubtitleText(
                  ownerPhone(),
                  textAlign: TextAlign.right,
                ),
              ],
            ),
          ],
        ),
      );
    }

    return Container();
  }

  Widget address() {
    bool showAddress = widget.verifyType == DriverVerifyType.confirmed &&
        widget.steps != RequestSteps.add;
    if (showAddress) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          //origin address
          Row(
            children: <Widget>[
              Icon(
                Icons.location_on,
                size: 18,
                color: Colors.black38,
              ),
              TitleText(
                "آدرس مبدا:",
              )
            ],
          ),
          SizedBox(
            height: 4,
          ),
          Padding(
            padding: EdgeInsets.only(right: 22),
            child: SubtitleText(
              widget.model.requests[widget.index].origin_address,
              textAlign: TextAlign.right,
            ),
          ),
          SizedBox(
            height: 8,
          ),
          //destination address
          Row(
            children: <Widget>[
              Icon(
                Icons.location_on,
                size: 18,
                color: Colors.black38,
              ),
              TitleText(
                "آدرس مقصد:",
              )
            ],
          ),
          SizedBox(
            height: 4,
          ),
          Padding(
            padding: EdgeInsets.only(right: 22),
            child: SubtitleText(
              widget.model.requests[widget.index].destination_address,
              textAlign: TextAlign.right,
            ),
          ),
        ],
      );
    } else {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          //origin address
          Row(
            children: <Widget>[
              Icon(
                Icons.location_on,
                size: 18,
                color: Colors.black38,
              ),
              TitleText(
                "شهر مبدا:",
              )
            ],
          ),
          SizedBox(
            height: 4,
          ),
          FutureBuilder(
            future:
            getCityName(widget.model.requests[widget.index].origin_city_id),
            builder: (context, snapshot) {
              if (snapshot.hasData)
                return Padding(
                  padding: EdgeInsets.only(right: 22),
                  child: SubtitleText(
                    snapshot.data,
                    textAlign: TextAlign.right,
                  ),
                );

              return Container();
            },
          ),
          SizedBox(
            height: 8,
          ),
          //destination address
          Row(
            children: <Widget>[
              Icon(
                Icons.location_on,
                size: 18,
                color: Colors.black38,
              ),
              TitleText(
                "شهر مقصد:",
              )
            ],
          ),
          SizedBox(
            height: 4,
          ),
          FutureBuilder(
            future: getCityName(
              widget.model.requests[widget.index].destination_city_id,
            ),
            builder: (context, snapshot) {
              if (snapshot.hasData)
                return Padding(
                  padding: EdgeInsets.only(right: 22),
                  child: SubtitleText(
                    snapshot.data,
                    textAlign: TextAlign.right,
                  ),
                );

              return Container();
            },
          ),
        ],
      );
    }
  }


  checkCancelTimeFinish(String expTime, String updated_at) {
    if (DateTimeConverter.timeDifferenceWithNow(updated_at, expTime) <= 0)
      showCancelButton = false;
  }

  Future<SettingModel> getSettings() async {
    return await Save().getSettingData();
  }

  price() {
    if (widget.model.requests[widget.index].price == "null" ||
        widget.model.requests[widget.index].price == "0") return "توافقی";

    return widget.model.requests[widget.index].price + " تومان";
  }

  Future<String> getVehicleName(vehicle_id) async {
    VehicleModelDetail modelDetail = await VehicleModel.returnData(vehicle_id);
    return modelDetail.name;
  }

  String ownerName() {
    bool isCargoOwner =
        widget.model.requests[widget.index].cargoOwnerDetail != null;
    if (isCargoOwner) {
      return widget.model.requests[widget.index].cargoOwnerDetail.first_name +
          " " +
          widget.model.requests[widget.index].cargoOwnerDetail.last_name;
    } else {
      return "باربری " +
          widget.model.requests[widget.index].carrierDetail.freight_name;
    }
  }

  String ownerPhone() {
    bool isCargoOwner =
        widget.model.requests[widget.index].cargoOwnerDetail != null;
    if (isCargoOwner) {
      return widget.model.requests[widget.index].cargoOwnerDetail.phone_number;
    } else {
      return widget.model.requests[widget.index].carrierDetail.telephone_number;
    }
  }


  getCityName(String id) async {
    String cityName = await CityModel.returnData(id);

    return cityName;
  }
}
